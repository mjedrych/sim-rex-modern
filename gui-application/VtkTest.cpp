// VtkTest.cpp : Defines the entry point for the console application.
//

#include "myVtk.h"
#include <time.h>
#include <cstdlib>

int main(int argc, char* argv[])
{

	vtkRenderWindowInteractor *WxPanelVtk=vtkRenderWindowInteractor::New();
	
	/*
	MyVtk myVtk; 
	myVtk.SetWxVTKRenderWindowInteractor(WxPanelVtk);
	myVtk.Run();
	*/

	MJVtk myVtk; 
	myVtk.SetVtkRenderWindowInteractor(WxPanelVtk);
	myVtk.Run();
	
	
	_sleep(1000);
	WxPanelVtk->Initialize();
	WxPanelVtk->Start();
	/*
	myVtk.SetReader1();
	myVtk.Update();
	WxPanelVtk->Initialize();
	WxPanelVtk->Start();
	_sleep(1000);
	myVtk.SetReader2();
	myVtk.Update();
	*/
	// !!!!!!!!!!!!!!!!!!!//
	//myVtk->Delete();
	//delete myVtk;
	// !!!!!!!!!!!!!!!!!!!//
	return 0;
}

