#include "myVtk.h"

void MJVtk::Initialize()
{
	_dsMapper=0;
	_pdMapper=0;
	_outlineMapper=0;
	_dataActor=0;
	_outlineActor=0;
	_axesActor=0;
	_omWidget=0;
	_outlineFilter=0;
	_renderer=0;
    _renderWindow=0;
	_renderWindowInteractor=0; 
	_styleVtk=0;
	_thresholdPoints=0;
	_thresholdBacup=0;
	_smoother=0;
	_smoother2=0;
	_dsSurfaceFilter=0;
	_geoFilter=0;
	_delaunay3d=0;

	_dsMapper=vtkDataSetMapper::New();
	_pdMapper=vtkPolyDataMapper::New();
	_outlineMapper=vtkPolyDataMapper::New();
	_dataActor=vtkActor::New();
	_outlineActor=vtkActor::New();
	_axesActor=vtkAxesActor::New();
	_omWidget=vtkOrientationMarkerWidget::New();
	_outlineFilter=vtkOutlineFilter::New();
	_renderer=vtkRenderer::New();
	_renderWindow=vtkRenderWindow::New();
	_renderWindowInteractor=vtkRenderWindowInteractor::New();
	_styleVtk=vtkInteractorStyleTrackballCamera::New();
	_thresholdPoints=vtkThresholdPoints::New();
	_thresholdBacup=vtkThreshold::New();
	_smoother=vtkSmoothPolyDataFilter::New();
	_smoother2=vtkWindowedSincPolyDataFilter::New();
	_dsSurfaceFilter=vtkDataSetSurfaceFilter::New();
	_geoFilter=vtkGeometryFilter::New();
	_delaunay3d=vtkDelaunay3D::New();

	_logfile.open("log.txt");
	_activeReader=-1;
	_resetCamera=true;

	_dsSurfaceFilter->DebugOff();
	_smoother->DebugOff();
	_smoother2->DebugOff();
	_smoother2->BoundarySmoothingOff();

	_renderer->SetBackground(0.1, 0.1, 0.1);
}

void MJVtk::Delete()
{
	_logfile.close();
	_dsMapper->Delete();
	_pdMapper->Delete();
	_outlineMapper->Delete();
	_dataActor->Delete();
	_outlineActor->Delete();
	_axesActor->Delete();
	_omWidget->Delete();
	_outlineFilter->Delete();
	_renderer->Delete();
    _renderWindow->Delete();
	_renderWindowInteractor->Delete();
	_styleVtk->Delete();
	_thresholdPoints->Delete();
	_thresholdBacup->Delete();
	_smoother->Delete();
	_dsSurfaceFilter->Delete();
	_geoFilter->Delete();
	_delaunay3d->Delete();

	for(unsigned int i=0;i<_dsReaderVector.size();++i)
		_dsReaderVector[i]->Delete();
}

void MJVtk::CreateVtkPipeLine()
{
	static bool create=true;

	if(create==true)
	{
	_geoFilter->SetInputConnection(_thresholdBacup->GetOutputPort());
	_dsSurfaceFilter->SetInputConnection(_thresholdBacup->GetOutputPort());
	_smoother->SetInputConnection(_dsSurfaceFilter->GetOutputPort());

	_dsMapper->SetInputConnection(_smoother->GetOutputPort());
	_pdMapper->SetInputConnection(_smoother->GetOutputPort());

	_outlineFilter->SetInputConnection(_dsReaderVector[_activeReader]->GetOutputPort());
	_outlineMapper->SetInputConnection(_outlineFilter->GetOutputPort());

	_dataActor->SetMapper(_dsMapper);
	_outlineActor->SetMapper(_outlineMapper);

	_dataActor->GetProperty()->SetRepresentationToSurface();
	_dataActor->GetProperty()->SetInterpolation(0);
	
	_renderer->AddActor(_outlineActor);
    _renderer->AddActor(_dataActor);
	_renderWindow->AddRenderer(_renderer);

	}
	create=false;
}

void MJVtk::SetSmoothParamters(double rel, int iter, double angle)
{
	_smoother->SetEdgeAngle(angle);
	_smoother->SetNumberOfIterations(iter);
	_smoother->SetRelaxationFactor(rel);

	_smoother2->SetEdgeAngle(angle);
	_smoother2->SetNumberOfIterations(iter);
	//_smoother2->SetRelaxationFactor(rel);
}

void MJVtk::SetThresholdParamters(double l,double u)
{
	_thresholdPoints->ThresholdBetween(l,u + 0.1);
	_thresholdBacup->ThresholdBetween(l,u + 0.1);
}

void MJVtk::GetDataScalarsRange(float &min, float &max)
{
	min=range[0];
	max=range[1];
}

void MJVtk::AddDataReader(std::string filename)
{
	vtkDataSetReader* dsReader=vtkDataSetReader::New();
	dsReader->SetFileName(filename.c_str());
	dsReader->ReadAllScalarsOn();
	dsReader->ReadAllColorScalarsOn();
	dsReader->OpenVTKFile();
	dsReader->ReadHeader();

	dsReader->Update();
	_dsReaderVector.push_back(dsReader);
	
	/*
	if(_dsReaderVector.size()==1)
	{
		_thresholdPoints->SetInputConnection(_dsReaderVector[0]->GetOutputPort());
		_thresholdBacup->SetInputConnection(_dsReaderVector[0]->GetOutputPort());
	}
	*/
}

void MJVtk::GetDataArraysName(std::vector<std::string>& danvec)
{
	int numArrays=0;
	vtkPointData *pointData = _dsReaderVector[_activeReader]->GetOutput()->GetPointData();
	vtkCellData *cellData = _dsReaderVector[_activeReader]->GetOutput()->GetCellData();
	numArrays = pointData->GetNumberOfArrays();
	_logfile << "Number of arrays in PointData = " << numArrays << endl;
	for(int i = 0; i < numArrays; ++i)
    {
        vtkDataArray *dataArray = pointData->GetArray(i);
        std::string name = pointData->GetArrayName(i);
		danvec.push_back(name);
        int numTuples = dataArray->GetNumberOfTuples();
        int numComponents = dataArray->GetNumberOfComponents();
        _logfile << "PointDataArray[" << i << "] = " << name;
        _logfile << "  " << dataArray->GetClassName();
        _logfile << "  " << "(" << numTuples << " x " << numComponents << ")";
        _logfile << endl;
    }
	
	numArrays = cellData->GetNumberOfArrays();
	_logfile << "Number of arrays in CellData = " << numArrays << endl;
	for (int i = 0; i < numArrays; i++)
	{
		vtkDataArray *dataArray = cellData->GetArray(i);
		std::string name = cellData->GetArrayName(i);
		danvec.push_back(name);
		int numTuples = dataArray->GetNumberOfTuples();
		int numComponents = dataArray->GetNumberOfComponents();
		_logfile << "CellDataArray[" << i << "] = " << name;
		_logfile << "  " << dataArray->GetClassName();
		_logfile << "  " << "(" << numTuples << " x " << numComponents << ")";
		_logfile << endl;
	}
	_logfile<<endl;
}

void MJVtk::SetColorDataName(std::string& name)
{
	vtkPointData *pointData = _dsReaderVector[_activeReader]->GetOutput()->GetPointData();
	int numArrays = pointData->GetNumberOfArrays();
    if (numArrays > 0)
	{
		_dsMapper->SetScalarModeToUsePointFieldData();
		pointData->GetScalars(name.c_str())->GetRange(range);	
	}
	else
	{
		_dsMapper->SetScalarModeToUseCellFieldData();
		_dsReaderVector[_activeReader]->GetOutput()->GetCellData()->GetScalars(name.c_str())->GetRange(range);	
	}
	_dsMapper->SelectColorArray(name.c_str());
	_dsMapper->SetScalarRange(range);

	_logfile<<"Mapper Parameters: \n";
	_logfile<<"Data Name "<<_dsMapper->GetArrayName()<<endl;
	_logfile<<"Class Name "<<_dsMapper->GetClassName()<<endl;
	_logfile<<"Number of colors: "<<_dsMapper->GetLookupTable()->GetNumberOfAvailableColors()<<endl;
	_logfile<<"min scalar: "<< range[0] <<endl;
	_logfile<<"max scalar: "<< range[1] <<endl <<endl;	
}

void MJVtk::SetThresholdDataName(std::string& name)
{
	vtkPointData *pointData = _dsReaderVector[_activeReader]->GetOutput()->GetPointData();
	int numArrays = pointData->GetNumberOfArrays();
    if (numArrays > 0)
	{
		_thresholdPoints->SetInputArrayToProcess(0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_POINTS, name.c_str());
		_thresholdBacup->SetInputArrayToProcess(0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_POINTS, name.c_str());
		pointData->GetScalars(name.c_str())->GetRange(range);
		// K:: Pobieram zakres skalarow, poniewaz musze pozniej odwiezyc kontrolki w GUI - ustawic zakresy sliderow //
	}
	else
	{
		_thresholdPoints->SetInputArrayToProcess(0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_CELLS, name.c_str());
		_thresholdBacup->SetInputArrayToProcess(0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_CELLS, name.c_str());
		_dsReaderVector[_activeReader]->GetOutput()->GetCellData()->GetScalars(name.c_str())->GetRange(range);
	}
}

void MJVtk::DeleteDataReader(int pos)
{
	_activeReader=-1;
	_dsReaderVector[pos]->Delete();
	_dsReaderVector.erase(_dsReaderVector.begin()+pos);
}

void MJVtk::SetActiveReader(int pos)
{
	_activeReader=pos;
}

void  MJVtk::SetInterpolation(bool i)
{
	_dataActor->GetProperty()->SetInterpolation(i);
}

void MJVtk::ResetCamera()
{
	_resetCamera=true;
}

void MJVtk::Render()
{
	if(_activeReader>=0)
	{
		_thresholdPoints->SetInputConnection(_dsReaderVector[_activeReader]->GetOutputPort());
		_thresholdBacup->SetInputConnection(_dsReaderVector[_activeReader]->GetOutputPort());
		_outlineFilter->SetInputConnection(_dsReaderVector[_activeReader]->GetOutputPort());
		//widget->SetOutlineColor( 0.9300, 0.5700, 0.1300 );
		
		_omWidget->SetOrientationMarker(_axesActor);
		_omWidget->SetInteractor(_renderWindowInteractor);
		_omWidget->SetEnabled(1);
		_omWidget->InteractiveOff();

		if(_resetCamera)
		{
			_renderer->ResetCamera();
			_resetCamera=false;
		}

		_renderWindow->Render();
	}
}

void  MJVtk::UpdateReader(int r)
{
	std::string filename=_dsReaderVector[r]->GetFileName();
	_dsReaderVector[r]->Delete();
	_dsReaderVector[r]=vtkDataSetReader::New();
	_dsReaderVector[r]->SetFileName(filename.c_str());
	_dsReaderVector[r]->Update();
	
	/* sprawdzic czy to jest rzeczywiscie potrzebne */
	//_thresholdPoints->SetInputConnection(_dsReaderVector[r]->GetOutputPort());
	//_thresholdBacup->SetInputConnection(_dsReaderVector[r]->GetOutputPort());
	
	ResetCamera();
	Render();
}

void MJVtk::SetPointSize(int s)
{
	_dataActor->GetProperty()->SetPointSize(static_cast<float>(s));
}

void MJVtk::SetRepresentation(int r)
{
	switch(r)
	{
		case 0: _dsMapper->SetInputConnection(_smoother->GetOutputPort());_dataActor->GetProperty()->SetRepresentationToSurface(); break;
		case 1: _dsMapper->SetInputConnection(_smoother->GetOutputPort());_dataActor->GetProperty()->SetRepresentationToPoints(); break;
		case 2: _dsMapper->SetInputConnection(_thresholdPoints->GetOutputPort()); break;
	}
}

MyVtk::MyVtk()
{
    //smoother    = 0;
    mapper = 0;
    actor  = 0;
    
    outlineData = 0;
    mapOutline  = 0;
    outline     = 0;
    renderer    = 0;
    styleVtk    = 0;
    file_name = "";
    folder    = "";
    //smooth    = 0.01;
    Init();
}

// -------------------------------- init ------------------------------//
void MyVtk::Init()
{
	threshold=vtkThreshold::New();
	//reader=vtkDataSetReader::New();

	reader = vtkStructuredGridReader::New();
	reader1 = vtkStructuredGridReader::New();
	reader2 = vtkStructuredGridReader::New();
    //smoother = vtkSmoothPolyDataFilter::New();
	mapper = vtkDataSetMapper::New();
    actor       = vtkActor::New();
	
	threshold->SetInputConnection(reader2->GetOutputPort());
	//threshold->ThresholdBetween(1.0,1.0);
	//threshold->SetInValue(2796.0);
	//threshold->SetOutValue(0.0);
	mapper->SetInputConnection(reader->GetOutputPort());
	//mapper->SetInputConnection(threshold->GetOutputPort());
	actor->SetMapper(mapper);
	//actor->GetProperty()->SetInterpolationToFlat();
	actor->GetProperty()->SetInterpolation(1);
	//actor->GetProperty()->SetAmbient(0.125);
	//actor->GetProperty()->SetDiffuse(0.0);
	//actor->GetProperty()->SetSpecular(0.0);
    outlineData = vtkOutlineFilter::New();
    mapOutline  = vtkPolyDataMapper::New();
    outline     = vtkActor::New();
    
    renderer    = vtkRenderer::New();
    renWin      = vtkRenderWindow::New();
    styleVtk    = vtkInteractorStyleTrackballCamera::New();
    
	// !!!!!!!!! DEBUGOWANIE !!!!!!!!!!!//
    //  reader->SetDebug(1);
     
      //smoother->SetInputConnection(...->GetOutputPort());
    //  smoother->SetDebug(1);
      //mapper->SetInputConnection(smoother->GetOutputPort());
      
      renWin->AddRenderer(renderer);
      
      outlineData->SetInputConnection(reader->GetOutputPort());
      mapOutline->SetInputConnection(outlineData->GetOutputPort());
      outline->SetMapper(mapOutline);
}

// --------------------------- delete ---------------------------------//
void MyVtk::Delete()
{
    if( renWin   != 0 )      renWin->Delete();    
    if( renderer != 0 )      renderer->Delete();
    if( styleVtk != 0 )      styleVtk->Delete();   

    if( outline     != 0 )   outline->Delete();
    if( mapOutline  != 0 )   mapOutline->Delete();    
    if( outlineData != 0 )   outlineData->Delete();
        
    if( actor    != 0 )      actor->Delete();
    if( mapper   != 0 )      mapper->Delete();
    //if( smoother != 0 )      smoother->Delete();                  
    if( reader1   != 0 )      reader->Delete();   
	//if( reader   != 0 )      reader->Delete();   


}
// ---------------------------- run -----------------------------------//

void MyVtk::Run()
{
	reader->SetFileName("wb.vtk");
	//reader->SetFileName("rec10.vtk");
	reader1->SetFileName("rec15.vtk");
	reader2->SetFileName("rec25.vtk");
	reader->Update();
	reader1->Update();
	reader2->Update();
	//system("pause");

	/*
	vtkStructuredGridWriter *wbac=vtkStructuredGridWriter::New();
	vtkUnstructuredGridWriter *w=vtkUnstructuredGridWriter::New();
	vtkUnstructuredGridReader *r=vtkUnstructuredGridReader::New();
	r->SetFileName("rec.vtk");
	r->Update();
	w->SetInputConnection(r->GetOutputPort());
	w->SetFileName("wb2.vtk");
	w->SetFileTypeToASCII();
	//w->Write();
	*/
	
	
	/*
	vtkStructuredGridWriter *w=vtkStructuredGridWriter::New();
	w->SetInputConnection(reader->GetOutputPort());
	w->SetFileName("wb.vtk");
	w->SetFileTypeToBinary();
	w->Write();
	*/
	
	
	
/*
      
      if(b_reader)
      {  
      
        reader->SetFilePattern(file_name.c_str());
        reader->SetDataSpacing( x_space, y_space, z_space );
        reader->SetDataExtent(x_min_size, x_max_size, y_min_size, y_max_size, z_min_size, z_max_size );
        reader->Update();
    }

      if(b_reader)
      {  
        clip->SetOutputWholeExtent(x_min_size, x_max_size, y_min_size, y_max_size, z_min_size, z_max_size);
        clip->ClipDataOn();
      }

  
    if(b_smooth)
    {
 
      smoother->SetRelaxationFactor(smooth);
      smoother->SetNumberOfIterations(iter);
      smoother->SetFeatureAngle(angle);
      smoother->FeatureEdgeSmoothingOn();
    }




     mapper->ScalarVisibilityOff();
     actor->SetMapper(mapper);
   //  actor->GetProperty()->SetOpacity(0.5);
    

    outline->GetProperty()->SetColor(1,1,1);

    if(b_reader)
    {
        camera->SetViewUp (0, 0, -1);
        camera->SetPosition (0, 1, 0);
        camera->SetFocalPoint (0, 0, 0);
        camera->ComputeViewPlaneNormal();
        camera->Dolly(1.5);
    }
    
*/
    renderer->AddActor(outline);
    renderer->AddActor(actor);
	//system("pause");
	
	axesactor = vtkAxesActor::New();
	widget = vtkOrientationMarkerWidget::New();
	//widget->SetOutlineColor( 0.9300, 0.5700, 0.1300 );
	widget->SetOrientationMarker( axesactor );
	widget->SetInteractor( wxVtkIter );
	widget->SetEnabled( 1 );
	widget->InteractiveOff();
	widget->InteractiveOn();
	
	// Czemu usunalem te linie ???
	// wxVtkIter->Initialize();
	
	renderer->ResetCamera();
	renWin->Render();
    
  //  if(b_reader)
  //  {
  //      renderer->SetActiveCamera(camera);
  //      renderer->ResetCamera();
    //    renderer->SetBackground(0,0,0);
  //      renderer->ResetCameraClippingRange ();
  //  }

    
  
   // b_reader     = false;
   // b_shrink     = false;
   // b_threshold  = false;
   // b_smooth     = false;

}
// ----------------------------------------------------------------------------//

void MyVtk::SetFileName()     
{ 
    /*
	file_name = folder; 
    file_name +="\\"; 
 
    int nr=0;
    std::string temp_prefix = "";
    for(unsigned int i=0; i<prefix.size(); i++)  if(prefix[i]=='#')  nr++; else temp_prefix+= prefix[i];
    if(nr == 0 )  temp_prefix += "%d.png";
      else  
        {
              
            std::ostringstream ss;
            ss << "%" << nr << "d.png";
            temp_prefix += ss.str();
    }
     
    file_name += temp_prefix; 
	 */ 
}

void MyVtk::SetFolder(std::string _str)   
{ 
    //if( folder != _str) { folder = _str; b_reader = true; } 
}


void MyVtk::SetPrefix(std::string _str)   
{ 
    //if( prefix != _str ) { prefix = _str; b_reader = true; }
}



