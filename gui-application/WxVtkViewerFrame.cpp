#include "WxVtkViewerFrame.h"
#include <wx/artprov.h> //wxAUI do zarzadania interfejsem
#include <wx/cmdline.h>

IMPLEMENT_APP(WxVtkViewerApp)

bool WxVtkViewerApp::OnInit()
{
	if ( !wxApp::OnInit() )
        return false;

    WxVtkViewerFrame* frame = new WxVtkViewerFrame(NULL);
	for(int i=0;i<files.GetCount();++i)
	{
		//wxMessageBox(files[i]);
		frame->LoadFileFromExplorer(files[i]);
	}
    frame->Show();
    return true;
}

void WxVtkViewerApp::OnInitCmdLine(wxCmdLineParser& parser)
{
	parser.AddParam(wxT( "[Filename #1] [... [Filename #n]]]]" ), wxCMD_LINE_VAL_STRING,
                      wxCMD_LINE_PARAM_OPTIONAL | wxCMD_LINE_PARAM_MULTIPLE );
}
 
bool WxVtkViewerApp::OnCmdLineParsed(wxCmdLineParser& parser)
{
    for (int i = 0; i < parser.GetParamCount(); i++)
    {
		wxFileName fn(parser.GetParam(i));
		fn.Normalize(wxPATH_NORM_LONG|wxPATH_NORM_DOTS|wxPATH_NORM_TILDE|wxPATH_NORM_ABSOLUTE);
		files.Add(fn.GetFullName());
    }
    return true;
}

WxVtkViewerFrame::WxVtkViewerFrame( wxWindow* parent ):
WxVtkViewerAbstractFrame( parent )
{
	//------------ AUI MANAGER SET UP ------------//
	_auiMgr.SetFlags(  wxAUI_MGR_ALLOW_FLOATING|
	               wxAUI_MGR_ALLOW_ACTIVE_PANE|
	               wxAUI_MGR_TRANSPARENT_DRAG |
	               wxAUI_MGR_TRANSPARENT_HINT);
	// -----------------------------------------//
	//_panelVtkRenderInteractor= new wxVTKRenderWindowInteractor(this, wxID_ANY);
	_panelVtkRenderInteractor = new wxVTKRenderWindowInteractor( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	_auiMgr.SetManagedWindow(this);
	_auiMgr.AddPane(_filePanel , wxAuiPaneInfo().Left().Floatable(true).CloseButton(false).Caption("List Of Files"));
	_auiMgr.AddPane(_panelNotebook, wxAuiPaneInfo().Left().Floatable(true).CloseButton(false).Caption("Tabs"));
	_auiMgr.AddPane(_panelVtkRenderInteractor, wxAuiPaneInfo().Center().Floatable(false).CloseButton(false).Caption("View"));

	_auiMgr.GetArtProvider()->SetMetric(wxAUI_DOCKART_GRADIENT_TYPE , wxAUI_GRADIENT_VERTICAL);
	_auiMgr.GetArtProvider()->SetColor(wxAUI_DOCKART_INACTIVE_CAPTION_COLOUR , wxColour(0,0,128));
	_auiMgr.GetArtProvider()->SetColor(wxAUI_DOCKART_INACTIVE_CAPTION_GRADIENT_COLOUR , wxColour(0,128,255));
	_auiMgr.GetArtProvider()->SetColor(wxAUI_DOCKART_INACTIVE_CAPTION_TEXT_COLOUR  , wxColour(220,220,220));

	_auiMgr.GetArtProvider()->SetColor(wxAUI_DOCKART_ACTIVE_CAPTION_GRADIENT_COLOUR ,wxColour(192,192,192));
	_auiMgr.GetArtProvider()->SetColor(wxAUI_DOCKART_ACTIVE_CAPTION_COLOUR,wxColour(56,56,56));
	_auiMgr.GetArtProvider()->SetColor(wxAUI_DOCKART_ACTIVE_CAPTION_TEXT_COLOUR  , wxColour(255,255,255));

	_auiMgr.Update();
	
	_mjvtk.SetVtkRenderWindowInteractor(_panelVtkRenderInteractor);
	
	_openDialog= new wxFileDialog(this, _("Choose a file"), _(" "), _(" "), _("*.vtk"), wxFD_OPEN|wxFD_MULTIPLE|wxFD_CHANGE_DIR, wxDefaultPosition, wxDefaultSize, _("filedlg"));

	_sliderRelaxation->SetValue(8);
	_sliderIterations->SetValue(10);
	_sliderAngle->SetValue(50);
	_sliderLL->SetValue(0);
	_sliderUL->SetValue(0);

	SliderRelaxationScrolled( wxScrollEvent() );
	SliderIterationsScrolled( wxScrollEvent() );
	SliderAngleScrolled( wxScrollEvent() );
	SliderLLScrolled( wxScrollEvent() );
	SliderULScrolled( wxScrollEvent() );

	_cbAutoRefresh->SetValue(true);
	_mjvtk.SetPointSize(_psSpin->GetValue());
}

WxVtkViewerFrame::~WxVtkViewerFrame()
{
	_auiMgr.UnInit();
	Destroy();
}

void  WxVtkViewerFrame::OnSize( wxSizeEvent& event )
{ 
	_panelVtkRenderInteractor->Refresh();
}

void WxVtkViewerFrame::SliderRelaxationScrolled( wxScrollEvent& event )
{
	_textCtrlRelaxation->SetLabelText(wxString::Format(wxT("%.2f"), 1.0*_sliderRelaxation->GetValue()/100.0));
	Update();
}

void WxVtkViewerFrame::SliderIterationsScrolled( wxScrollEvent& event )
{
	_textCtrlIterations->SetLabelText(wxString::Format(wxT("%d"),_sliderIterations->GetValue()));
	Update();
}

void WxVtkViewerFrame::SliderAngleScrolled( wxScrollEvent& event )
{
	_textCtrlAngle->SetLabelText(wxString::Format(wxT("%d"),_sliderAngle->GetValue()));
	Update();
}

void WxVtkViewerFrame::SliderLLScrolled( wxScrollEvent& event )
{
	if(_sliderLL->GetValue()>_sliderUL->GetValue())
		_sliderLL->SetValue(_sliderUL->GetValue());
	_textLL->SetLabelText(wxString::Format(wxT("%.2f"), 1.0*_sliderLL->GetValue()/1000.0));
	Update();
}

void WxVtkViewerFrame::SliderULScrolled( wxScrollEvent& event )
{
	if(_sliderUL->GetValue()<_sliderLL->GetValue())
		_sliderUL->SetValue(_sliderLL->GetValue());
	_textUL->SetLabelText(wxString::Format(wxT("%.2f"), 1.0*_sliderUL->GetValue()/1000.0));
	Update();
}

void WxVtkViewerFrame::SelectAllClicked( wxCommandEvent& event )
{
	static bool flag=false;

	if(_fileCheckList->IsEmpty())
	{
		flag=false;
		_selectAllCheckBox->SetValue(false);
	}
	else
	{
		flag = !flag;
		for(unsigned int i=0;i<_fileCheckList->GetCount();++i)
		{
			_fileCheckList->Check(i, flag);
		}
	}
}

void WxVtkViewerFrame::FileItemSelected( wxCommandEvent& event )
{
	int selection=_fileCheckList->GetSelection();
	//wxMessageBox(wxString::Format("%d",selection));
	_mjvtk.SetActiveReader(selection);
	_mjvtk.CreateVtkPipeLine();
	_mjvtk.ResetCamera();

	if(!_keepButton->IsChecked())
	{
		std::vector<std::string> dataArrayNames;
		_mjvtk.GetDataArraysName(dataArrayNames);

		_colorChoiceBox->Clear();
		_thresholdChoiceBox->Clear();
		for(unsigned int s=0;s<dataArrayNames.size();++s)
		{
			_colorChoiceBox->Append(wxString(dataArrayNames[s]));
			_thresholdChoiceBox->Append(wxString(dataArrayNames[s]));
		}
		_colorChoiceBox->SetSelection(0);
		_thresholdChoiceBox->SetSelection(0);

		ThresholdDataChosen(wxCommandEvent());
		ColorDataChosen(wxCommandEvent());
	}
	else
		Update();
}

void WxVtkViewerFrame::ColorDataChosen( wxCommandEvent& event ) 
{ 
	wxString name=_colorChoiceBox->GetStringSelection();
	_mjvtk.SetColorDataName(name.ToStdString());
	Update();
}

void WxVtkViewerFrame::ThresholdDataChosen( wxCommandEvent& event ) 
{ 
	wxString name=_thresholdChoiceBox->GetStringSelection();
	_mjvtk.SetThresholdDataName(name.ToStdString());
	
	float min,max;
	_mjvtk.GetDataScalarsRange(min,max);
	_sliderLL->SetMin(static_cast<int>(min*1000));
	_sliderLL->SetMax(static_cast<int>(max*1000));
	_sliderLL->SetValue(static_cast<int>(min*1000));
	_sliderUL->SetMin(static_cast<int>(min*1000));
	_sliderUL->SetMax(static_cast<int>(max*1000));
	_sliderUL->SetValue(static_cast<int>(max*1000));

	SliderLLScrolled(wxScrollEvent());
	SliderULScrolled(wxScrollEvent());
}

void WxVtkViewerFrame::ToolOpenClicked( wxCommandEvent& event )
{ 
	if (_openDialog->ShowModal() == wxID_OK)
	{
		wxArrayString _filenamesArray;
		_filenamesArray.Clear();
		_openDialog->GetFilenames(_filenamesArray);
		_fileCheckList->Append(_filenamesArray);
		for(unsigned int i=0;i<_filenamesArray.GetCount();++i)
		{
			_mjvtk.AddDataReader(_filenamesArray[i].ToStdString());
			//wxMessageBox(wxString::Format("%s",_filenamesArray[i].ToStdString().c_str()));
		}
	}	
}

void WxVtkViewerFrame::ToolReloadClicked( wxCommandEvent& event )
{
	int selection=_fileCheckList->GetSelection();
	if(selection>=0)
	{
		_mjvtk.UpdateReader(selection);	
	}

	for(unsigned int i=0; i<_fileCheckList->GetCount();++i)
	{
		if(	_fileCheckList->IsChecked(i))
		{
			_mjvtk.UpdateReader(i);
		}
	}	
	FileItemSelected(wxCommandEvent());
}

void WxVtkViewerFrame::ToolResetCameraClicked( wxCommandEvent& event )
{
	_mjvtk.ResetCamera();
	Update();
}

void WxVtkViewerFrame::ToolPlayClicked( wxCommandEvent& event )
{
	for(unsigned int i=0; i<_fileCheckList->GetCount();++i)
	{
		if(	_fileCheckList->IsChecked(i))
		{
			_mjvtk.SetActiveReader(i);
			_mjvtk.ResetCamera();
			_mjvtk.Render();
			Sleep(250);
		}
	}
	_fileCheckList->SetSelection(-1);
}

void WxVtkViewerFrame::ToolDeleteClicked( wxCommandEvent& event )
{
	for(int i=_fileCheckList->GetCount()-1;i>=0;--i)
		if(	_fileCheckList->IsChecked(i))
		{
			_fileCheckList->Delete(i);
			_mjvtk.DeleteDataReader(i);
		}
	if(_fileCheckList->IsEmpty())
		SelectAllClicked( wxCommandEvent() );
	_colorChoiceBox->Clear();
	_thresholdChoiceBox->Clear();
	_keepButton->SetValue(false);
}

void WxVtkViewerFrame::ToolRenderClicked( wxCommandEvent& event )
{
	_mjvtk.SetSmoothParamters(static_cast<double>(_sliderRelaxation->GetValue()/100.0), _sliderIterations->GetValue(), static_cast<double>(_sliderAngle->GetValue()));
	_mjvtk.SetThresholdParamters(static_cast<double>(_sliderLL->GetValue()/1000.0), static_cast<double>(_sliderUL->GetValue()/1000.0));
	_mjvtk.Render();
}

void WxVtkViewerFrame::OnInterpolationCB( wxCommandEvent& event )
{
	_mjvtk.SetInterpolation(_interpolationCB->GetValue());
	_mjvtk.Render();

}

void WxVtkViewerFrame::Update()
{
	if(_cbAutoRefresh->IsChecked())
		ToolRenderClicked( wxCommandEvent());
	_panelVtkRenderInteractor->Refresh();
}

void WxVtkViewerFrame::LoadFileFromExplorer(wxString filename)
{
	_fileCheckList->Append(filename);
	_mjvtk.AddDataReader(filename.ToStdString());
	_fileCheckList->SetSelection(0);
	FileItemSelected( wxCommandEvent() );
}

void WxVtkViewerFrame::OnDispChoice( wxCommandEvent& event )
{
	_mjvtk.SetRepresentation(_dispChoice->GetSelection());
	Update();
}

void WxVtkViewerFrame::OnPointSize( wxSpinEvent& event )
{
	_mjvtk.SetPointSize(_psSpin->GetValue());
	Update();
}