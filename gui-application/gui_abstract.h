///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun 30 2011)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __GUI_ABSTRACT_H__
#define __GUI_ABSTRACT_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/gdicmn.h>
#include <wx/toolbar.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/stattext.h>
#include <wx/checkbox.h>
#include <wx/sizer.h>
#include <wx/checklst.h>
#include <wx/choice.h>
#include <wx/panel.h>
#include <wx/slider.h>
#include <wx/textctrl.h>
#include <wx/statbox.h>
#include <wx/spinctrl.h>
#include <wx/aui/auibook.h>
#include <wx/frame.h>

///////////////////////////////////////////////////////////////////////////

#define WxOPEN 1000
#define WxRELOAD 1001
#define WxDELETE 1002
#define WxRENDER 1003
#define wxRESETCAMERA 1004
#define wxPLAY 1005

///////////////////////////////////////////////////////////////////////////////
/// Class WxVtkViewerAbstractFrame
///////////////////////////////////////////////////////////////////////////////
class WxVtkViewerAbstractFrame : public wxFrame 
{
	private:
	
	protected:
		wxBoxSizer* _sizerMain;
		wxPanel* _filePanel;
		wxCheckBox* _selectAllCheckBox;
		wxCheckBox* _keepButton;
		wxCheckListBox* _fileCheckList;
		wxStaticText* _colorByLabel;
		wxChoice* _colorChoiceBox;
		wxCheckBox* _cbAutoRefresh;
		wxAuiNotebook* _panelNotebook;
		wxPanel* _thresholdPanel;
		wxChoice* _thresholdChoiceBox;
		wxSlider* _sliderLL;
		wxTextCtrl* _textLL;
		wxSlider* _sliderUL;
		wxTextCtrl* _textUL;
		wxPanel* _smoothPanel;
		wxSlider* _sliderRelaxation;
		wxTextCtrl* _textCtrlRelaxation;
		wxSlider* _sliderIterations;
		wxTextCtrl* _textCtrlIterations;
		wxSlider* _sliderAngle;
		wxTextCtrl* _textCtrlAngle;
		wxPanel* _disPanel;
		wxChoice* _dispChoice;
		wxSpinCtrl* _psSpin;
		wxCheckBox* _interpolationCB;
		
		// Virtual event handlers, overide them in your derived class
		virtual void OnSize( wxSizeEvent& event ) { event.Skip(); }
		virtual void ToolOpenClicked( wxCommandEvent& event ) { event.Skip(); }
		virtual void ToolReloadClicked( wxCommandEvent& event ) { event.Skip(); }
		virtual void ToolDeleteClicked( wxCommandEvent& event ) { event.Skip(); }
		virtual void ToolRenderClicked( wxCommandEvent& event ) { event.Skip(); }
		virtual void ToolResetCameraClicked( wxCommandEvent& event ) { event.Skip(); }
		virtual void ToolPlayClicked( wxCommandEvent& event ) { event.Skip(); }
		virtual void SelectAllClicked( wxCommandEvent& event ) { event.Skip(); }
		virtual void FileItemSelected( wxCommandEvent& event ) { event.Skip(); }
		virtual void ColorDataChosen( wxCommandEvent& event ) { event.Skip(); }
		virtual void ThresholdDataChosen( wxCommandEvent& event ) { event.Skip(); }
		virtual void SliderLLScrolled( wxScrollEvent& event ) { event.Skip(); }
		virtual void SliderULScrolled( wxScrollEvent& event ) { event.Skip(); }
		virtual void SliderRelaxationScrolled( wxScrollEvent& event ) { event.Skip(); }
		virtual void SliderIterationsScrolled( wxScrollEvent& event ) { event.Skip(); }
		virtual void SliderAngleScrolled( wxScrollEvent& event ) { event.Skip(); }
		virtual void OnDispChoice( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnPointSize( wxSpinEvent& event ) { event.Skip(); }
		virtual void OnInterpolationCB( wxCommandEvent& event ) { event.Skip(); }
		
	
	public:
		
		WxVtkViewerAbstractFrame( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 848,600 ), long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );
		
		~WxVtkViewerAbstractFrame();
	
};

#endif //__GUI_ABSTRACT_H__
