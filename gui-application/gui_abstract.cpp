///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun 30 2011)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "gui_abstract.h"

///////////////////////////////////////////////////////////////////////////

WxVtkViewerAbstractFrame::WxVtkViewerAbstractFrame( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxSize( 800,600 ), wxDefaultSize );
	
	wxToolBar* m_toolBar1;
	m_toolBar1 = this->CreateToolBar( wxTB_HORIZONTAL|wxTB_HORZ_TEXT|wxTB_NOICONS|wxTB_TEXT, wxID_ANY ); 
	m_toolBar1->AddTool( WxOPEN, wxT("Open"), wxNullBitmap, wxNullBitmap, wxITEM_NORMAL, wxT("Open Vtk File"), wxEmptyString, NULL ); 
	
	m_toolBar1->AddTool( WxRELOAD, wxT("Reload"), wxNullBitmap, wxNullBitmap, wxITEM_NORMAL, wxT("Reload Vtk Files"), wxEmptyString, NULL ); 
	
	m_toolBar1->AddTool( WxDELETE, wxT("Delete"), wxNullBitmap, wxNullBitmap, wxITEM_NORMAL, wxEmptyString, wxEmptyString, NULL ); 
	
	m_toolBar1->AddTool( WxRENDER, wxT("Render"), wxNullBitmap, wxNullBitmap, wxITEM_NORMAL, wxEmptyString, wxEmptyString, NULL ); 
	
	m_toolBar1->AddTool( wxRESETCAMERA, wxT("Reset"), wxNullBitmap, wxNullBitmap, wxITEM_NORMAL, wxEmptyString, wxEmptyString, NULL ); 
	
	m_toolBar1->AddTool( wxPLAY, wxT("Play"), wxNullBitmap, wxNullBitmap, wxITEM_NORMAL, wxEmptyString, wxEmptyString, NULL ); 
	
	m_toolBar1->Realize(); 
	
	_sizerMain = new wxBoxSizer( wxHORIZONTAL );
	
	_filePanel = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer1;
	bSizer1 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer8;
	bSizer8 = new wxBoxSizer( wxHORIZONTAL );
	
	wxStaticText* m_staticText4;
	m_staticText4 = new wxStaticText( _filePanel, wxID_ANY, wxT("List of Files"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText4->Wrap( -1 );
	m_staticText4->Hide();
	
	bSizer8->Add( m_staticText4, 1, wxALL, 5 );
	
	_selectAllCheckBox = new wxCheckBox( _filePanel, wxID_ANY, wxT("(De)Select All"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer8->Add( _selectAllCheckBox, 0, wxALL, 5 );
	
	_keepButton = new wxCheckBox( _filePanel, wxID_ANY, wxT("Keep View"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer8->Add( _keepButton, 0, wxALL, 5 );
	
	bSizer1->Add( bSizer8, 0, wxEXPAND, 5 );
	
	wxArrayString _fileCheckListChoices;
	_fileCheckList = new wxCheckListBox( _filePanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, _fileCheckListChoices, wxLB_NEEDED_SB|wxLB_SINGLE );
	bSizer1->Add( _fileCheckList, 1, wxALL|wxEXPAND, 5 );
	
	_colorByLabel = new wxStaticText( _filePanel, wxID_ANY, wxT("Color By:"), wxDefaultPosition, wxDefaultSize, 0 );
	_colorByLabel->Wrap( -1 );
	bSizer1->Add( _colorByLabel, 0, wxRIGHT|wxLEFT, 5 );
	
	wxArrayString _colorChoiceBoxChoices;
	_colorChoiceBox = new wxChoice( _filePanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, _colorChoiceBoxChoices, 0 );
	_colorChoiceBox->SetSelection( 0 );
	bSizer1->Add( _colorChoiceBox, 0, wxALL|wxEXPAND, 5 );
	
	_cbAutoRefresh = new wxCheckBox( _filePanel, wxID_ANY, wxT("Auto Refresh [On/OFF]"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer1->Add( _cbAutoRefresh, 0, wxALL, 5 );
	
	_filePanel->SetSizer( bSizer1 );
	_filePanel->Layout();
	bSizer1->Fit( _filePanel );
	_sizerMain->Add( _filePanel, 1, wxALL|wxEXPAND, 5 );
	
	_panelNotebook = new wxAuiNotebook( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxAUI_NB_SCROLL_BUTTONS|wxAUI_NB_TAB_EXTERNAL_MOVE|wxAUI_NB_TAB_MOVE|wxAUI_NB_TAB_SPLIT|wxAUI_NB_WINDOWLIST_BUTTON );
	_thresholdPanel = new wxPanel( _panelNotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxStaticBoxSizer* sbSizer2;
	sbSizer2 = new wxStaticBoxSizer( new wxStaticBox( _thresholdPanel, wxID_ANY, wxEmptyString ), wxVERTICAL );
	
	wxArrayString _thresholdChoiceBoxChoices;
	_thresholdChoiceBox = new wxChoice( _thresholdPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, _thresholdChoiceBoxChoices, 0 );
	_thresholdChoiceBox->SetSelection( 0 );
	sbSizer2->Add( _thresholdChoiceBox, 0, wxALL|wxEXPAND, 5 );
	
	wxBoxSizer* bSizer6;
	bSizer6 = new wxBoxSizer( wxHORIZONTAL );
	
	wxStaticText* m_staticText6;
	m_staticText6 = new wxStaticText( _thresholdPanel, wxID_ANY, wxT("Lower Limit"), wxDefaultPosition, wxSize( 60,-1 ), 0 );
	m_staticText6->Wrap( -1 );
	bSizer6->Add( m_staticText6, 0, wxALL, 5 );
	
	_sliderLL = new wxSlider( _thresholdPanel, wxID_ANY, 0, 0, 200, wxDefaultPosition, wxSize( -1,-1 ), wxSL_HORIZONTAL );
	bSizer6->Add( _sliderLL, 1, wxALL|wxEXPAND, 5 );
	
	_textLL = new wxTextCtrl( _thresholdPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 60,-1 ), wxTE_READONLY );
	bSizer6->Add( _textLL, 0, wxALL, 5 );
	
	sbSizer2->Add( bSizer6, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer7;
	bSizer7 = new wxBoxSizer( wxHORIZONTAL );
	
	wxStaticText* m_staticText7;
	m_staticText7 = new wxStaticText( _thresholdPanel, wxID_ANY, wxT("Upper Limit"), wxDefaultPosition, wxSize( 60,-1 ), 0 );
	m_staticText7->Wrap( -1 );
	bSizer7->Add( m_staticText7, 0, wxALL, 5 );
	
	_sliderUL = new wxSlider( _thresholdPanel, wxID_ANY, 0, 0, 400, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL );
	bSizer7->Add( _sliderUL, 1, wxALL|wxEXPAND, 5 );
	
	_textUL = new wxTextCtrl( _thresholdPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 60,-1 ), wxTE_READONLY );
	bSizer7->Add( _textUL, 0, wxALL, 5 );
	
	sbSizer2->Add( bSizer7, 0, wxEXPAND, 5 );
	
	_thresholdPanel->SetSizer( sbSizer2 );
	_thresholdPanel->Layout();
	sbSizer2->Fit( _thresholdPanel );
	_panelNotebook->AddPage( _thresholdPanel, wxT("Threshold"), true, wxNullBitmap );
	_smoothPanel = new wxPanel( _panelNotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxStaticBoxSizer* smoothSizer;
	smoothSizer = new wxStaticBoxSizer( new wxStaticBox( _smoothPanel, wxID_ANY, wxEmptyString ), wxVERTICAL );
	
	wxBoxSizer* bSizer3;
	bSizer3 = new wxBoxSizer( wxHORIZONTAL );
	
	wxStaticText* m_staticText1;
	m_staticText1 = new wxStaticText( _smoothPanel, wxID_ANY, wxT("Relaxation"), wxDefaultPosition, wxSize( 60,-1 ), 0 );
	m_staticText1->Wrap( -1 );
	bSizer3->Add( m_staticText1, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	_sliderRelaxation = new wxSlider( _smoothPanel, wxID_ANY, 0, 0, 50, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL );
	bSizer3->Add( _sliderRelaxation, 2, wxALL|wxEXPAND, 5 );
	
	_textCtrlRelaxation = new wxTextCtrl( _smoothPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 60,-1 ), wxTE_READONLY );
	bSizer3->Add( _textCtrlRelaxation, 0, wxALL|wxEXPAND, 5 );
	
	smoothSizer->Add( bSizer3, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer31;
	bSizer31 = new wxBoxSizer( wxHORIZONTAL );
	
	wxStaticText* m_staticText11;
	m_staticText11 = new wxStaticText( _smoothPanel, wxID_ANY, wxT("Iterations"), wxDefaultPosition, wxSize( 60,-1 ), 0 );
	m_staticText11->Wrap( -1 );
	bSizer31->Add( m_staticText11, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	_sliderIterations = new wxSlider( _smoothPanel, wxID_ANY, 0, 0, 500, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL );
	bSizer31->Add( _sliderIterations, 2, wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	_textCtrlIterations = new wxTextCtrl( _smoothPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 60,-1 ), wxTE_READONLY );
	bSizer31->Add( _textCtrlIterations, 0, wxALL|wxEXPAND, 5 );
	
	smoothSizer->Add( bSizer31, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer32;
	bSizer32 = new wxBoxSizer( wxHORIZONTAL );
	
	wxStaticText* m_staticText12;
	m_staticText12 = new wxStaticText( _smoothPanel, wxID_ANY, wxT("Angle"), wxDefaultPosition, wxSize( 60,-1 ), 0 );
	m_staticText12->Wrap( -1 );
	bSizer32->Add( m_staticText12, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	_sliderAngle = new wxSlider( _smoothPanel, wxID_ANY, 0, 0, 360, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL );
	bSizer32->Add( _sliderAngle, 2, wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );
	
	_textCtrlAngle = new wxTextCtrl( _smoothPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 60,-1 ), wxTE_READONLY );
	bSizer32->Add( _textCtrlAngle, 0, wxALL|wxEXPAND, 5 );
	
	smoothSizer->Add( bSizer32, 0, wxEXPAND, 5 );
	
	_smoothPanel->SetSizer( smoothSizer );
	_smoothPanel->Layout();
	smoothSizer->Fit( _smoothPanel );
	_panelNotebook->AddPage( _smoothPanel, wxT("Smooth"), false, wxNullBitmap );
	_disPanel = new wxPanel( _panelNotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer9;
	bSizer9 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer10;
	bSizer10 = new wxBoxSizer( wxHORIZONTAL );
	
	wxStaticText* m_staticText8;
	m_staticText8 = new wxStaticText( _disPanel, wxID_ANY, wxT("Representation"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText8->Wrap( -1 );
	bSizer10->Add( m_staticText8, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	wxString _dispChoiceChoices[] = { wxT("Surface"), wxT("MapperPoints"), wxT("ThresholdPoints") };
	int _dispChoiceNChoices = sizeof( _dispChoiceChoices ) / sizeof( wxString );
	_dispChoice = new wxChoice( _disPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, _dispChoiceNChoices, _dispChoiceChoices, 0 );
	_dispChoice->SetSelection( 0 );
	bSizer10->Add( _dispChoice, 2, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	bSizer9->Add( bSizer10, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer11;
	bSizer11 = new wxBoxSizer( wxHORIZONTAL );
	
	wxStaticText* m_staticText9;
	m_staticText9 = new wxStaticText( _disPanel, wxID_ANY, wxT("Point Size"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText9->Wrap( -1 );
	bSizer11->Add( m_staticText9, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	_psSpin = new wxSpinCtrl( _disPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 1, 20, 5 );
	bSizer11->Add( _psSpin, 2, wxALL, 5 );
	
	bSizer9->Add( bSizer11, 0, wxEXPAND, 5 );
	
	_interpolationCB = new wxCheckBox( _disPanel, wxID_ANY, wxT("Interpolation [On/Off]"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer9->Add( _interpolationCB, 0, wxALL, 5 );
	
	_disPanel->SetSizer( bSizer9 );
	_disPanel->Layout();
	bSizer9->Fit( _disPanel );
	_panelNotebook->AddPage( _disPanel, wxT("Display"), false, wxNullBitmap );
	
	_sizerMain->Add( _panelNotebook, 1, wxEXPAND | wxALL, 5 );
	
	this->SetSizer( _sizerMain );
	this->Layout();
	
	this->Centre( wxBOTH );
	
	// Connect Events
	this->Connect( wxEVT_SIZE, wxSizeEventHandler( WxVtkViewerAbstractFrame::OnSize ) );
	this->Connect( WxOPEN, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( WxVtkViewerAbstractFrame::ToolOpenClicked ) );
	this->Connect( WxRELOAD, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( WxVtkViewerAbstractFrame::ToolReloadClicked ) );
	this->Connect( WxDELETE, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( WxVtkViewerAbstractFrame::ToolDeleteClicked ) );
	this->Connect( WxRENDER, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( WxVtkViewerAbstractFrame::ToolRenderClicked ) );
	this->Connect( wxRESETCAMERA, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( WxVtkViewerAbstractFrame::ToolResetCameraClicked ) );
	this->Connect( wxPLAY, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( WxVtkViewerAbstractFrame::ToolPlayClicked ) );
	_selectAllCheckBox->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( WxVtkViewerAbstractFrame::SelectAllClicked ), NULL, this );
	_fileCheckList->Connect( wxEVT_COMMAND_LISTBOX_SELECTED, wxCommandEventHandler( WxVtkViewerAbstractFrame::FileItemSelected ), NULL, this );
	_colorChoiceBox->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( WxVtkViewerAbstractFrame::ColorDataChosen ), NULL, this );
	_thresholdChoiceBox->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( WxVtkViewerAbstractFrame::ThresholdDataChosen ), NULL, this );
	_sliderLL->Connect( wxEVT_SCROLL_TOP, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderLLScrolled ), NULL, this );
	_sliderLL->Connect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderLLScrolled ), NULL, this );
	_sliderLL->Connect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderLLScrolled ), NULL, this );
	_sliderLL->Connect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderLLScrolled ), NULL, this );
	_sliderLL->Connect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderLLScrolled ), NULL, this );
	_sliderLL->Connect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderLLScrolled ), NULL, this );
	_sliderLL->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderLLScrolled ), NULL, this );
	_sliderLL->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderLLScrolled ), NULL, this );
	_sliderLL->Connect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderLLScrolled ), NULL, this );
	_sliderUL->Connect( wxEVT_SCROLL_TOP, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderULScrolled ), NULL, this );
	_sliderUL->Connect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderULScrolled ), NULL, this );
	_sliderUL->Connect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderULScrolled ), NULL, this );
	_sliderUL->Connect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderULScrolled ), NULL, this );
	_sliderUL->Connect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderULScrolled ), NULL, this );
	_sliderUL->Connect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderULScrolled ), NULL, this );
	_sliderUL->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderULScrolled ), NULL, this );
	_sliderUL->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderULScrolled ), NULL, this );
	_sliderUL->Connect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderULScrolled ), NULL, this );
	_sliderRelaxation->Connect( wxEVT_SCROLL_TOP, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderRelaxationScrolled ), NULL, this );
	_sliderRelaxation->Connect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderRelaxationScrolled ), NULL, this );
	_sliderRelaxation->Connect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderRelaxationScrolled ), NULL, this );
	_sliderRelaxation->Connect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderRelaxationScrolled ), NULL, this );
	_sliderRelaxation->Connect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderRelaxationScrolled ), NULL, this );
	_sliderRelaxation->Connect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderRelaxationScrolled ), NULL, this );
	_sliderRelaxation->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderRelaxationScrolled ), NULL, this );
	_sliderRelaxation->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderRelaxationScrolled ), NULL, this );
	_sliderRelaxation->Connect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderRelaxationScrolled ), NULL, this );
	_sliderIterations->Connect( wxEVT_SCROLL_TOP, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderIterationsScrolled ), NULL, this );
	_sliderIterations->Connect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderIterationsScrolled ), NULL, this );
	_sliderIterations->Connect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderIterationsScrolled ), NULL, this );
	_sliderIterations->Connect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderIterationsScrolled ), NULL, this );
	_sliderIterations->Connect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderIterationsScrolled ), NULL, this );
	_sliderIterations->Connect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderIterationsScrolled ), NULL, this );
	_sliderIterations->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderIterationsScrolled ), NULL, this );
	_sliderIterations->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderIterationsScrolled ), NULL, this );
	_sliderIterations->Connect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderIterationsScrolled ), NULL, this );
	_sliderAngle->Connect( wxEVT_SCROLL_TOP, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderAngleScrolled ), NULL, this );
	_sliderAngle->Connect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderAngleScrolled ), NULL, this );
	_sliderAngle->Connect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderAngleScrolled ), NULL, this );
	_sliderAngle->Connect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderAngleScrolled ), NULL, this );
	_sliderAngle->Connect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderAngleScrolled ), NULL, this );
	_sliderAngle->Connect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderAngleScrolled ), NULL, this );
	_sliderAngle->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderAngleScrolled ), NULL, this );
	_sliderAngle->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderAngleScrolled ), NULL, this );
	_sliderAngle->Connect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderAngleScrolled ), NULL, this );
	_dispChoice->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( WxVtkViewerAbstractFrame::OnDispChoice ), NULL, this );
	_psSpin->Connect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( WxVtkViewerAbstractFrame::OnPointSize ), NULL, this );
	_interpolationCB->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( WxVtkViewerAbstractFrame::OnInterpolationCB ), NULL, this );
}

WxVtkViewerAbstractFrame::~WxVtkViewerAbstractFrame()
{
	// Disconnect Events
	this->Disconnect( wxEVT_SIZE, wxSizeEventHandler( WxVtkViewerAbstractFrame::OnSize ) );
	this->Disconnect( WxOPEN, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( WxVtkViewerAbstractFrame::ToolOpenClicked ) );
	this->Disconnect( WxRELOAD, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( WxVtkViewerAbstractFrame::ToolReloadClicked ) );
	this->Disconnect( WxDELETE, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( WxVtkViewerAbstractFrame::ToolDeleteClicked ) );
	this->Disconnect( WxRENDER, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( WxVtkViewerAbstractFrame::ToolRenderClicked ) );
	this->Disconnect( wxRESETCAMERA, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( WxVtkViewerAbstractFrame::ToolResetCameraClicked ) );
	this->Disconnect( wxPLAY, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( WxVtkViewerAbstractFrame::ToolPlayClicked ) );
	_selectAllCheckBox->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( WxVtkViewerAbstractFrame::SelectAllClicked ), NULL, this );
	_fileCheckList->Disconnect( wxEVT_COMMAND_LISTBOX_SELECTED, wxCommandEventHandler( WxVtkViewerAbstractFrame::FileItemSelected ), NULL, this );
	_colorChoiceBox->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( WxVtkViewerAbstractFrame::ColorDataChosen ), NULL, this );
	_thresholdChoiceBox->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( WxVtkViewerAbstractFrame::ThresholdDataChosen ), NULL, this );
	_sliderLL->Disconnect( wxEVT_SCROLL_TOP, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderLLScrolled ), NULL, this );
	_sliderLL->Disconnect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderLLScrolled ), NULL, this );
	_sliderLL->Disconnect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderLLScrolled ), NULL, this );
	_sliderLL->Disconnect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderLLScrolled ), NULL, this );
	_sliderLL->Disconnect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderLLScrolled ), NULL, this );
	_sliderLL->Disconnect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderLLScrolled ), NULL, this );
	_sliderLL->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderLLScrolled ), NULL, this );
	_sliderLL->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderLLScrolled ), NULL, this );
	_sliderLL->Disconnect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderLLScrolled ), NULL, this );
	_sliderUL->Disconnect( wxEVT_SCROLL_TOP, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderULScrolled ), NULL, this );
	_sliderUL->Disconnect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderULScrolled ), NULL, this );
	_sliderUL->Disconnect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderULScrolled ), NULL, this );
	_sliderUL->Disconnect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderULScrolled ), NULL, this );
	_sliderUL->Disconnect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderULScrolled ), NULL, this );
	_sliderUL->Disconnect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderULScrolled ), NULL, this );
	_sliderUL->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderULScrolled ), NULL, this );
	_sliderUL->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderULScrolled ), NULL, this );
	_sliderUL->Disconnect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderULScrolled ), NULL, this );
	_sliderRelaxation->Disconnect( wxEVT_SCROLL_TOP, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderRelaxationScrolled ), NULL, this );
	_sliderRelaxation->Disconnect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderRelaxationScrolled ), NULL, this );
	_sliderRelaxation->Disconnect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderRelaxationScrolled ), NULL, this );
	_sliderRelaxation->Disconnect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderRelaxationScrolled ), NULL, this );
	_sliderRelaxation->Disconnect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderRelaxationScrolled ), NULL, this );
	_sliderRelaxation->Disconnect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderRelaxationScrolled ), NULL, this );
	_sliderRelaxation->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderRelaxationScrolled ), NULL, this );
	_sliderRelaxation->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderRelaxationScrolled ), NULL, this );
	_sliderRelaxation->Disconnect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderRelaxationScrolled ), NULL, this );
	_sliderIterations->Disconnect( wxEVT_SCROLL_TOP, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderIterationsScrolled ), NULL, this );
	_sliderIterations->Disconnect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderIterationsScrolled ), NULL, this );
	_sliderIterations->Disconnect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderIterationsScrolled ), NULL, this );
	_sliderIterations->Disconnect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderIterationsScrolled ), NULL, this );
	_sliderIterations->Disconnect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderIterationsScrolled ), NULL, this );
	_sliderIterations->Disconnect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderIterationsScrolled ), NULL, this );
	_sliderIterations->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderIterationsScrolled ), NULL, this );
	_sliderIterations->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderIterationsScrolled ), NULL, this );
	_sliderIterations->Disconnect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderIterationsScrolled ), NULL, this );
	_sliderAngle->Disconnect( wxEVT_SCROLL_TOP, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderAngleScrolled ), NULL, this );
	_sliderAngle->Disconnect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderAngleScrolled ), NULL, this );
	_sliderAngle->Disconnect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderAngleScrolled ), NULL, this );
	_sliderAngle->Disconnect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderAngleScrolled ), NULL, this );
	_sliderAngle->Disconnect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderAngleScrolled ), NULL, this );
	_sliderAngle->Disconnect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderAngleScrolled ), NULL, this );
	_sliderAngle->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderAngleScrolled ), NULL, this );
	_sliderAngle->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderAngleScrolled ), NULL, this );
	_sliderAngle->Disconnect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( WxVtkViewerAbstractFrame::SliderAngleScrolled ), NULL, this );
	_dispChoice->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( WxVtkViewerAbstractFrame::OnDispChoice ), NULL, this );
	_psSpin->Disconnect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( WxVtkViewerAbstractFrame::OnPointSize ), NULL, this );
	_interpolationCB->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( WxVtkViewerAbstractFrame::OnInterpolationCB ), NULL, this );
	
}
