#ifndef _myVTK_h_
#define _myVTK_h_
#include <iostream>

#include "vtkUnsignedCharArray.h"
#include "vtkPointData.h"
#include "vtkUnstructuredGrid.h"
#include "vtkCellData.h"
#include "vtkDataArray.h"
#include "vtkDataSetReader.h"

#include "vtkStructuredGridReader.h"

#include "vtkPolyDataMapper.h"
#include "vtkDataSetMapper.h"
#include "vtkActor.h"
#include "vtkAxesActor.h"
#include "vtkOutlineFilter.h"
#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkCamera.h"
#include "vtkOrientationMarkerWidget.h"
#include "vtkInteractorStyleTrackballCamera.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkProperty.h"

// -- testowanie innych struktur danych --//
#include "vtkUnstructuredGridWriter.h"
#include "vtkUnstructuredGridReader.h"
// -----------------------------------------//
#include "vtkThreshold.h"
#include "vtkThresholdPoints.h"
#include "vtkSmartPointer.h"

#include "vtkSmoothPolyDataFilter.h"
#include "vtkWindowedSincPolyDataFilter.h"
#include "vtkDataSetSurfaceFilter.h"
#include "vtkGeometryFilter.h"
#include "vtkSurfaceReconstructionFilter.h"
#include "vtkDelaunay3D.h"
#include "wxVTKRenderWindowInteractor.h" //TODO:: dodac obsluge wxWidgets

//TODO:: SmartPointers! //

class MJVtk
{
	private:
		double range[2];
		int _activeReader;
		bool _resetCamera;
	
		ofstream _logfile;
		std::vector<vtkDataSetReader*> _dsReaderVector;
		vtkDataSetMapper* _dsMapper;
		vtkPolyDataMapper* _pdMapper;
		vtkPolyDataMapper* _outlineMapper;
		vtkActor* _dataActor;
		vtkActor* _outlineActor;
		vtkAxesActor* _axesActor;
		vtkOrientationMarkerWidget*	_omWidget;
		vtkOutlineFilter* _outlineFilter;
		vtkRenderer* _renderer;
        vtkRenderWindow* _renderWindow;
		vtkRenderWindowInteractor* _renderWindowInteractor; 
        vtkInteractorStyleTrackballCamera* _styleVtk;

		vtkThresholdPoints* _thresholdPoints;
		vtkThreshold* _thresholdBacup;

		vtkSmoothPolyDataFilter* _smoother;
		vtkWindowedSincPolyDataFilter* _smoother2;
		vtkDataSetSurfaceFilter* _dsSurfaceFilter;
		vtkGeometryFilter* _geoFilter;
		vtkDelaunay3D* _delaunay3d;
		
	public:
		MJVtk(){Initialize();}
		~MJVtk(){Delete();}
		void SetVtkRenderWindowInteractor(vtkRenderWindowInteractor* rwinteractor) {_renderWindowInteractor=rwinteractor; _renderWindowInteractor->SetRenderWindow(_renderWindow); _renderWindowInteractor->SetInteractorStyle(_styleVtk);}
		void SetVtkRenderWindowInteractor(wxVTKRenderWindowInteractor* rwinteractor) {_renderWindowInteractor=rwinteractor; _renderWindowInteractor->SetRenderWindow(_renderWindow); _renderWindowInteractor->SetInteractorStyle(_styleVtk);}
		void AddDataReader(std::string filename);
		void CreateVtkPipeLine();
		void DeleteDataReader(int pos);
		void GetDataArraysName(std::vector<std::string>& danvec);
		void SetColorDataName(std::string&);
		void SetThresholdDataName(std::string&);
		void SetActiveReader(int pos);
		void SetSmoothParamters(double rel, int iter, double angle);
		void SetThresholdParamters(double l, double u);
		void SetInterpolation(bool i);
		void GetDataScalarsRange(float &min, float &max);
		void SetPointSize(int);
		void SetRepresentation(int);
		void ResetCamera();
		void Render();
		void UpdateReader(int r);
	private:
		void Initialize();
		void Delete();
};

class MyVtk
{
    public:
        MyVtk();
        void SetWxVTKRenderWindowInteractor(vtkRenderWindowInteractor* _wxVtkIter) { wxVtkIter=_wxVtkIter; wxVtkIter->SetRenderWindow(renWin); wxVtkIter->SetInteractorStyle(styleVtk); }
        void Init();
        void Delete();
        void Run();
        void toStl(std::string _str);
        
        void SetFileName();
        void SetFolder(std::string _str);
        void SetPrefix(std::string _str);
		void Update()
		{
			renderer->Render();
		}

		void SetReader1()
		{
			cout<<"Ustawiam Reader 1\n";
			threshold->SetInputConnection(reader1->GetOutputPort());
		}

		void SetReader2()
		{
			cout<<"Ustawiam Reader 2\n";
			threshold->SetInputConnection(reader2->GetOutputPort());
		}
        
        std::string GetFileName() { return file_name; }
        std::string GetFolder() { return folder; }
        
        //void SetSmooth( float _smooth) { if( smooth    != _smooth) { smooth    = _smooth; b_smooth    = true;} }
        //float GetSmooth()    { return smooth; }
        
private:
		//vtkDataSetReader*	reader;
        vtkStructuredGridReader*	reader;
		vtkStructuredGridReader*	reader1;
		vtkStructuredGridReader*	reader2;
		
        vtkSmoothPolyDataFilter*	smoother;
        vtkDataSetMapper*			mapper;
        vtkActor*                   actor;
		vtkAxesActor*				axesactor;
		vtkOrientationMarkerWidget*	widget;
        vtkOutlineFilter*                   outlineData;
		vtkPolyDataMapper*                  mapOutline;
		vtkActor*                           outline;
        vtkRenderer*                        renderer;
        vtkRenderWindow*                    renWin;
        vtkInteractorStyleTrackballCamera*  styleVtk;
        vtkRenderWindowInteractor*        wxVtkIter; 
        vtkThreshold*			threshold;
        std::string file_name;
        std::string folder;
  
        //float smooth;
};
#endif
