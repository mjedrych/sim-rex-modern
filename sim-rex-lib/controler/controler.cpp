//#include <wx/wx.h>
#include "../../TexTooLib/base_classes.h"
#include "controler.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include "../mcbox/hex_box.hpp"
#include "../mcbox/cube_box.hpp"
#include "../microstructure/microstructure.hpp"
#include "../physics/physics.hpp"

template <typename T>
void get(std::istream& str, std::string& keyvalue, T& value)
{
	str>>keyvalue>>value>>rd2EOL;
}

void get(std::istream& str, std::string& keyvalue, std::string& svalue)
{
	char c;
	str>>keyvalue;
	str.get(c);
	getline(str,svalue);
}

void ReadControler::GetParameters(Controler* c)
{
	_p = &(c->_mp);
}

Controler::Controler()
{
	using namespace GuiParameters;

	_mp.Put(pGLoadModelPath,"");
	_mp.Put(pGSaveModelPath,"");
	_mp.Put(pGWorkingDir,"");
	_mp.Put(pGExecPath,"");
	_mp.Put(pGDebug,1);

	_mp.Put(pMOption,0);
	_mp.Put(pMSaveM,1);
	_mp.Put(pMSaveVtk,1);
	_mp.Put(pMSaveEulers,1);
	_mp.Put(pMSaveMicrostructureFileName,"");
	_mp.Put(pMSaveVtkFileName,"");
	_mp.Put(pMSaveEulersFileName,"");
	_mp.Put(pMLoadMicrostructurePath,"");
	_mp.Put(pMImportAngPath,"");
	_mp.Put(pMGenerateOrientations,1);
	_mp.Put(pMGridType,0);
	_mp.Put(pMGridLength,100);
	_mp.Put(pMGridWidth,100);
	_mp.Put(pMGridHeight,10);
	_mp.Put(pMGrainsNumber,500);
	_mp.Put(pMMaxSpin,1000);
	_mp.Put(pMXRatio,1);
	_mp.Put(pMYRatio,1);
	_mp.Put(pMZRatio,1);
	_mp.Put(pMGradient,1);
	_mp.Put(pMK,1);
	_mp.Put(pMB0,0.1);
	_mp.Put(pMB1,1.0);

	_mp.Put(pSSaveFileName,"");
	_mp.Put(pSMinMis,15);
	_mp.Put(pSMinGS,2);
	_mp.Put(pSGetGrains,0);
	_mp.Put(pSGetSE,0);
	//TODO :: add param for the field Fractions!
	//---- END OF GUI PARAMETERS ----//
	_mp.Put("grain_calculation_by_mis",1);
	//_mp.Put("Theta",15);
	//_mp.Put("GammaMax",1);
}



void Controler::SaveModel()
{
	// TODO -- exception for write failure
	using namespace GuiParameters;
	std::string filename;
	std::string svalue;
	int ivalue;
	float fvalue;
	_mp.Get(pGSaveModelPath,filename);
	std::ofstream f(filename.c_str());
	//if (f==NULL){}//TODO !!!

	f<<"MONTE CARLO MODEL SETTINGS\n";
	f<<"##### GENERAL #####\n";
	_mp.Get(pGLoadModelPath,svalue);
	f<<" "<<pGLoadModelPath<<" "<<svalue<<"\n";
	_mp.Get(pGSaveModelPath,svalue);
	f<<" "<<pGSaveModelPath<<" "<<svalue<<"\n";
	_mp.Get(pGWorkingDir,svalue);
	f<<" "<<pGWorkingDir<<" "<<svalue<<"\n";
	_mp.Get(pGExecPath,svalue);
	f<<" "<<pGExecPath<<" "<<svalue<<"\n";
	_mp.Get(pGDebug,ivalue);
	f<<" "<<pGDebug<<" "<<ivalue<<"\n";

	f<<"##### MICROSTRUCTURE #####\n";
	_mp.Get(pMSaveMicrostructureFileName,svalue);
	f<<" "<<pMSaveMicrostructureFileName<<" "<<svalue<<"\n";
	_mp.Get(pMSaveVtkFileName,svalue);
	f<<" "<<pMSaveVtkFileName<<" "<<svalue<<"\n";
	_mp.Get(pMSaveEulersFileName,svalue);
	f<<" "<<pMSaveEulersFileName<<" "<<svalue<<"\n";
	_mp.Get(pMLoadMicrostructurePath,svalue);
	f<<" "<<pMLoadMicrostructurePath<<" "<<svalue<<"\n";
	_mp.Get(pMImportAngPath,svalue);
	f<<" "<<pMImportAngPath<<" "<<svalue<<"\n";

	_mp.Get(pMOption,ivalue);
	f<<" "<<pMOption<<" "<<ivalue<<"\n";
	_mp.Get(pMSaveM,ivalue);
	f<<" "<<pMSaveM<<" "<<ivalue<<"\n";
	_mp.Get(pMSaveVtk,ivalue);
	f<<" "<<pMSaveVtk<<" "<<ivalue<<"\n";
	_mp.Get(pMSaveEulers,ivalue);
	f<<" "<<pMSaveEulers<<" "<<ivalue<<"\n";
	_mp.Get(pMGenerateOrientations,ivalue);
	f<<" "<<pMGenerateOrientations<<" "<<ivalue<<"\n";
	_mp.Get(pMGridType,ivalue);
	f<<" "<<pMGridType<<" "<<ivalue<<"\n";
	_mp.Get(pMGridLength,ivalue);
	f<<" "<<pMGridLength<<" "<<ivalue<<"\n";
	_mp.Get(pMGridWidth,ivalue);
	f<<" "<<pMGridWidth<<" "<<ivalue<<"\n";
	_mp.Get(pMGridHeight,ivalue);
	f<<" "<<pMGridHeight<<" "<<ivalue<<"\n";
	_mp.Get(pMGrainsNumber,ivalue);
	f<<" "<<pMGrainsNumber<<" "<<ivalue<<"\n";
	_mp.Get(pMMaxSpin,ivalue);
	f<<" "<<pMMaxSpin<<" "<<ivalue<<"\n";
	_mp.Get(pMXRatio,ivalue);
	f<<" "<<pMXRatio<<" "<<ivalue<<"\n";
	_mp.Get(pMYRatio,ivalue);
	f<<" "<<pMYRatio<<" "<<ivalue<<"\n";
	_mp.Get(pMZRatio,ivalue);
	f<<" "<<pMZRatio<<" "<<ivalue<<"\n";
	_mp.Get(pMGradient,ivalue);
	f<<" "<<pMGradient<<" "<<ivalue<<"\n";
	_mp.Get(pMK,ivalue);
	f<<" "<<pMK<<" "<<ivalue<<"\n";

	_mp.Get(pMB0,fvalue);
	f<<" "<<pMB0<<" "<<fvalue<<"\n";
	_mp.Get(pMB1,fvalue);
	f<<" "<<pMB1<<" "<<fvalue<<"\n";

	f<<"##### STATISTICS #####\n";
	_mp.Get(pSSaveFileName,svalue);
	f<<" "<<pSSaveFileName<<" "<<svalue<<"\n";
	_mp.Get(pSMinMis,ivalue);
	f<<" "<<pSMinMis<<" "<<ivalue<<"\n";
	_mp.Get(pSMinGS,ivalue);
	f<<" "<<pSMinGS<<" "<<ivalue<<"\n";
	_mp.Get(pSGetGrains,ivalue);
	f<<" "<<pSGetGrains<<" "<<ivalue<<"\n";
	_mp.Get(pSGetSE,ivalue);
	f<<" "<<pSGetSE<<" "<<ivalue<<"\n";

	f.close();

}

void Controler::LoadModel()
{

	using namespace GuiParameters;
	using namespace std;
	string buffer;
	string filename;
	string keyvalue;
	string svalue;
	int ivalue;
	float fvalue;
	_mp.Get(pGLoadModelPath,filename);

	fstream f;
	f.open(filename.c_str(),ios::in);
	if (!f.is_open())
		std::cout << "Problem z otwarciem pliku modelu\n";
	else
		std::cout << "Wczytano model "<<filename<<"\n";
	getline(f,buffer);
	getline(f,buffer);

//------ general ------//
	for (int i=0;i<4;++i)
	{
		get(f,keyvalue,svalue);
		_mp.Set(keyvalue,svalue);
	}
	//DEBUG
	get(f,keyvalue,ivalue);
	_mp.Set(keyvalue,ivalue);
//--------------------------------//
	getline(f,buffer);
//------- microstructure ---------//
	for (int i=0;i<5;++i)
	{
		get(f,keyvalue,svalue);
		_mp.Set(keyvalue,svalue);
	}

	for (int i=0;i<16;++i)
	{
		get(f,keyvalue,ivalue);
		_mp.Set(keyvalue,ivalue);
	}

	for (int i=0;i<2;++i)
	{
		get(f,keyvalue,fvalue);
		_mp.Set(keyvalue,fvalue);
	}

//----------------------------//
	getline(f,buffer);
//-------- statistics --------//
	for (int i=0;i<1;++i)
	{
		get(f,keyvalue,svalue);
		_mp.Set(keyvalue,svalue);
	}
	for (int i=0;i<4;++i)
	{
		get(f,keyvalue,ivalue);
		_mp.Set(keyvalue,ivalue);
	}
}
void Controler::RunModel()
{
	using namespace GuiParameters;
	std::string svalue;
	int ivalue;

	MCBox* box;
	ReadControler readctrl(this);
	_mp.Get(pMOption,ivalue);
	switch (ivalue)
	{
	case 0: //GENERATE MICROSTRUCTURE
		int x,y,z;
		_mp.Get(pMGridType,ivalue);
		_mp.Get(pMGridLength,x);
		_mp.Get(pMGridWidth,y);
		_mp.Get(pMGridHeight,z);

		//utworzenie obiektu mcbox//
		switch (ivalue)
		{
		case 0: //Isotropic 3D
			box=new Iso3D(x,y,z);
			break;
		case 1: //CUBE 3D
			//box=new Cube3D(x,y,z); :: TODO
			break;
		case 2: //HEXAGONAL 2D
			box=new Hex2D(x,y);
			break;
		}

		_mp.Set("grain_calculation_by_mis",0);
		/* testing of microstructure generation*/
		//CoaersingTest micro3d_test(box, readctrl);
		//  micro3d_test.GenerateTest();
		/**************************************************/
		SpinMicrostructureBuilder mic(box,readctrl);
		mic.Generate();
		break;
	}
	box->ExportVtk("mic.vtk");
	box->ExportSPVtk("mic.sp.vtk");
	box->ExportEulers("mic.eul");
	
	OldPhysicsModel physx(box,readctrl);
	physx.DoNucleation();
	physx.Recrystallization();
	
	box->ExportEulers("rec.eul");
	box->ExportVtk("rec.vtk");
	
	if (box!=NULL)
		delete box;
}
