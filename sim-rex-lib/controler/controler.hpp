#ifndef CONTROLER_HPP
#define CONTROLER_HPP
#include "../base/parameters.hpp"
#include "../mcbox/abstract_box.hpp"

/**** GUI AND MODEL PARAMETERS ****/
using std::string;
typedef const string Key;

namespace GuiParameters
{
	///-- GENERAL --//
	Key pGDebug="DEBUG"; // debug_state: ON=1, OFF=0
	Key pGSaveModelPath="SAVE_MODEL";
	Key pGLoadModelPath="LOAD_MODEL";
	Key pGWorkingDir="WORKING_DIRECTORY";
	Key pGExecPath="EXEC_PATH";

	///-- MICROSTRUCTURE --//
	Key pMOption="MICROSTRUCTURE_OPTION"; //microstructure option:Generate=0, Load=1, ImportAng=2
	Key pMSaveM="SAVE_MICROSTRUCTURE_TO_FILE";
	Key pMSaveVtk="EXPORT_MICROSTRUCTURE_TO_PARAVIEW";
	Key pMSaveEulers="EXPORT_EULER_ANGLES_TO_FILE";
	Key pMSaveMicrostructureFileName="SAVE_MIC_FILE";
	Key pMSaveVtkFileName="VTK_FILE";
	Key pMSaveEulersFileName="EULERS_FILE";
	Key pMLoadMicrostructurePath="LOAD_MIC_FILE";
	Key pMImportAngPath="IMPORT_ANG_FILE";
	Key pMGenerateOrientations="GENERATE_ORIENTATIONS";
	Key pMGridType="GRID_TYPE";
	Key pMGridLength="GRID_LENGTH(X)";
	Key pMGridWidth="GRID_WIDTH(Y)";
	Key pMGridHeight="GRID_HEIGHT(Z)";
	Key pMGrainsNumber="NUMBER_OF_GRAINS";
	Key pMMaxSpin="MAX_SPIN";
	Key pMXRatio="X_RATIO";
	Key pMYRatio="Y_RATIO";
	Key pMZRatio="Z_RATIO";
	Key pMGradient="USE_GRADIENT";
	Key pMK="GRADIENT_K";
	Key pMB0="GRADIENT_B0";
	Key pMB1="GRADIENT_B1";
	//symetric -- not implemented
		// int parameters=16
		// float parameters=2
		// string parameters=5


	//-- STATISTICS --/
	Key pSSaveFileName="STATISTICS_FILE";
	Key pSMinMis="MINIMAL_MISORIENTATION";
	Key pSMinGS="MINIMAL_GRAIN_SIZE";
	Key pSGetGrains="GRAINS_STATISTICS";
	Key pSGetSE="STORED_ENERGY_STATISTICS";
};

class Controler;

class ReadControler
{
    private:
        Parameters* _p;

    public:
        void GetParameters(Controler* c);

    public:
        ReadControler(){}
        ReadControler(Controler* c){GetParameters(c);}
        ~ReadControler(){}

        template<typename T>
            void Get(const std::string& name,  T& value){_p->Get(name,value);}

};

class Controler
{
    protected:
       Parameters _mp;  // model parameters

    public:
          Controler();
		  //Controler(const std::string& model);
          ~Controler(){}
		void SaveModel();
		void LoadModel();
		void RunModel();

        template <typename T>
        void Put(const std::string& name, const T& value){_mp.Put(name,value);}

        template<typename T>
        void Get(const std::string& name,  T& value){_mp.Get(name,value);}

        template<typename T>
        void Set(const std::string& name, const T& value){_mp.Set(name,value);}

        friend void ReadControler::GetParameters(Controler*);
};

#endif // CONTROLER_HPP
