#set terminal postscript eps \
#    leveldefault color colortext \
#    dashed dashlength 3.0 linewidth 1.0 rounded \
#	"Helvetica" 16 
#set term png size 800, 600 font "arial" 16
#set output 's.eps'
#set xrange [0.2:2]
#set yrange [0:60]
#set xrange [0:90]
#set yrange [0:80]
#set xlabel "FI [degrees]" 0.0, 0.2
#set ylabel "" 2.0
#set key right top
 

set style line 1  linetype 1 linecolor rgb "blue"  linewidth 2.500 pointtype 1 pointsize default 
set style line 2  linetype 1 linecolor rgb "green"  linewidth 2.500 pointtype 1 pointsize default 
set style line 3  linetype 1 linecolor rgb "red"  linewidth 2.500 pointtype 3 pointsize default 
set style line 5  linetype 1 linecolor rgb "dark-goldenrod"  linewidth 2.500 pointtype 1 pointsize default 
set style line 6  linetype 3 linecolor rgb "dark-blue"  linewidth 2.500 pointtype 1 pointsize default 
set style line 4  linetype 4 linecolor rgb "black"  linewidth 2.5
set style line 7  linetype 4 linecolor rgb "grey"  linewidth 2.5
set style line 10  linetype 1 linecolor rgb "black"  linewidth 2.500 pointtype 1 pointsize default 

f(x)=a*x+b;
g(x)=c*x+d;
fit f(x) "Output/jmak.txt" u 3:4 via a,b
fit [:][-2.15:0.85] g(x) "Output/jmak.txt" u 3:4 via c,d
#set label 1 sprintf("n = %3.4f", a) at first 1, first 1 font "Helvetica,16"
set key left top
set multiplot
plot "Output/jmak.txt" using 3:4 ti "JMAK" ps 2, f(x) ti sprintf("n = %3.4f", a), g(x) ti sprintf("n = %3.4f", c)

# Now we set the options for the smaller plot
set size 0.5,0.5
set origin 0.45,0.1
set title 'Zoom'
set yrange [-2.15:0.85]
plot "Output/jmak.txt" u 3:4 ti "JMAK zoom" ps 2, g(x) ls 1

unset multiplot

#plot "../0.txt" using 1:2 ls 1 smooth csplines ti "0_50bin", \
#	 "../1.txt" using 1:2 ls 2 smooth csplines ti "1_50bin", \
#	 "../2.txt" using 1:2 ls 3 smooth csplines ti "2_50bin", \
#	 "../3.txt" using 1:2 ls 5 smooth csplines ti "3_50bin", \
#	 "../4.txt" using 1:2 ls 10 smooth csplines ti "4_50bin"
#unset output