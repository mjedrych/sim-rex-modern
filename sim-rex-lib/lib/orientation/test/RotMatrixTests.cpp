#include "gtest/gtest.h"
#include <Orientation/Orientation.h>
#include <cstdlib>
#include <exception>
#include <iostream>
#include <random>

using namespace Orientation;

TEST(RotMatrix, PlusNonZero) {
  RotMatrix<double> rm(0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0);
  RotMatrix<double> rm2 =
      rm + RotMatrix<double>(0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0);
  EXPECT_FALSE(rm == rm2);
}

TEST(RotMatrix, PlusNonZero2) {
  RotMatrix<double> rm(0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0);
  RotMatrix<double> rm2 =
      rm + RotMatrix<double>(0.01, 0.01, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
  EXPECT_FALSE(rm == rm2);
}

TEST(RotMatrix, MinusNonZero) {
  RotMatrix<double> rm(0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0);
  EXPECT_FALSE(rm == rm - rm);
}

TEST(RotMatrix, MinusZero) {
  RotMatrix<double> rm(0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0);
  EXPECT_TRUE(rm == rm - RotMatrix<double>());
}

TEST(RotMatrix, PlusZero) {
  RotMatrix<double> rm(0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0);
  EXPECT_TRUE(rm == rm + RotMatrix<double>());
}

TEST(RotMatrix, MultiplyByZero) {
  RotMatrix<double> rm(0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0);
  EXPECT_TRUE(rm * 0.0 == RotMatrix<double>());
}

TEST(RotMatrix, Multiply) {
  RotMatrix<double> rm1(0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0);
  RotMatrix<double> rm2(0.0, 2.0, 0.0, 0.0, 0.0, 2.0, 2.0, 0.0, 0.0);
  EXPECT_TRUE(rm1 * 2.0 == rm2);
}

TEST(RotMatrix, Multiply2) {
  const int multiplier = 6;
  BungeAngles<double> ba_in(0.0, 0.0, 10.0);
  BungeAngles<double> ba_out(0.0, 0.0, multiplier * 10.0);

  RotMatrix<double> r_in = Converter::BungeAnglesToRotMatrix(ba_in);
  auto r_in_copy = r_in;
  RotMatrix<double> r_out = Converter::BungeAnglesToRotMatrix(ba_out);

  for (auto i = 1; i < multiplier; ++i) {

    r_in = r_in_copy * r_in;
  }

  EXPECT_TRUE(r_out == r_in);
}

TEST(RotMatrix, NumericOperators) {
  RotMatrix<double> rm1(1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0);
  EXPECT_TRUE(rm1 * 3.0 == rm1 + rm1 * 3.0 - rm1);
}

TEST(RotMatrix, TransposeSelf) {
  RotMatrix<double> rm(0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0);
  RotMatrix<double> rm_copy(0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0);
  RotMatrix<double> one(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0);
  rm.transpose();
  RotMatrix<double> rm3 = rm_copy * rm;
  EXPECT_TRUE(one == rm3);
}

TEST(RotMatrix, ZeroMatrix) {
  std::cout << "this is zero matrix \n";
  std::cout << RotMatrix<double>();
}

TEST(RotMatrix, GetOperator1) {
  // EXPECT_THROW(RotMatrix<double>()(1, 10), std::out_of_range);
}

TEST(RotMatrix, GetOperator2) { EXPECT_NO_THROW(RotMatrix<double>()(1, 1)); }

TEST(RotMatrix, GetOperator3) {
  RotMatrix<double> r;
  r(1, 1) = 2.0;
  EXPECT_FLOAT_EQ(r(1, 1), 2.0);
}

TEST(RotMatrix, Determinant1) {
  BungeAngles<double> ba(45.0, 0.0, 0.0);
  RotMatrix<double> r = Converter::BungeAnglesToRotMatrix<double>(ba);
  EXPECT_FLOAT_EQ(r.det(), 1.0);
  r = r * r;
  EXPECT_FLOAT_EQ(r.det(), 1.0);
  r = r * r * r;
  EXPECT_FLOAT_EQ(r.det(), 1.0);
}

TEST(RotMatrix, Determinant2) {
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<> distrib(1, 360.0);
  for (auto i = 0; i < 1000; ++i) {
    auto ba = BungeAngles<double>(distrib(gen), distrib(gen), distrib(gen));
    auto m = Converter::BungeAnglesToRotMatrix(ba);
    EXPECT_NEAR(m.det(), 1.0, 0.0001);
  }
}

TEST(RotMatrix, Determinant3) {
  CrystalSymmetry cs;
  for (auto m : cs.getSymmetries<RotMatrix<double>>()) {
    EXPECT_FLOAT_EQ(m.det(), 1.0);
  }

  cs.setActiveSymmetry(CrystalSymmetry::SymmetryList::HEXAGONAL);
  for (auto m : cs.getSymmetries<RotMatrix<double>>())
    EXPECT_FLOAT_EQ(m.det(), 1.0);
}
// ASSERT_DEATH(RotMatrix<float>(), "");
// EXPECT_EXIT(, ::testing::ExitedWithCode(EXIT_FAILURE), "");