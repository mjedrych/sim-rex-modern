#include "gtest/gtest.h"
#include <Orientation/Orientation.h>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <random>
using namespace Orientation;
TEST(Converter, Transitions) {
  auto ba = BungeAngles<double>(203.9, 160.0, 336);
  std::cout << Converter::BungeAnglesToQuaternion(ba);
  std::cout << Converter::BungeAnglesToRotMatrix(ba);
  std::cout << Converter::BungeAnglesToQuaternionIndirect(ba);
}

TEST(Converter, Fi_Above_180) {
  BungeAngles<double> ba(74.0, 280.0, 60.0);
  auto ba2 =
      Converter::RotMatrixToBungeAngles(Converter::BungeAnglesToRotMatrix(ba));
  std::cout << ba;
  std::cout << ba2;
  EXPECT_FALSE(ba.getPhi() == ba2.getPhi());
}

TEST(Converter, Fi_180) {
  BungeAngles<double> ba(30.0, 180.0, 60.0);
  auto ba2 =
      Converter::RotMatrixToBungeAngles(Converter::BungeAnglesToRotMatrix(ba));
  std::cout << ba;
  std::cout << ba2;
  EXPECT_TRUE(ba.getPhi() == ba2.getPhi());
}

TEST(Converter, Fi_Below_180) {
  BungeAngles<double> ba(30.0, 120.0, 60.0);
  auto ba2 =
      Converter::RotMatrixToBungeAngles(Converter::BungeAnglesToRotMatrix(ba));
  std::cout << ba;
  std::cout << ba2;
  EXPECT_TRUE(ba == ba2);
}

TEST(Converter, Fi_Near_180_RotMatrix) {
  BungeAngles<double> ba(30.0, 179.8, 60.0);
  std::cout << "SIN FI 179.8 = " << std::sin(179.8 / 180.0 * M_PI) << "\n";
  std::cout << Converter::BungeAnglesToRotMatrix(ba);
  auto ba2 =
      Converter::RotMatrixToBungeAngles(Converter::BungeAnglesToRotMatrix(ba));
  std::cout << ba;
  std::cout << ba2;
  EXPECT_FLOAT_EQ(ba.getPhi(), ba2.getPhi());

  ba = BungeAngles<double>(30.0, 179.9, 60.0);
  std::cout << "SIN FI 179.9 = " << std::sin(179.9 / 180.0 * M_PI) << "\n";
  std::cout << Converter::BungeAnglesToRotMatrix(ba);
  ba2 =
      Converter::RotMatrixToBungeAngles(Converter::BungeAnglesToRotMatrix(ba));
  std::cout << ba;
  std::cout << ba2;
  EXPECT_FLOAT_EQ(ba.getPhi(), ba2.getPhi());

  // inverted Fi
  ba = BungeAngles<double>(30.0, 180.001, 60.0);
  ba2 =
      Converter::RotMatrixToBungeAngles(Converter::BungeAnglesToRotMatrix(ba));
  std::cout << ba;
  std::cout << ba2;
  EXPECT_NEAR(ba.getPhi(), ba2.getPhi(), 0.01);
}

TEST(Converter, Fi_Near_180_Quaternion) {
  BungeAngles<double> ba(30.0, 179.8, 60.0);
  std::cout << "SIN FI 179.8 = " << std::sin(179.8 / 180.0 * M_PI) << "\n";
  auto ba2 = Converter::QuaternionToBungeAngles(
      Converter::BungeAnglesToQuaternion(ba));
  std::cout << ba;
  std::cout << ba2;
  EXPECT_FLOAT_EQ(ba.getPhi(), ba2.getPhi());

  ba = BungeAngles<double>(30.0, 179.9, 60.0);
  std::cout << "SIN FI 179.9 = " << std::sin(179.9 / 180.0 * M_PI) << "\n";
  ba2 = Converter::QuaternionToBungeAngles(
      Converter::BungeAnglesToQuaternion(ba));
  std::cout << ba;
  std::cout << ba2;
  EXPECT_FLOAT_EQ(ba.getPhi(), ba2.getPhi());

  // inverted Fi
  ba = BungeAngles<double>(30.0, 180.001, 60.0);
  ba2 = Converter::QuaternionToBungeAngles(
      Converter::BungeAnglesToQuaternion(ba));
  std::cout << ba;
  std::cout << ba2;
  EXPECT_NEAR(ba.getPhi(), ba2.getPhi(), 0.01);
}

TEST(Converter, TransitionsCubic) {
  CrystalSymmetry cs;
  auto symmetriesR = cs.getSymmetriesAsMatrix();
  auto symmetriesQ = cs.getSymmetriesAsQuaternion();

  for (auto i = 0; i < symmetriesQ.size(); ++i) {
    auto ba = Converter::RotMatrixToBungeAngles(symmetriesR[i]);
    if (Converter::BungeAnglesToQuaternion(ba) == symmetriesQ[i]) {
    } else {
      std::cout << ba;
      std::cout << symmetriesQ[i];
      std::cout << Converter::BungeAnglesToQuaternion(ba);
      std::cout << Converter::BungeAnglesToQuaternionIndirect(ba);
    }
    EXPECT_TRUE(symmetriesQ[i] == Converter::BungeAnglesToQuaternion(ba));
    EXPECT_TRUE(symmetriesQ[i] ==
                Converter::BungeAnglesToQuaternionIndirect(ba));
  }

  for (auto i = 0; i < symmetriesQ.size(); ++i) {
    auto ba = Converter::QuaternionToBungeAngles(symmetriesQ[i]);
    EXPECT_TRUE(symmetriesR[i] == Converter::BungeAnglesToRotMatrix(ba));
  }
}

TEST(Converter, TransitionsHex) {
  CrystalSymmetry cs;
  cs.setActiveSymmetry(CrystalSymmetry::SymmetryList::HEXAGONAL);
  auto symmetriesR = cs.getSymmetriesAsMatrix();
  auto symmetriesQ = cs.getSymmetriesAsQuaternion();

  for (auto i = 0; i < symmetriesQ.size(); ++i) {
    auto ba = Converter::RotMatrixToBungeAngles(symmetriesR[i]);
    if (Converter::BungeAnglesToQuaternion(ba) == symmetriesQ[i]) {
    } else {
      std::cout << ba;
      std::cout << symmetriesQ[i];
      std::cout << Converter::BungeAnglesToQuaternion(ba);
      // std::cout << Converter::BungeAnglesToQuaternionW(ba);
    }
    EXPECT_TRUE(symmetriesQ[i] == Converter::BungeAnglesToQuaternion(ba));
    EXPECT_TRUE(symmetriesQ[i] ==
                Converter::BungeAnglesToQuaternionIndirect(ba));
  }

  for (auto i = 0; i < symmetriesQ.size(); ++i) {
    auto ba = Converter::QuaternionToBungeAngles(symmetriesQ[i]);
    EXPECT_TRUE(symmetriesR[i] == Converter::BungeAnglesToRotMatrix(ba));
  }
}

TEST(Converter, BungeAnglesToQuaternion_MidFi) {
  std::cout << std::fixed << std::setprecision(5);
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<> distrib(15.0, 170.0);
  for (auto i = 0; i < 100000; ++i) {
    auto ba = BungeAngles<double>(2.0 * distrib(gen), distrib(gen),
                                  2.0 * distrib(gen));
    auto q1 = Converter::BungeAnglesToQuaternion(ba);
    auto q2 = Converter::BungeAnglesToQuaternionZXZ(ba);
    if (q1 == q2) {
    } else {
      std::cout << q1;
      std::cout << q2;
      std::cout << ba;
      std::cout << Converter::QuaternionToBungeAngles(q1);
      std::cout << Converter::QuaternionToBungeAngles(q2) << "\n";
    }

    if (ba == Converter::QuaternionToBungeAngles(q1)) {
    } else {
      std::cout << ba;
      std::cout << Converter::QuaternionToBungeAngles(q1);
      std::cout << Converter::QuaternionToBungeAngles(q2) << "\n";
    }

    if (ba == Converter::QuaternionToBungeAngles(q2)) {
    } else {
      std::cout << ba;
      std::cout << Converter::QuaternionToBungeAngles(q1);
      std::cout << Converter::QuaternionToBungeAngles(q2) << "\n";
    }
    EXPECT_TRUE(q1 == q2);
    EXPECT_TRUE(ba == Converter::QuaternionToBungeAngles(q1));
    EXPECT_TRUE(ba == Converter::QuaternionToBungeAngles(q2));
  }
}

TEST(Converter, BungeAnglesToQuaternion_LowFi) {
  std::cout << std::fixed << std::setprecision(5);
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<> distrib(5.0, 15.0);
  for (auto i = 0; i < 1000; ++i) {
    auto ba = BungeAngles<long double>(2.0 * distrib(gen), distrib(gen),
                                       2.0 * distrib(gen));
    auto q1 = Converter::BungeAnglesToQuaternion(ba);
    auto q2 = Converter::BungeAnglesToQuaternionIndirect(ba);
    if (q1 == q2) {
    } else {
      std::cout << q1;
      std::cout << q2;
      std::cout << ba;
      std::cout << Converter::QuaternionToBungeAngles(q1);
      std::cout << Converter::QuaternionToBungeAngles(q2) << "\n";
    }

    if (ba == Converter::QuaternionToBungeAngles(q1)) {
    } else {
      std::cout << ba;
      std::cout << Converter::QuaternionToBungeAngles(q1);
      std::cout << Converter::QuaternionToBungeAngles(q2) << "\n";
    }

    if (ba == Converter::QuaternionToBungeAngles(q2)) {
    } else {
      std::cout << ba;
      std::cout << Converter::QuaternionToBungeAngles(q1);
      std::cout << Converter::QuaternionToBungeAngles(q2) << "\n";
    }
    EXPECT_TRUE(q1 == q2);
    EXPECT_TRUE(ba == Converter::QuaternionToBungeAngles(q1));
    EXPECT_TRUE(ba == Converter::QuaternionToBungeAngles(q2));
  }
}
