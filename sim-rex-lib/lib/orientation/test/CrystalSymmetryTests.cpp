#include "gtest/gtest.h"
#include <Orientation/Orientation.h>
#include <iostream>

using namespace Orientation;

TEST(CrystalSymmetry, IsEqualCube1) {
  CrystalSymmetry cs;
  auto symmetriesR = cs.getSymmetriesAsMatrix();
  for (auto i = 0; i < symmetriesR.size() - 1; ++i) {
    for (auto j = i + 1; j < symmetriesR.size(); ++j) {
      EXPECT_FALSE(symmetriesR[i] == symmetriesR[j]);
    }
  }

  auto symmetriesQ = cs.getSymmetriesAsQuaternion();
  for (auto i = 0; i < symmetriesQ.size() - 1; ++i) {
    for (auto j = i + 1; j < symmetriesQ.size(); ++j) {
      EXPECT_FALSE(symmetriesQ[i].theSameAs(symmetriesQ[j]));
    }
  }

  for (auto i = 0; i < symmetriesQ.size(); ++i) {
    EXPECT_TRUE(symmetriesQ[i] ==
                Converter::RotMatrixToQuaternion(symmetriesR[i]));
  }

  for (auto i = 0; i < symmetriesQ.size(); ++i) {
    EXPECT_TRUE(symmetriesR[i] ==
                Converter::QuaternionToRotMatrix(symmetriesQ[i]));
  }
}

TEST(CrystalSymmetry, IsEqualHex1) {
  CrystalSymmetry cs;
  cs.setActiveSymmetry(CrystalSymmetry::SymmetryList::HEXAGONAL);

  auto symmetriesR = cs.getSymmetriesAsMatrix();
  for (auto i = 0; i < symmetriesR.size() - 1; ++i) {
    for (auto j = i + 1; j < symmetriesR.size(); ++j) {
      EXPECT_FALSE(symmetriesR[i] == symmetriesR[j]);
    }
  }

  auto symmetriesQ = cs.getSymmetriesAsQuaternion();
  for (auto i = 0; i < symmetriesQ.size() - 1; ++i) {
    for (auto j = i + 1; j < symmetriesQ.size(); ++j) {
      EXPECT_FALSE(symmetriesQ[i].theSameAs(symmetriesQ[j]));
    }
  }

  for (auto i = 0; i < symmetriesQ.size(); ++i) {
    EXPECT_TRUE(symmetriesQ[i] ==
                Converter::RotMatrixToQuaternion(symmetriesR[i]));
  }

  for (auto i = 0; i < symmetriesQ.size(); ++i) {
    EXPECT_TRUE(symmetriesR[i] ==
                Converter::QuaternionToRotMatrix(symmetriesQ[i]));
  }
}

TEST(CrystalSymmetry, IsEqualCube2) {
  CrystalSymmetry cs;
  for (auto s : cs.getSymmetriesAsMatrix()) {
    // std::cout << s;
    std::cout << Converter::RotMatrixToQuaternion(s);
    // std::cout << Converter::RotMatrixToBungeAngles(s);
    // std::cout <<
    // Converter::QuaternionToBungeAngles(Converter::RotMatrixToQuaternion(s))
    // <<
    // "\n";
  }
}

TEST(CrystalSymmetry, IsEqualHex2) {
  CrystalSymmetry cs;
  cs.setActiveSymmetry(CrystalSymmetry::SymmetryList::HEXAGONAL);
  for (auto s : cs.getSymmetriesAsMatrix()) {
    // std::cout << s;
    std::cout << Converter::RotMatrixToQuaternion(s);
    // std::cout <<
    // Converter::QuaternionToBungeAngles(Converter::RotMatrixToQuaternion(s))
    // <<
    // "\n";
  }
}

TEST(CrystalSymmetry, IsEqualHex) {
  CrystalSymmetry cs;
  cs.setActiveSymmetry(CrystalSymmetry::SymmetryList::HEXAGONAL);
  for (auto s : cs.getSymmetries<Quaternion<double>>()) {
    std::cout << s << "\n";
    std::cout << Converter::QuaternionToRotMatrix(s);
    std::cout << Converter::RotMatrixToBungeAngles(
                     Converter::QuaternionToRotMatrix(s))
              << "\n";
  }

  for (auto s : cs.getSymmetries<RotMatrix<double>>()) {
    std::cout << s << "\n";
    std::cout << Converter::RotMatrixToBungeAngles(s);
  }
}