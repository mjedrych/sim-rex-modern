#include "gtest/gtest.h"
#include <Orientation/CalculatorLegacy.h>
#include <Orientation/Orientation.h>
using namespace Orientation;
TEST(Calculator, MinMisorientationAngle) {
  CrystalSymmetryLegacy CSLegacy;
  BungeAngles<double> ba(88.0, 10.0, 10.0);
  auto q = Converter::BungeAnglesToQuaternion(ba);
  auto q2 = Quaternion<double>();
  auto omegaL = CalculatorLegacy::minMisoAngle(q, q2, CSLegacy);
  auto omega = Calculator::minMisoAngle(q, q2, CS);
  EXPECT_FLOAT_EQ(omegaL, omega);
}