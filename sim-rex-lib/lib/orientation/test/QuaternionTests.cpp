#include "gtest/gtest.h"
#include <Orientation/Orientation.h>
#include <iostream>

using namespace Orientation;

TEST(Quaternion, IsEqual) {
  Quaternion<double> rm;
  Quaternion<double> rm2 = rm;
  EXPECT_TRUE(rm == rm2);
}

TEST(Quaternion, IsNotEqual) {
  Quaternion<double> rm;
  EXPECT_FALSE(rm == rm * rm);
}

TEST(Quaternion, Multiply1) {
  Quaternion<double> q1(0.0, 0.0, 0.0, 1.0);
  Quaternion<double> q2(std::sqrt(2.0) / 2.0, 0.0, 0.0, std::sqrt(2.0) / 2.0);
  q2 = q2 * q2;
  EXPECT_TRUE(q1 == q2);
  EXPECT_TRUE(q1 * q1 == q2 * q2);
  EXPECT_TRUE(q1 * q2 == q2 * q1);
}

TEST(Quaternion, Multiply2) {
  Quaternion<double> one(1.0, 0.0, 0.0, 0.0);
  EXPECT_TRUE(one == one * one * one * one * one);
}

TEST(Quaternion, Multiply3) {
  const int multiplier = 3;
  BungeAngles<double> ba_in(0.0, 0.0, 15.0);
  BungeAngles<double> ba_out(0.0, 0.0, multiplier * 15.0);
  Converter oc;
  Quaternion<double> q_in = oc.BungeAnglesToQuaternion(ba_in);
  auto q_in_copy = q_in;
  Quaternion<double> q_out = oc.BungeAnglesToQuaternion(ba_out);

  for (auto i = 1; i < multiplier; ++i) {

    q_in = q_in * q_in_copy;
  }
  EXPECT_TRUE(q_out == q_in);
}

TEST(Quaternion, TransposeSelf) {
  Quaternion<double> q(0.5, 0.5, 0.5, 0.5);
  Quaternion<double> q_copy = q;
  Quaternion<double> one(1.0, 0.0, 0.0, 0.0);
  q.transpose();
  EXPECT_TRUE(one == q_copy * q);
  EXPECT_TRUE(one == q * q_copy);
}

TEST(Quaternion, TransposedCopy) {
  Quaternion<double> q(0.5, 0.5, 0.5, 0.5);
  Quaternion<double> one(1.0, 0.0, 0.0, 0.0);
  EXPECT_TRUE(one == q.transposedCopy() * q);
  EXPECT_TRUE(one == q * q.transposedCopy());
}

TEST(Quaternion, Misorientation) {
  Quaternion<double> q =
      Converter::BungeAnglesToQuaternion(BungeAngles<double>(30.0, 0.0, 15.0));
  EXPECT_FLOAT_EQ(45.0, q.getMisorientationInDeg());
}

// ASSERT_DEATH(Quaternion<float>(), "");
// EXPECT_EXIT(, ::testing::ExitedWithCode(EXIT_FAILURE), "");