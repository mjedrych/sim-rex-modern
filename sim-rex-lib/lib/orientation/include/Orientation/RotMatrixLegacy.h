#pragma once
#include <iostream>

namespace Orientation {
template <typename T> class RotMatrixLegacy {
public:
  RotMatrixLegacy() { zero(); }
  RotMatrixLegacy(T m00, T m01, T m02, T m10, T m11, T m12, T m20, T m21,
                  T m22) {
    aa[0][0] = m00;
    aa[0][1] = m01;
    aa[0][2] = m02;
    aa[1][0] = m10;
    aa[1][1] = m11;
    aa[1][2] = m12;
    aa[2][0] = m20;
    aa[2][1] = m21;
    aa[2][2] = m22;
  }
  T &operator()(int i, int j) { return aa[i][j]; };
  constexpr bool operator==(const RotMatrixLegacy &rhs) const {
    for (auto i = 0; i < 3; ++i)
      for (auto j = 0; j < 3; ++j)
        if (fabs(aa[i][j] - rhs.aa[i][j]) > 0.01)
          return false;
    return true;
  }
  RotMatrixLegacy operator+(const RotMatrixLegacy &m);
  RotMatrixLegacy operator-(const RotMatrixLegacy &m);
  RotMatrixLegacy operator*(T a);
  RotMatrixLegacy operator*(const RotMatrixLegacy &m);
  void transpose();
  void zero() {
    aa[0][0] = aa[1][0] = aa[2][0] = 0;
    aa[0][1] = aa[1][1] = aa[2][1] = 0;
    aa[0][2] = aa[1][2] = aa[2][2] = 0;
  }

  friend std::ostream &operator<<(std::ostream &out, RotMatrixLegacy m) {
    out << "(" << m.aa[0][0] << "," << m.aa[0][1] << "," << m.aa[0][2] << ")"
        << "\n";
    out << "(" << m.aa[1][0] << "," << m.aa[1][1] << "," << m.aa[1][2] << ")"
        << "\n";
    out << "(" << m.aa[2][0] << "," << m.aa[2][1] << "," << m.aa[2][2] << ")"
        << "\n";
    return out;
  }

protected:
  T aa[3][3];
};

template <typename T>
RotMatrixLegacy<T> RotMatrixLegacy<T>::operator+(const RotMatrixLegacy<T> &m) {
  RotMatrixLegacy r;
  r.aa[0][0] = aa[0][0] + m.aa[0][0];
  r.aa[1][0] = aa[1][0] + m.aa[1][0];
  r.aa[2][0] = aa[2][0] + m.aa[2][0];
  r.aa[0][1] = aa[0][1] + m.aa[0][1];
  r.aa[1][1] = aa[1][1] + m.aa[1][1];
  r.aa[2][1] = aa[2][1] + m.aa[2][1];
  r.aa[0][2] = aa[0][2] + m.aa[0][2];
  r.aa[1][2] = aa[1][2] + m.aa[1][2];
  r.aa[2][2] = aa[2][2] + m.aa[2][2];
  return r;
}
template <typename T>
RotMatrixLegacy<T> RotMatrixLegacy<T>::operator-(const RotMatrixLegacy<T> &m) {
  RotMatrixLegacy r;
  r.aa[0][0] = aa[0][0] - m.aa[0][0];
  r.aa[1][0] = aa[1][0] - m.aa[1][0];
  r.aa[2][0] = aa[2][0] - m.aa[2][0];
  r.aa[0][1] = aa[0][1] - m.aa[0][1];
  r.aa[1][1] = aa[1][1] - m.aa[1][1];
  r.aa[2][1] = aa[2][1] - m.aa[2][1];
  r.aa[0][2] = aa[0][2] - m.aa[0][2];
  r.aa[1][2] = aa[1][2] - m.aa[1][2];
  r.aa[2][2] = aa[2][2] - m.aa[2][2];
  return r;
}

template <typename T> RotMatrixLegacy<T> RotMatrixLegacy<T>::operator*(T a) {
  RotMatrixLegacy r;
  r.aa[0][0] = aa[0][0] * a;
  r.aa[1][0] = aa[1][0] * a;
  r.aa[2][0] = aa[2][0] * a;
  r.aa[0][1] = aa[0][1] * a;
  r.aa[1][1] = aa[1][1] * a;
  r.aa[2][1] = aa[2][1] * a;
  r.aa[0][2] = aa[0][2] * a;
  r.aa[1][2] = aa[1][2] * a;
  r.aa[2][2] = aa[2][2] * a;
  return r;
}

template <typename T>
RotMatrixLegacy<T> RotMatrixLegacy<T>::operator*(const RotMatrixLegacy<T> &m) {
  RotMatrixLegacy r;

  r.aa[0][0] =
      aa[0][0] * m.aa[0][0] + aa[0][1] * m.aa[1][0] + aa[0][2] * m.aa[2][0];
  r.aa[0][1] =
      aa[0][0] * m.aa[0][1] + aa[0][1] * m.aa[1][1] + aa[0][2] * m.aa[2][1];
  r.aa[0][2] =
      aa[0][0] * m.aa[0][2] + aa[0][1] * m.aa[1][2] + aa[0][2] * m.aa[2][2];

  r.aa[1][0] =
      aa[1][0] * m.aa[0][0] + aa[1][1] * m.aa[1][0] + aa[1][2] * m.aa[2][0];
  r.aa[1][1] =
      aa[1][0] * m.aa[0][1] + aa[1][1] * m.aa[1][1] + aa[1][2] * m.aa[2][1];
  r.aa[1][2] =
      aa[1][0] * m.aa[0][2] + aa[1][1] * m.aa[1][2] + aa[1][2] * m.aa[2][2];

  r.aa[2][0] =
      aa[2][0] * m.aa[0][0] + aa[2][1] * m.aa[1][0] + aa[2][2] * m.aa[2][0];
  r.aa[2][1] =
      aa[2][0] * m.aa[0][1] + aa[2][1] * m.aa[1][1] + aa[2][2] * m.aa[2][1];
  r.aa[2][2] =
      aa[2][0] * m.aa[0][2] + aa[2][1] * m.aa[1][2] + aa[2][2] * m.aa[2][2];
  return r;
}

template <typename T> void RotMatrixLegacy<T>::transpose() {
  T t;
  t = aa[0][1];
  aa[0][1] = aa[1][0];
  aa[1][0] = t;
  t = aa[0][2];
  aa[0][2] = aa[2][0];
  aa[2][0] = t;
  t = aa[1][2];
  aa[1][2] = aa[2][1];
  aa[2][1] = t;
}
} // namespace Orientation
