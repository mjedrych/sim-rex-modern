#pragma once
#include "Quaternion.h"
#include "RotMatrix.h"
#include <cmath>
#include <memory>
#include <vector>

namespace Orientation {
template <typename T> T sqrt2() { return std::sqrt(static_cast<T>(2.0)); }

template <typename T> T sqrt3() { return std::sqrt(static_cast<T>(3.0)); }

template <typename T> T half_sqrt3() { return sqrt3<T>() / 2.0; }

class CrystalSymmetry {
public:
  enum class SymmetryList {
    CUBIC = 0,
    HEXAGONAL = 1,
  };

  void setActiveSymmetry(CrystalSymmetry::SymmetryList value) {
    active_symmetry = value;
  }

  template <typename TOrientation> auto &getSymmetries() const;

  template <template <typename> class TOrientation> auto &getSymmetries() const;

  SymmetryList getActiveSymmetry() { return active_symmetry; }

  const Quaternion<double> &getSymmetryQuaternion(unsigned int i) const;
  const RotMatrix<double> &getSymmetryMatrix(unsigned int i) const;

  const auto &getSymmetriesAsMatrix() const {
    switch (active_symmetry) {
    case SymmetryList::CUBIC:
      return cubic_CSRotMatrix;

    case SymmetryList::HEXAGONAL:
      return hex_CSRotMatrix;
    }
    return cubic_CSRotMatrix;
  }

  const auto &getSymmetriesAsQuaternion() const {
    switch (active_symmetry) {
    case SymmetryList::CUBIC:
      return cubic_CSQuaternion;

    case SymmetryList::HEXAGONAL:
      return hex_CSQuaternion;
    }
    return cubic_CSQuaternion;
  }

private:
  unsigned int MaxSymetries = 24;
  long double p32 = half_sqrt3<long double>();
  long double p12 = 1.0L / sqrt2<long double>();
  SymmetryList active_symmetry = SymmetryList::CUBIC;

  const std::vector<RotMatrix<double>> hex_CSRotMatrix{
      // Matrices for hexagonal symetry
      // ----------------------------------------------
      // after Tome, Wenk
      RotMatrix<double>(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0),
      RotMatrix<double>(-0.5, p32, 0.0, -p32, -0.5, 0.0, 0.0, 0.0, 1.0),
      RotMatrix<double>(-0.5, -p32, 0.0, p32, -0.5, 0.0, 0.0, 0.0, 1.0),
      //----------------------- END of 3-fold axes
      RotMatrix<double>(0.5, p32, 0.0, -p32, 0.5, 0.0, 0.0, 0.0, 1.0),
      RotMatrix<double>(-1.0, 0.0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0, 1.0),
      RotMatrix<double>(0.5, -p32, 0.0, p32, 0.5, 0.0, 0.0, 0.0, 1.0),
      //------------------------
      RotMatrix<double>(-0.5, -p32, 0.0, -p32, 0.5, 0.0, 0.0, 0.0, -1.0),
      RotMatrix<double>(1.0, 0.0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0, -1.0),
      RotMatrix<double>(-0.5, p32, 0.0, p32, 0.5, 0.0, 0.0, 0.0, -1.0),
      RotMatrix<double>(0.5, p32, 0.0, p32, -0.5, 0.0, 0.0, 0.0, -1.0),
      RotMatrix<double>(-1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, -1.0),
      RotMatrix<double>(0.5, -p32, 0.0, -p32, -0.5, 0.0, 0.0, 0.0, -1.0)};

  const std::vector<RotMatrix<double>> cubic_CSRotMatrix{
      // Matrices for cubic symetry
      // --------------------------------------------------
      // after Tome, Wenk
      RotMatrix<double>(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0), // E
      //
      RotMatrix<double>(0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 0.0, 1.0,
                        0.0), // L-1-1-1 x3
      RotMatrix<double>(0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0), // L111 x3
      RotMatrix<double>(0.0, -1.0, 0.0, 0.0, 0.0, 1.0, -1.0, 0.0,
                        0.0), // L1-1-1 x3
      RotMatrix<double>(0.0, -1.0, 0.0, 0.0, 0.0, -1.0, 1.0, 0.0,
                        0.0), // L-11-1 x3
      RotMatrix<double>(0.0, 1.0, 0.0, 0.0, 0.0, -1.0, -1.0, 0.0,
                        0.0), // L-1-11 x3
      RotMatrix<double>(0.0, 0.0, -1.0, 1.0, 0.0, 0.0, 0.0, -1.0,
                        0.0), // L11-1 x3
      RotMatrix<double>(0.0, 0.0, -1.0, -1.0, 0.0, 0.0, 0.0, 1.0,
                        0.0), // L-111 x3
      RotMatrix<double>(0.0, 0.0, 1.0, -1.0, 0.0, 0.0, 0.0, -1.0,
                        0.0), // L1-11 x3
      //----------------------- END of 3-fold axes
      RotMatrix<double>(-1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
                        -1.0), // L010 x2
      RotMatrix<double>(-1.0, 0.0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0,
                        1.0), // L001 x2
      RotMatrix<double>(1.0, 0.0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0,
                        -1.0), // L100 x2
      //----------------------- END of 222 point group which includes also first
      // matrix
      RotMatrix<double>(0.0, 0.0, -1.0, 0.0, -1.0, 0.0, -1.0, 0.0,
                        0.0), // L10-1 x2
      RotMatrix<double>(0.0, 0.0, 1.0, 0.0, -1.0, 0.0, 1.0, 0.0,
                        0.0), // L101 x2
      RotMatrix<double>(0.0, 0.0, 1.0, 0.0, 1.0, 0.0, -1.0, 0.0,
                        0.0), // L0-10 x4
      RotMatrix<double>(0.0, 0.0, -1.0, 0.0, 1.0, 0.0, 1.0, 0.0,
                        0.0), // L010 x4
      //------------------------
      RotMatrix<double>(-1.0, 0.0, 0.0, 0.0, 0.0, -1.0, 0.0, -1.0,
                        0.0), // L01-1 x2
      RotMatrix<double>(1.0, 0.0, 0.0, 0.0, 0.0, -1.0, 0.0, 1.0,
                        0.0), // L100 x4
      RotMatrix<double>(1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, -1.0,
                        0.0), // L100 x4
      RotMatrix<double>(-1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0,
                        0.0), // L011 x2
      //------------------------
      RotMatrix<double>(0.0, -1.0, 0.0, -1.0, 0.0, 0.0, 0.0, 0.0,
                        -1.0), // L-110 x2
      RotMatrix<double>(0.0, 1.0, 0.0, -1.0, 0.0, 0.0, 0.0, 0.0,
                        1.0), // L001 x4
      RotMatrix<double>(0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
                        -1.0), // L110 x2
      RotMatrix<double>(0.0, -1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
                        1.0) // L00-1 x4
  };

  const std::vector<Quaternion<double>> hex_CSQuaternion{
      Quaternion<double>(1.0, 0.0, 0.0, 0.0),
      Quaternion<double>(0.5, 0.0, 0.0, p32),
      Quaternion<double>(0.5, 0.0, 0.0, -p32),
      Quaternion<double>(p32, 0.0, 0.0, 0.5),
      Quaternion<double>(0.0, 0.0, 0.0, 1.0),
      Quaternion<double>(p32, 0.0, 0.0, -0.5),
      Quaternion<double>(0.0, 0.5, -p32, 0.0),
      Quaternion<double>(0.0, 1.0, 0.0, 0.0),
      Quaternion<double>(0.0, 0.5, p32, 0.0),
      Quaternion<double>(0.0, p32, 0.5, 0.0),
      Quaternion<double>(0.0, 0.0, 1.0, 0.0),
      Quaternion<double>(0.0, p32, -0.5, 0.0)};

  const std::vector<Quaternion<double>> cubic_CSQuaternion{
      // for cubic symetry --------------------------------------------------
      Quaternion<double>(1.0, 0.0, 0.0, 0.0),
      //
      Quaternion<double>(0.5, -0.5, -0.5, -0.5), // L-1-1-1 x3
      Quaternion<double>(0.5, 0.5, 0.5, 0.5),    // L111 x3
      Quaternion<double>(0.5, 0.5, -0.5, -0.5),  // L1-1-1 x3
      Quaternion<double>(0.5, -0.5, 0.5, -0.5),  // L-11-1 x3
      Quaternion<double>(0.5, -0.5, -0.5, 0.5),  // L-1-11 x3
      Quaternion<double>(0.5, 0.5, 0.5, -0.5),   // L11-1 x3
      Quaternion<double>(0.5, -0.5, 0.5, 0.5),   // L-111 x3
      Quaternion<double>(0.5, 0.5, -0.5, 0.5),   // L1-11 x3
      //-------------------------------
      Quaternion<double>(0.0, 0.0, 1.0, 0.0), // L010 x2
      Quaternion<double>(0.0, 0.0, 0.0, 1.0), // L001 x2
      Quaternion<double>(0.0, 1.0, 0.0, 0.0), // L100 x2
      //-------------------------------
      Quaternion<double>(0.0, -p12, 0.0, p12), // L-101 x2
      Quaternion<double>(0.0, p12, 0.0, p12),  // L101 x2
      Quaternion<double>(p12, 0.0, -p12, 0.0), // L0-10 x4
      Quaternion<double>(p12, 0.0, p12, 0.0),  // L010 x4
      //-------------------------------
      Quaternion<double>(0.0, 0.0, -p12, p12), // L01-1 x2
      Quaternion<double>(p12, -p12, 0.0, 0.0), // L-100 x4
      Quaternion<double>(p12, p12, 0.0, 0.0),  // L100 x4
      Quaternion<double>(0.0, 0.0, p12, p12),  // L011 x2
      //-------------------------------
      Quaternion<double>(0.0, p12, -p12, 0.0), // L-110 x2
      Quaternion<double>(p12, 0.0, 0.0, p12),  // L001 x4
      Quaternion<double>(0.0, p12, p12, 0.0),  // L110 x2
      Quaternion<double>(p12, 0.0, 0.0, -p12)  // L00-1 x4
  };
};
template <> inline auto &CrystalSymmetry::getSymmetries<RotMatrix>() const {
  return getSymmetriesAsMatrix();
}

template <> inline auto &CrystalSymmetry::getSymmetries<Quaternion>() const {
  return getSymmetriesAsQuaternion();
}

template <>
inline auto &CrystalSymmetry::getSymmetries<RotMatrix<double>>() const {
  return getSymmetriesAsMatrix();
}

template <>
inline auto &CrystalSymmetry::getSymmetries<RotMatrix<long double>>() const {
  return getSymmetriesAsMatrix();
}

template <>
inline auto &CrystalSymmetry::getSymmetries<RotMatrix<float>>() const {
  return getSymmetriesAsMatrix();
}

template <>
inline auto &CrystalSymmetry::getSymmetries<Quaternion<double>>() const {
  return getSymmetriesAsQuaternion();
}

template <>
inline auto &CrystalSymmetry::getSymmetries<Quaternion<long double>>() const {
  return getSymmetriesAsQuaternion();
}

template <>
inline auto &CrystalSymmetry::getSymmetries<Quaternion<float>>() const {
  return getSymmetriesAsQuaternion();
}

inline const Quaternion<double> &
CrystalSymmetry::getSymmetryQuaternion(unsigned int i) const {
  return getSymmetries<Quaternion>()[i];
}
inline const RotMatrix<double> &
CrystalSymmetry::getSymmetryMatrix(unsigned int i) const {
  return getSymmetries<RotMatrix>()[i];
}

} // namespace Orientation

extern Orientation::CrystalSymmetry CS;