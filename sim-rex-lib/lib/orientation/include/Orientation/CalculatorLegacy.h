#pragma once
#include "Calculator.h"
#include "CrystalSymmetryLegacy.h"
#include "Quaternion.h"
#include "RotMatrix.h"

namespace Orientation {
class CalculatorLegacy : public Calculator {
public:
  template <typename T>
  static double minMisoAngle(const Quaternion<T> &qA, const Quaternion<T> &qB,
                             CrystalSymmetryLegacy &cs) {
    if (qA == qB)
      return 0.0;

    Quaternion<T> mAT, gAB;
    double om, min_om = 1000;

    mAT = qA.transposedCopy();

    gAB = mAT * qB;

    for (unsigned int i = 0; i < cs.getMaxSymmetries(); i++) {
      auto c_sym = cs.getSymmetryQuaternion<T>(i);
      auto miso_sym = gAB * c_sym;
      om = miso_sym.getMisorientation();

      if (om <= min_om)
        min_om = om;
    }

    return min_om;
  }

  template <typename T>
  double minMisoAngle(const RotMatrix<T> &A, const RotMatrix<T> &B,
                      const CrystalSymmetryLegacy &cs) {
    if (A == B)
      return 0.0;

    RotMatrix<T> mAT, gAB;
    double om, min_om = 1000;

    mAT = A.transposedCopy();

    gAB = B * mAT;

    for (unsigned int i = 0; i < cs.getMaxSymmetries(); i++) {
      auto c_sym = cs.getSymmetryMatrix<T>(i);
      auto miso_sym = c_sym * gAB;
      om = miso_sym.getMisorientation();

      if (om <= min_om)
        min_om = om;
    }

    return min_om;
  }
};
} // namespace Orientation