#pragma once
#include <array>
#include <cmath>
#include <exception>
#include <iostream>
#include <numeric>
#include <type_traits>
namespace Orientation {
template <typename T> class RotMatrix {
  static_assert(std::is_floating_point<T>::value, "T must be float");

public:
  RotMatrix() { zero(); }
  RotMatrix(T m00, T m01, T m02, T m10, T m11, T m12, T m20, T m21, T m22)
      : data{m00, m01, m02, m10, m11, m12, m20, m21, m22} {
    normalize();
  }
  T &operator()(unsigned int i, unsigned int j) {
    size_t idx = i * 3 + j;
    try {
      return data.at(idx);
    } catch (const std::out_of_range &oor) {
      std::cerr << "Out of Range error: " << oor.what() << '\n';
      exit(EXIT_FAILURE);
    }
  };
  T &fastGet(unsigned int i, unsigned int j) { return data[i * 3 + j]; }
  constexpr bool operator==(const RotMatrix &rhs) const {
    auto r = *this - rhs;
    for (auto m : r.data)
      if (fabs(m) > 0.0001)
        return false;
    return true;
  }
  constexpr bool operator==(RotMatrix &rhs) {
    return *this == static_cast<const RotMatrix &>(rhs);
  }
  T det() {
    // m00 * m11 * m22 +
    // m01 * m12 * m20 +
    // m02 * m10 * m21 -
    // m02 * m11 * m20 -
    // m21 * m12 * m00 -
    // m22 * m10 * m01;
    return fastGet(0, 0) * fastGet(1, 1) * fastGet(2, 2) +
           fastGet(0, 1) * fastGet(1, 2) * fastGet(2, 0) +
           fastGet(0, 2) * fastGet(1, 0) * fastGet(2, 1) -
           fastGet(2, 0) * fastGet(1, 1) * fastGet(0, 2) -
           fastGet(2, 1) * fastGet(1, 2) * fastGet(0, 0) -
           fastGet(2, 2) * fastGet(1, 0) * fastGet(0, 1);
  }
  RotMatrix operator+(const RotMatrix &m) const;
  RotMatrix operator-(const RotMatrix &m) const;
  RotMatrix operator*(T a) const;
  RotMatrix operator*(const RotMatrix &m) const;
  void transpose();
  RotMatrix<T> transposedCopy() const;
  T getTrace() const { return data[0] + data[4] + data[8]; }
  T getMisorientation() const {
    T temp = (getTrace() - 1.0) / 2.0;
    if (temp > 1.001 || temp < -1.001) {
      std::cerr << "ACOS DOMAIN ERROR: " << temp << "\n";
      exit(EXIT_FAILURE);
    }
    return std::acos(temp);
    // return std::acos((GetTrace() - 1.0) / 2.0);
  }
  void zero() { data.fill(0.0); }
  friend std::ostream &operator<<(std::ostream &out, RotMatrix m) {
    auto idx = 1U;
    for (auto v : m.data) {
      out << v;
      if (idx % 3 == 0)
        out << "\n";
      else
        out << ",";
      idx++;
    }
    return out;
  }

protected:
  std::array<T, 9> data{};

  void normalize() {
    // T det = Det();
    // if(fabs(det - 1.0) > 0.1)
    //    det = 1.0;
    // std::cout << det << "\n";
    for (auto &m : data) {
      // m = crop<T>(m, 1000000.0);
      if (fabs(m) < 0.000001) {
        m = 0.0;
      }
    }

    if (data[8] > 1.0)
      data[8] = 1.0;
    if (data[8] < -1.0)
      data[8] = -1.0;
  }
};

template <typename T>
RotMatrix<T> RotMatrix<T>::operator+(const RotMatrix<T> &m) const {
  RotMatrix r;
  r.data = data + m.data;
  return r;
}

template <typename T>
RotMatrix<T> RotMatrix<T>::operator-(const RotMatrix<T> &m) const {
  RotMatrix r;
  r.data = data - m.data;
  return r;
}

template <typename T> RotMatrix<T> RotMatrix<T>::operator*(T a) const {
  RotMatrix r;
  r.data = data * a;
  return r;
}

template <typename T>
RotMatrix<T> RotMatrix<T>::operator*(const RotMatrix<T> &m) const {
  RotMatrix r;
  std::array<T, 3> this_fst_row{data[0], data[1], data[2]};
  std::array<T, 3> this_snd_row{data[3], data[4], data[5]};
  std::array<T, 3> this_trd_row{data[6], data[7], data[8]};

  std::array<T, 3> m_fst_col{m.data[0], m.data[3], m.data[6]};
  std::array<T, 3> m_snd_col{m.data[1], m.data[4], m.data[7]};
  std::array<T, 3> m_trd_col{m.data[2], m.data[5], m.data[8]};

  std::array<T, 3> &active_row = this_fst_row;
  r.data[0] = std::inner_product(active_row.begin(), active_row.end(),
                                 m_fst_col.begin(), 0.0);
  r.data[1] = std::inner_product(active_row.begin(), active_row.end(),
                                 m_snd_col.begin(), 0.0);
  r.data[2] = std::inner_product(active_row.begin(), active_row.end(),
                                 m_trd_col.begin(), 0.0);

  active_row = this_snd_row;
  r.data[3] = std::inner_product(active_row.begin(), active_row.end(),
                                 m_fst_col.begin(), 0.0);
  r.data[4] = std::inner_product(active_row.begin(), active_row.end(),
                                 m_snd_col.begin(), 0.0);
  r.data[5] = std::inner_product(active_row.begin(), active_row.end(),
                                 m_trd_col.begin(), 0.0);

  active_row = this_trd_row;
  r.data[6] = std::inner_product(active_row.begin(), active_row.end(),
                                 m_fst_col.begin(), 0.0);
  r.data[7] = std::inner_product(active_row.begin(), active_row.end(),
                                 m_snd_col.begin(), 0.0);
  r.data[8] = std::inner_product(active_row.begin(), active_row.end(),
                                 m_trd_col.begin(), 0.0);

  r.normalize();

  return r;
}

template <typename T> void RotMatrix<T>::transpose() {
  // m01 -- m10
  std::swap(data[1], data[3]);

  // m02 -- m20
  std::swap(data[2], data[6]);

  // m12 -- m21
  std::swap(data[5], data[7]);
}

template <typename T> RotMatrix<T> RotMatrix<T>::transposedCopy() const {
  RotMatrix<T> out = *this;
  out.transpose();
  return out;
}

template <typename T, unsigned long N>
std::array<T, N> &operator+=(std::array<T, N> &thi,
                             const std::array<T, N> &oth) {
  for (int i = 0; i < N; ++i)
    thi[i] += oth[i];
  return thi;
}

template <typename T, unsigned long N>
std::array<T, N> operator+(const std::array<T, N> &a,
                           const std::array<T, N> &b) {
  std::array<T, N> sum = a;
  sum += b;
  return sum;
}

template <typename T, unsigned long N>
std::array<T, N> operator-(const std::array<T, N> &thi,
                           const std::array<T, N> &oth) {
  std::array<T, N> out;
  for (int i = 0; i < N; ++i)
    out[i] = thi[i] - oth[i];
  return out;
}

template <typename T, unsigned long N>
std::array<T, N> operator*(const std::array<T, N> &thi, T a) {
  std::array<T, N> out;
  for (int i = 0; i < N; ++i)
    out[i] = thi[i] * a;
  return out;
}

} // namespace Orientation
