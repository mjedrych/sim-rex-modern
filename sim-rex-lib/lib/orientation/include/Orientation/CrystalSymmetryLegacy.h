#pragma once

#include "CrystalSymmetry.h"
#include "Quaternion.h"
#include "RotMatrix.h"
#include <cmath>
#include <memory>

namespace Orientation {
class CrystalSymmetryLegacy {
public:
  enum class SymmetryList {
    CUBIC = 0,
    HEXAGONAL = 1,
  };

  void setActiveSymmetry(CrystalSymmetryLegacy::SymmetryList value) {
    active_symmetry = value;
    switch (active_symmetry) {
    case SymmetryList::CUBIC:
      MaxSymetries = 24;
      break;
    case SymmetryList::HEXAGONAL:
      MaxSymetries = 12;
      break;
    }
  }

  SymmetryList getActiveSymmetry() { return active_symmetry; }

  unsigned int getMaxSymmetries() const { return MaxSymetries; }

  template <typename T> Quaternion<T> getSymmetryQuaternion(unsigned int i) {
    return CrystalSymetryQuaternion[static_cast<unsigned int>(active_symmetry)]
                                   [i];
  }
  template <typename T>
  const RotMatrix<T> &getSymmetryMatrix(unsigned int i) const {
    return CrystalSymetryMatrix[static_cast<unsigned int>(active_symmetry)][i];
  }

private:
  unsigned int MaxSymetries = 24;
  long double p32 = half_sqrt3<long double>();
  long double p12 = 1.0L / sqrt2<long double>();
  SymmetryList active_symmetry = SymmetryList::CUBIC;
  const RotMatrix<double> CrystalSymetryMatrix[2][24] = {
      // Matrices for cubic symetry
      // --------------------------------------------------
      // after Tome, Wenk
      RotMatrix<double>(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0), // E
      //
      RotMatrix<double>(0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 0.0, 1.0,
                        0.0), // L-1-1-1 x3
      RotMatrix<double>(0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0), // L111 x3
      RotMatrix<double>(0.0, -1.0, 0.0, 0.0, 0.0, 1.0, -1.0, 0.0,
                        0.0), // L1-1-1 x3
      RotMatrix<double>(0.0, -1.0, 0.0, 0.0, 0.0, -1.0, 1.0, 0.0,
                        0.0), // L-11-1 x3
      RotMatrix<double>(0.0, 1.0, 0.0, 0.0, 0.0, -1.0, -1.0, 0.0,
                        0.0), // L-1-11 x3
      RotMatrix<double>(0.0, 0.0, -1.0, 1.0, 0.0, 0.0, 0.0, -1.0,
                        0.0), // L11-1 x3
      RotMatrix<double>(0.0, 0.0, -1.0, -1.0, 0.0, 0.0, 0.0, 1.0,
                        0.0), // L-111 x3
      RotMatrix<double>(0.0, 0.0, 1.0, -1.0, 0.0, 0.0, 0.0, -1.0,
                        0.0), // L1-11 x3
      //----------------------- END of 3-fold axes
      RotMatrix<double>(-1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
                        -1.0), // L010 x2
      RotMatrix<double>(-1.0, 0.0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0,
                        1.0), // L001 x2
      RotMatrix<double>(1.0, 0.0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0,
                        -1.0), // L100 x2
      //----------------------- END of 222 point group which includes also first
      //matrix
      RotMatrix<double>(0.0, 0.0, -1.0, 0.0, -1.0, 0.0, -1.0, 0.0,
                        0.0), // L10-1 x2
      RotMatrix<double>(0.0, 0.0, 1.0, 0.0, -1.0, 0.0, 1.0, 0.0,
                        0.0), // L101 x2
      RotMatrix<double>(0.0, 0.0, 1.0, 0.0, 1.0, 0.0, -1.0, 0.0,
                        0.0), // L0-10 x4
      RotMatrix<double>(0.0, 0.0, -1.0, 0.0, 1.0, 0.0, 1.0, 0.0,
                        0.0), // L010 x4
      //------------------------
      RotMatrix<double>(-1.0, 0.0, 0.0, 0.0, 0.0, -1.0, 0.0, -1.0,
                        0.0), // L01-1 x2
      RotMatrix<double>(1.0, 0.0, 0.0, 0.0, 0.0, -1.0, 0.0, 1.0,
                        0.0), // L-100 x4
      RotMatrix<double>(1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, -1.0,
                        0.0), // L100 x4
      RotMatrix<double>(-1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0,
                        0.0), // L011 x2
      //------------------------
      RotMatrix<double>(0.0, -1.0, 0.0, -1.0, 0.0, 0.0, 0.0, 0.0,
                        -1.0), // L-110 x2
      RotMatrix<double>(0.0, 1.0, 0.0, -1.0, 0.0, 0.0, 0.0, 0.0,
                        1.0), // L001 x4
      RotMatrix<double>(0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
                        -1.0), // L110 x2
      RotMatrix<double>(0.0, -1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
                        1.0), // L00-1 x4
      // Matrices for hexagonal symetry
      // ----------------------------------------------
      // after Tome, Wenk
      RotMatrix<double>(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0),
      RotMatrix<double>(-0.5, p32, 0.0, -p32, -0.5, 0.0, 0.0, 0.0, 1.0),
      RotMatrix<double>(-0.5, -p32, 0.0, p32, -0.5, 0.0, 0.0, 0.0, 1.0),
      //----------------------- END of 3-fold axes
      RotMatrix<double>(0.5, p32, 0.0, -p32, 0.5, 0.0, 0.0, 0.0, 1.0),
      RotMatrix<double>(-1.0, 0.0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0, 1.0),
      RotMatrix<double>(0.5, -p32, 0.0, p32, 0.5, 0.0, 0.0, 0.0, 1.0),
      //------------------------
      RotMatrix<double>(-0.5, -p32, 0.0, -p32, 0.5, 0.0, 0.0, 0.0, -1.0),
      RotMatrix<double>(1.0, 0.0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0, -1.0),
      RotMatrix<double>(-0.5, p32, 0.0, p32, 0.5, 0.0, 0.0, 0.0, -1.0),
      RotMatrix<double>(0.5, p32, 0.0, p32, -0.5, 0.0, 0.0, 0.0, -1.0),
      RotMatrix<double>(-1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, -1.0),
      RotMatrix<double>(0.5, -p32, 0.0, -p32, -0.5, 0.0, 0.0, 0.0, -1.0),
      // Hexagonal has only 12 symetries. The rest are set to 0.
      // ---------------------
      RotMatrix<double>(0, 0, 0, 0, 0, 0, 0, 0, 0),
      RotMatrix<double>(0, 0, 0, 0, 0, 0, 0, 0, 0),
      RotMatrix<double>(0, 0, 0, 0, 0, 0, 0, 0, 0),
      RotMatrix<double>(0, 0, 0, 0, 0, 0, 0, 0, 0),
      RotMatrix<double>(0, 0, 0, 0, 0, 0, 0, 0, 0),
      RotMatrix<double>(0, 0, 0, 0, 0, 0, 0, 0, 0),
      RotMatrix<double>(0, 0, 0, 0, 0, 0, 0, 0, 0),
      RotMatrix<double>(0, 0, 0, 0, 0, 0, 0, 0, 0),
      RotMatrix<double>(0, 0, 0, 0, 0, 0, 0, 0, 0),
      RotMatrix<double>(0, 0, 0, 0, 0, 0, 0, 0, 0),
      RotMatrix<double>(0, 0, 0, 0, 0, 0, 0, 0, 0),
      RotMatrix<double>(0, 0, 0, 0, 0, 0, 0, 0, 0)};

  const Quaternion<double> CrystalSymetryQuaternion[2][24] = {
      // for cubic symetry --------------------------------------------------
      Quaternion<double>(1.0, 0.0, 0.0, 0.0),
      //
      Quaternion<double>(0.5, -0.5, -0.5, -0.5), // L-1-1-1 x3
      Quaternion<double>(0.5, 0.5, 0.5, 0.5),    // L111 x3
      Quaternion<double>(0.5, 0.5, -0.5, -0.5),  // L1-1-1 x3
      Quaternion<double>(0.5, -0.5, 0.5, -0.5),  // L-11-1 x3
      Quaternion<double>(0.5, -0.5, -0.5, 0.5),  // L-1-11 x3
      Quaternion<double>(0.5, 0.5, 0.5, -0.5),   // L11-1 x3
      Quaternion<double>(0.5, -0.5, 0.5, 0.5),   // L-111 x3
      Quaternion<double>(0.5, 0.5, -0.5, 0.5),   // L1-11 x3
      //-------------------------------
      Quaternion<double>(0.0, 0.0, 1.0, 0.0), // L010 x2
      Quaternion<double>(0.0, 0.0, 0.0, 1.0), // L001 x2
      Quaternion<double>(0.0, 1.0, 0.0, 0.0), // L100 x2
      //-------------------------------
      Quaternion<double>(0.0, -p12, 0.0, p12), // L-101 x2
      Quaternion<double>(0.0, p12, 0.0, p12),  // L101 x2
      Quaternion<double>(p12, 0.0, -p12, 0.0), // L0-10 x4
      Quaternion<double>(p12, 0.0, p12, 0.0),  // L010 x4
      //-------------------------------
      Quaternion<double>(0.0, 0.0, -p12, p12), // L01-1 x2
      Quaternion<double>(p12, -p12, 0.0, 0.0), // L-100 x4
      Quaternion<double>(p12, p12, 0.0, 0.0),  // L100 x4
      Quaternion<double>(0.0, 0.0, p12, p12),  // L011 x2
      //-------------------------------
      Quaternion<double>(0.0, p12, -p12, 0.0), // L-110 x2
      Quaternion<double>(p12, 0.0, 0.0, p12),  // L001 x4
      Quaternion<double>(0.0, p12, p12, 0.0),  // L110 x2
      Quaternion<double>(p12, 0.0, 0.0, -p12), // L00-1 x4
      // Matrices for hexagonal symetry
      // ----------------------------------------------
      Quaternion<double>(1.0, 0.0, 0.0, 0.0),
      Quaternion<double>(0.5, 0.0, 0.0, p32),
      Quaternion<double>(0.5, 0.0, 0.0, -p32),
      Quaternion<double>(p32, 0.0, 0.0, 0.5),
      Quaternion<double>(0.0, 0.0, 0.0, 1.0),
      Quaternion<double>(p32, 0.0, 0.0, -0.5),
      Quaternion<double>(0.0, 0.5, -p32, 0.0),
      Quaternion<double>(0.0, 1.0, 0.0, 0.0),
      Quaternion<double>(0.0, 0.5, p32, 0.0),
      Quaternion<double>(0.0, p32, 0.5, 0.0),
      Quaternion<double>(0.0, 0.0, 1.0, 0.0),
      Quaternion<double>(0.0, p32, -0.5, 0.0),
      // Hexagonal has only 12 symetries. The rest are set to 0.
      // ---------------------
      Quaternion<double>(1.0, 1.0, 1.0, 1.0),
      Quaternion<double>(1.0, 1.0, 1.0, 1.0),
      Quaternion<double>(1.0, 1.0, 1.0, 1.0),
      Quaternion<double>(1.0, 1.0, 1.0, 1.0),
      Quaternion<double>(1.0, 1.0, 1.0, 1.0),
      Quaternion<double>(1.0, 1.0, 1.0, 1.0),
      Quaternion<double>(1.0, 1.0, 1.0, 1.0),
      Quaternion<double>(1.0, 1.0, 1.0, 1.0),
      Quaternion<double>(1.0, 1.0, 1.0, 1.0),
      Quaternion<double>(1.0, 1.0, 1.0, 1.0),
      Quaternion<double>(1.0, 1.0, 1.0, 1.0),
      Quaternion<double>(1.0, 1.0, 1.0, 1.0)};
} CSLegacy;
} // namespace Orientation