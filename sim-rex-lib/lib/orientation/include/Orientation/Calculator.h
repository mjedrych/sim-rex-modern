#pragma once

#include "Converter.h"
#include "CrystalSymmetry.h"
#include "Quaternion.h"
#include "RotMatrix.h"

#include <vector>
namespace Orientation {

class Calculator {
public:
  template <typename TOrientation>
  static double minMisoAngle(const TOrientation &A, const TOrientation &B,
                             const CrystalSymmetry &cs) {
    if (A == B)
      return 0.0;

    double om, min_om = 1000;

    auto mAT = A.transposedCopy();

    auto gAB = multiply(mAT, B);

    // std::vector<TOrientation> symmetries;
    // if (std::is_same<TOrientation, RotMatrix<double>>::value ||
    // std::is_same<TOrientation, RotMatrix<long double>>::value ||
    // std::is_same<TOrientation, RotMatrix<float>>::value) 	symmetries =
    // cs.GetSymmetriesAsMatrix(); if (std::is_same<TOrientation,
    // Quaternion<double>>::value || std::is_same<TOrientation, Quaternion<long
    // double>>::value || std::is_same<TOrientation, Quaternion<float>>::value)
    //	symmetries = cs.GetSymmetriesAsQuaternion();
    for (auto c_sym : cs.getSymmetries<TOrientation>()) {
      auto miso_sym = multiply(gAB, c_sym);
      om = std::fabs(miso_sym.getMisorientation());
      if (om <= min_om)
        min_om = om;
    }

    return min_om;
  }

  template <typename TOrientation>
  static double desorientation(const TOrientation &A, const TOrientation &B,
                               const CrystalSymmetry &cs) {
    double om, min_om = 1000;

    auto mAT = A.transposedCopy();

    auto gAB = multiply(mAT, B);
    auto idx = 0;
    auto jdx = 0;
    auto j = 0;
    auto i = 0;
    for (auto c_sym_i : cs.getSymmetries<TOrientation>()) {
      for (auto c_sym_j : cs.getSymmetries<TOrientation>()) {
        auto miso_sym = multiply(c_sym_j, gAB, c_sym_i);
        om = std::fabs(miso_sym.getMisorientation());
        // TODO -- AXIS IN FUNDAMENTAL ZONE
        if (om <= min_om) {
          min_om = om;
          i = idx;
          j = jdx;
        }
        jdx++;
      }
      idx++;
    }
    return min_om;
  }

  template <typename TOrientation>
  static std::vector<TOrientation>
  SymmetricOrientations(const TOrientation &in, const CrystalSymmetry &cs) {
    std::vector<TOrientation> out;
    for (auto s : cs.getSymmetries<TOrientation>())
      out.emplace_back(multiply(in, s));
    return out;
  }

private:
  template <typename TOrientation>
  static TOrientation multiply(const TOrientation &a, const TOrientation &b) {
    if (std::is_same<TOrientation, RotMatrix<double>>::value ||
        std::is_same<TOrientation, RotMatrix<long double>>::value ||
        std::is_same<TOrientation, RotMatrix<float>>::value)
      return b * a;
    if (std::is_same<TOrientation, Quaternion<double>>::value ||
        std::is_same<TOrientation, Quaternion<long double>>::value ||
        std::is_same<TOrientation, Quaternion<float>>::value)
      return a * b;
  }

  template <typename TOrientation>
  static TOrientation multiply(const TOrientation &a, const TOrientation &b,
                               const TOrientation &c) {
    return multiply(multiply(a, b), c);
  }
};
} // namespace Orientation