#pragma once
#include <array>
#include <cmath>
#include <iomanip>
#include <iostream>
namespace Orientation {

template <typename T> class Quaternion {
  static_assert(std::is_same<T, double>::value ||
                    std::is_same<T, long double>::value ||
                    std::is_same<T, float>::value,
                "Only floating point types are accepted");
  std::array<T, 4> components = {0.0, 0.0, 0.0, 1.0};

public:
  Quaternion getCopy() const { return *this; }
  T getW() const { return components[0]; }
  T getX() const { return components[1]; }
  T getY() const { return components[2]; }
  T getZ() const { return components[3]; }
  T getNorm() const {
    return std::sqrt(
        components[0] * components[0] + components[1] * components[1] +
        components[2] * components[2] + components[3] * components[3]);
  }
  T getTrace() const { return 4.0 * components[0] * components[0] - 1.0; }
  Quaternion() {}
  Quaternion(T w, T x, T y, T z) { set(w, x, y, z); }
  void set(Quaternion &&q) { *this = q; }
  void set(T w, T x, T y, T z) {
    components = {w, x, y, z};
    normalize();

    for (auto &c : components) {
      c = fabs(c) < 0.0000000001 ? 0.0 : c;
      // c = crop<T>(c);
    }
  }

  T getMisorientation() const { return 2.0 * std::acos(getW()); }
  T getMisorientationInDeg() const {
    return getMisorientation() * 180.0 / M_PI;
  }

  void conjugate() {
    components[1] = -components[1];
    components[2] = -components[2];
    components[3] = -components[3];
  }

  void negate() {
    for (auto &c : components)
      if (c != 0.0)
        c = -c;
  }

  void transpose() { conjugate(); }

  Quaternion<T> transposedCopy() const {
    Quaternion<T> qout = *this;
    qout.transpose();
    return qout;
  }

  void normalize() {
    T norm = getNorm();
    if (norm != 1.0)
      for (auto &c : components)
        c /= norm;
  }

  Quaternion<T> operator+(const Quaternion<T> &qin) const {
    return Quaternion<T>(getW() + qin.getW(), getX() + qin.getX(),
                         getY() + qin.getY(), getZ() + qin.getZ());
  }
  Quaternion<T> operator*(const Quaternion<T> &qin) const {
    T w = components[0];
    T x = components[1];
    T y = components[2];
    T z = components[3];
    T ww, xx, yy, zz = 0.0;
    // static_cast<float> is used to get rid of calculation errors that leads to
    // a problem when two components are compared
    // == is giving false for the same orientations
    ww = w * qin.getW() - x * qin.getX() - y * qin.getY() - z * qin.getZ();
    xx = w * qin.getX() + x * qin.getW() + y * qin.getZ() - z * qin.getY();
    yy = w * qin.getY() - x * qin.getZ() + y * qin.getW() + z * qin.getX();
    zz = w * qin.getZ() + x * qin.getY() - y * qin.getX() + z * qin.getW();

    return Quaternion<T>(ww, xx, yy, zz);
  }

  constexpr bool operator==(const Quaternion &q) const {
    for (auto i = 0; i < components.size(); ++i)
      if ((fabs(components[i] - q.components[i]) > 0.001) &&
          (fabs(components[i] + q.components[i]) > 0.001))
        return false;
    return true;
  }
  constexpr bool operator==(Quaternion &q) {
    return *this == static_cast<const Quaternion &>(q);
  }

  bool theSameAs(const Quaternion &q) {
    for (auto i = 0; i < components.size(); ++i)
      if (fabs(components[i] - q.components[i]) > 0.001)
        return false;
    return true;
  }

  friend std::ostream &operator<<(std::ostream &stream, const Quaternion &q) {
    stream << "[w, x, y, z] = " << q.getW() << ", " << q.getX() << ", "
           << q.getY() << ", " << q.getZ() << '\n';

    return stream;
  }
};

} // namespace Orientation