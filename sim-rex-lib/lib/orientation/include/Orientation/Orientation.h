#pragma once

#include "BungeAngles.h"
#include "Calculator.h"
#include "Converter.h"
#include "CrystalSymmetry.h"
#include "Quaternion.h"
#include "RotMatrix.h"
namespace Orientation {
template <typename T>
// TruncRoundErrors
T crop(const T &in, T div = 1000000.0) {
  return static_cast<T>(std::round(div * in)) / div;
}

} // namespace Orientation