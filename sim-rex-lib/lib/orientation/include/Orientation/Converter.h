#pragma once
#include "BungeAngles.h"
#include "Quaternion.h"
#include "RotMatrix.h"
#include <cmath>
#include <string>
namespace Orientation {
class Converter {

public:
  template <typename T>
  static RotMatrix<T> QuaternionToRotMatrix(Quaternion<T> qin) {

    const auto w = qin.getW();
    const auto x = qin.getX();
    const auto y = qin.getY();
    const auto z = qin.getZ();

    T m00 = 1.0 - 2.0 * y * y - 2.0 * z * z;
    T m01 = 2.0 * x * y + 2.0 * w * z;
    T m02 = 2.0 * x * z - 2.0 * w * y;
    T m10 = 2.0 * x * y - 2.0 * w * z;
    T m11 = 1.0 - 2.0 * x * x - 2.0 * z * z;
    T m12 = 2.0 * y * z + 2.0 * w * x;
    T m20 = 2.0 * x * z + 2.0 * w * y;
    T m21 = 2.0 * y * z - 2.0 * w * x;
    T m22 = 1.0 - 2.0 * x * x - 2.0 * y * y;

    return RotMatrix<T>(m00, m01, m02, m10, m11, m12, m20, m21, m22);
  }

  template <typename T>
  static Quaternion<T> RotMatrixToQuaternionFromDiagonal(RotMatrix<T> m) {
    T w, x, y, z = 0.0;
    x = std::sqrt((m(0, 0) - m(1, 1) - m(2, 2) + 1.0) / 4.0);

    w = std::sqrt((m(0, 0) + m(1, 1) + m(2, 2) + 1.0) / 4.0);

    y = std::sqrt((-m(0, 0) + m(1, 1) - m(2, 2) + 1.0) / 4.0);
    z = std::sqrt((-m(0, 0) - m(1, 1) + m(2, 2) + 1.0) / 4.0);

    x = m(1, 2) - m(2, 1) < -0.1 ? -x : x;
    y = m(2, 0) - m(0, 2) < -0.1 ? -y : y;
    z = m(0, 1) - m(1, 0) < -0.1 ? -z : z;

    Quaternion<T> result;
    result.set(w, x, y, z);
    return result;
  }

  template <typename T>
  static Quaternion<T> RotMatrixToQuaternion(RotMatrix<T> m) {
    T w, x, y, z = 0.0;
    T trace = m(0, 0) + m(1, 1) + m(2, 2);

    w = std::sqrt(1.0 + trace) / 2.0;

    if (fabs(w) < 0.00001) {
      x = std::sqrt(m(0, 0) + 1) / 2.0;
      y = std::sqrt(m(1, 1) + 1) / 2.0;
      z = std::sqrt(m(2, 2) + 1) / 2.0;

      if (std::signbit(m(0, 2)) == 1.0)
        x = -x;

      if (std::signbit(m(1, 2)) == 1.0)
        y = -y;

      // in case z = 0.0
      if (std::signbit(m(0, 1)) == 1.0 && std::signbit(x * y) != 1.0)
        y = -y;
    } else {
      x = (m(1, 2) - m(2, 1)) / (4.0 * w);
      y = (m(2, 0) - m(0, 2)) / (4.0 * w);
      z = (m(0, 1) - m(1, 0)) / (4.0 * w);
    }

    Quaternion<T> result;
    result.set(w, x, y, z);

    return result;
  }

  // TODO
  template <typename T>
  static Quaternion<T> RotMatrixToQuaternionZ(RotMatrix<T> m) {
    T w, x, y, z = 0.0;

    z = 0.5 * std::sqrt(1.0 + m(2, 2) - m(0, 0) - m(1, 1));

    if (z < 0.00001) {
      y = std::sqrt(1 - m(0, 0)) / 2.0;
      x = std::sqrt(1 - m(1, 1)) / 2.0;
      w = std::sqrt(m(2, 2) + 1) / 2.0;

      // if (m(2, 0) < -0.000001)
      //     w = -w;
      // if (m(1, 2) < -0.000001)
      //     y = -y;
      // if (std::signbit(m(0, 1)) != 0.0 && std::signbit(x * y) != 1.0)
      //     x = -x;
    } else {
      x = (m(0, 2) + m(2, 0)) / (4.0 * z);
      y = (m(2, 1) + m(1, 2)) / (4.0 * z);
      w = (m(0, 1) - m(1, 0)) / (4.0 * z);
    }

    Quaternion<T> result;
    result.set(w, x, y, z);

    // if (std::signbit(z) == true)
    //    result.InvertAxis();
    return result;
  }

  template <typename T>
  static BungeAngles<T> QuaternionToBungeAngles(Quaternion<T> qin) {
    return RotMatrixToBungeAngles<T>(QuaternionToRotMatrix(qin));
  }

  template <typename T>
  static RotMatrix<T> BungeAnglesToRotMatrix(BungeAngles<T> ea) {

    T cfi1, cfi, cfi2, sfi1, sfi, sfi2;

    cfi1 = std::cos(ea.getPhi1());
    cfi = std::cos(ea.getPhi());
    cfi2 = std::cos(ea.getPhi2());
    sfi1 = std::sin(ea.getPhi1());
    sfi = std::sin(ea.getPhi());
    sfi2 = std::sin(ea.getPhi2());

    T m00 = cfi1 * cfi2 - sfi1 * sfi2 * cfi;
    T m01 = cfi2 * sfi1 + sfi2 * cfi1 * cfi;
    T m02 = sfi2 * sfi;
    T m10 = -cfi1 * sfi2 - sfi1 * cfi2 * cfi;
    T m11 = -sfi2 * sfi1 + cfi2 * cfi1 * cfi;
    T m12 = cfi2 * sfi;
    T m20 = sfi1 * sfi;
    T m21 = -cfi1 * sfi;
    T m22 = cfi;

    return RotMatrix<T>(m00, m01, m02, m10, m11, m12, m20, m21, m22);
  }

  template <typename T>
  static Quaternion<T> BungeAnglesToQuaternionIndirect(BungeAngles<T> ba) {
    return RotMatrixToQuaternion<T>(BungeAnglesToRotMatrix<T>(ba));
  }

  template <typename T>
  static Quaternion<T> BungeAnglesToQuaternionZXZ(BungeAngles<T> ba) {
    Quaternion<T> Z1(std::cos(0.5 * ba.getPhi1()), 0.0, 0.0,
                     std::sin(0.5 * ba.getPhi1()));
    Quaternion<T> Z2(std::cos(0.5 * ba.getPhi2()), 0.0, 0.0,
                     std::sin(0.5 * ba.getPhi2()));
    Quaternion<T> X(std::cos(0.5 * ba.getPhi()), std::sin(0.5 * ba.getPhi()),
                    0.0, 0.0);

    return Z1 * X * Z2;
  }

  template <typename T>
  static Quaternion<T> BungeAnglesToQuaternion(BungeAngles<T> ba) {
    T halfsum_fi12 = 0.5 * (ba.getPhi1() + ba.getPhi2());
    T halfdiff_fi12 = 0.5 * (ba.getPhi1() - ba.getPhi2());
    if (std::cos(halfsum_fi12) == 0.0)
      return BungeAnglesToQuaternionIndirect(ba);
    T rho3 = std::tan(halfsum_fi12);
    T rho2 = std::tan(0.5 * ba.getPhi()) * std::sin(halfdiff_fi12) /
             std::cos(halfsum_fi12);
    T rho1 = std::tan(0.5 * ba.getPhi()) * std::cos(halfdiff_fi12) /
             std::cos(halfsum_fi12);

    T div = std::sqrt(rho1 * rho1 + rho2 * rho2 + rho3 * rho3 + 1.0);
    Quaternion<T> result;

    T w = 1.0 / div;
    result.set(w, rho1 * w, rho2 * w, rho3 * w);

    return result;
  }

  template <typename T>
  static BungeAngles<T> RotMatrixToBungeAngles(RotMatrix<T> m) {
    T sigma = 0.000001;
    T phi1, phi2, Phi;

    if (fabs(m(2, 2)) > 1.0) {
      throw std::invalid_argument(
          std::to_string(m(2, 2)) +
          "  RotMatrixToBungeAngles::Argument of arccos cannot be outside of "
          "the range [-1,1]");
    }

    Phi = std::acos(m(2, 2));
    phi2 = 0.0;

    if (fabs(Phi) < sigma) {
      Phi = 0.0;
      // phi1 = std::atan2(-m(1, 0), m(0, 0));
      phi1 = 0.5 * std::atan2(m(0, 1), m(0, 0));
      phi2 = phi1;
    } else if (fabs(Phi - M_PI) < sigma) {
      Phi = M_PI;
      // ba.fi1 = std::atan2(m(1, 0), m(0, 0));
      phi1 = 0.5 * std::atan2(m(0, 1), m(0, 0));
      phi2 = -phi1;
    } else {
      phi1 = std::atan2(m(2, 0), -m(2, 1));
      phi2 = std::atan2(m(0, 2), m(1, 2));
    }

    phi1 = fabs(phi1) < sigma ? 0.0 : phi1;
    phi2 = fabs(phi2) < sigma ? 0.0 : phi2;

    if (phi1 < 0.0)
      phi1 += 2.0 * M_PI;

    if (phi2 < 0.0)
      phi2 += 2.0 * M_PI;

    return BungeAngles<T>(phi1, Phi, phi2, false);
  }
};
} // namespace Orientation