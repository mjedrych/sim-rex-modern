#pragma once
// https://docs.microsoft.com/en-us/cpp/c-runtime-library/math-constants?view=msvc-160
#define _USE_MATH_DEFINES // for C++
#include <Common/RandGen.h>
#include <cmath>
#include <iostream>
namespace Orientation {
template <typename T> class BungeAngles {
  //	static_assert(std::is_same<T, double>::value || std::is_same<T,
  // float>::value, "Only floating point types are accepted");

public:
  BungeAngles() {}
  BungeAngles(T phi1, T Phi, T phi2, bool deg = true) {
    set(phi1, Phi, phi2, deg);
  }
  void set(T phi1, T Phi, T phi2, bool deg = true) {
    phi_1 = phi1;
    phi = Phi;
    phi_2 = phi2;

    if (deg == true) {
      phi_1 *= M_PI / 180.0;
      phi *= M_PI / 180.0;
      phi_2 *= M_PI / 180.0;
    }
  }

  static auto getRandomOrientation() {
    T phi_1{0.0}, phi_2{0.0}, phi{0.0};
    phi_1 = static_cast<T>(Common::RandGen::genUInt(360));
    phi_2 = static_cast<T>(Common::RandGen::genUInt(360));
    do {
      phi = static_cast<T>(Common::RandGen::genUInt(180));
    } while (Common::RandGen::genProbability() > std::sin(phi / 180.0 * M_PI));

    return BungeAngles<T>(phi_1, phi, phi_2);
  }
  static auto getRandomSteelOrientation() {
    int g = Common::RandGen::genUInt(7);
    T phi_1{0.0}, phi_2{0.0}, phi{0.0};
    switch (g) {
    case 0:
      phi_1 = 45.0;
      phi = 90.0;
      phi_2 = 0.0;
      break;
    case 1:
      phi_1 = 0.0;
      phi = 19.0;
      phi_2 = 45.0;
      break;
    case 2:
      phi_1 = 0.0;
      phi = 25.0;
      phi_2 = 45.0;
      break;
    case 3:
      phi_1 = 0.0;
      phi = 35.0;
      phi_2 = 45.0;
      break;
    case 4:
      phi_1 = 19.0;
      phi = 54.0;
      phi_2 = 45.0;
      break;
    case 5:
      phi_1 = 10.0;
      phi = 54.0;
      phi_2 = 45.0;
      break;
    case 6:
      phi_1 = 90.0;
      phi = 54.0;
      phi_2 = 45.0;
      break;
    case 7:
      phi_1 = 0.0;
      phi = 54.0;
      phi_2 = 45.0;
      break;
    }
    return BungeAngles<T>(phi_1, phi, phi_2);
  }

  T getPhi1() const { return phi_1; }
  T getPhi2() const { return phi_2; }
  T getPhi() const { return phi; }

  T getPhi1InDeg() const { return getPhi1() * static_cast<T>(180.0 / M_PI); }
  T getPhi2InDeg() const { return getPhi2() * static_cast<T>(180.0 / M_PI); }
  T getPhiInDeg() const { return getPhi() * static_cast<T>(180.0 / M_PI); }

  friend std::ostream &operator<<(std::ostream &stream, const BungeAngles &q) {
    stream << "In Degrees [phi_1, phi, phi_2] = " << q.getPhi1InDeg() << ", "
           << q.getPhiInDeg() << ", " << q.getPhi2InDeg();
    return stream;
  }

  constexpr bool operator==(const BungeAngles &rhs) {
    if (fabs(phi_1 - rhs.phi_1) * 180.0 / M_PI > 0.5)
      return false;
    if (fabs(phi_2 - rhs.phi_2) * 180.0 / M_PI > 0.5)
      return false;
    if (fabs(phi - rhs.phi) * 180.0 / M_PI > 0.5)
      return false;
    return true;
  }
  constexpr bool operator==(BungeAngles &rhs) {
    return *this == static_cast<const BungeAngles &>(rhs);
  }

private:
  T phi_1, phi_2, phi = 0.0;
};
} // namespace Orientation
