#pragma once
#include "SpinBuilder.h"

namespace Microstructure {

class SpinBuilderRolled : public SpinBuilder {
public:
  SpinBuilderRolled(Grid::Structure *pgrid) : SpinBuilder(pgrid) {}
  void coarsening(GridPoint &p);

protected:
  void fillSpins(GridPoint &p);

  std::vector<int> spin_vec;
};
} // namespace Microstructure
