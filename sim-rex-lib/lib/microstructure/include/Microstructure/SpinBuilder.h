#pragma once
#include "Builder.h"
#include <unordered_map>
namespace Microstructure {

class SpinBuilder : public Builder {
public:
  SpinBuilder(Grid::Structure *pgrid) { grid = pgrid; }

#ifdef READCONTROLER
  SpinBuilder(MCBox *b, const ReadControler &rc) : _box(b), _rc(rc) {
    using namespace GuiParameters;
    _rc.Get(pMMaxSpin, _max_spin);
    _rc.Get(pMK, _k);
    _rc.Get(pMGrainsNumber, _gn);
    _rc.Get(pMB0, _beg_gs);
    _rc.Get(pMB1, _end_gs);
    _rc.Get(pMXRatio, _xratio);
    _rc.Get(pMYRatio, _yratio);
    _rc.Get(pMZRatio, _zratio);

    //_end_gs=_box->Get_xsize()*_box->Get_ysize()*_box->Get_zsize()/float(_gn);
  }
#endif

  void setGrainDistribution(const float &b, const float &e, const int &pgn) {
    beg_gs = b;
    end_gs = e;
    gn = pgn;
  }
  void setMaxSpin(int maxs) { max_spin = maxs; }
  void setSpinWeights(float x, float y, float z) {
    x_ratio = x;
    y_ratio = y;
    z_ratio = z;
  }
  void setTexture();
  void generateGrains();

protected:
  // TODO::move to Grid class
  void randId();

  void coarsening(GridPoint &p);
  bool neighborsHaveTheSameId(GridPoint &p);
  virtual void fillSpins(GridPoint &p) = 0;

  void randomOrientations();
  //  void RandomOrientations(std::vector<Orientation> &t) {}

  void clean();

  // ReadControler _rc;
  unsigned int max_spin{1000};

  // Grains stats
  int gn{300};
  float beg_gs{10};
  float end_gs{200};

  // ratios as floats?
  float x_ratio{1.0f};
  float y_ratio{1.0f};
  float z_ratio{1.0f};
  std::unordered_map<int, float> spins;
};
} // namespace Microstructure
