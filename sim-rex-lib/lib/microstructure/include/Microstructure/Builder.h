#pragma once
#include <Grains/GrainCollection.h>
#include <Grid/Grid.h>
namespace Microstructure {
class GradientFunc {
public:
  static double applyGradientX(const Grid::GridPosition &p, double limit_low,
                               double limit_high, size_t size,
                               unsigned int strength = 0) {
    if (strength == 0)
      return 1.0;
    float a = 2.0 * static_cast<float>(limit_high - limit_low) / size;
    float th = size / 2.0f;

    float max = a * pow(th, strength) + limit_low;

    if (p.getX() <= th)
      return (a * pow(p.getX(), strength) + limit_low) / max;
    else
      return (a * pow(size - p.getX(), strength) + limit_low) / max;
  }
};

class Builder {
public:
  Builder() {}
  virtual void generate() = 0;
  virtual void generateGrains() = 0;
  virtual void setTexture() = 0;

protected:
  Grid::Structure *grid;
  Grains::GrainCollection *grains;
};

} // namespace Microstructure