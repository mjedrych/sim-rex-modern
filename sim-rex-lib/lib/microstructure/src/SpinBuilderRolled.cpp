#include "SpinBuilderRolled.h"
namespace Microstructure {
void SpinBuilderRolled::coarsening(GridPoint &p) {
  spin_vec.clear();
  if (!neighborsHaveTheSameId(p)) {
    std::random_device rd;
    std::mt19937 g(rd());

    std::shuffle(spin_vec.begin(), spin_vec.end(), g);
    p.setId(spin_vec[Common::RandGen::genUInt(spin_vec.size() - 1)]);
  }
}

void SpinBuilderRolled::fillSpins(GridPoint &p) {
  for (auto i = 0; i < static_cast<unsigned int>(x_ratio * 10); ++i)
    std::transform(p.getNeighbors().getFirstOrderX().begin(),
                   p.getNeighbors().getFirstOrderX().end(),
                   std::back_inserter(spin_vec),
                   [](auto &&item) { return item->getId(); });

  for (auto i = 0; i < static_cast<unsigned int>(y_ratio * 10); ++i)
    std::transform(p.getNeighbors().getFirstOrderY().begin(),
                   p.getNeighbors().getFirstOrderY().end(),
                   std::back_inserter(spin_vec),
                   [](auto &&item) { return item->getId(); });

  for (auto i = 0; i < static_cast<unsigned int>(z_ratio * 10); ++i)
    std::transform(p.getNeighbors().getFirstOrderZ().begin(),
                   p.getNeighbors().getFirstOrderZ().end(),
                   std::back_inserter(spin_vec),
                   [](auto &&item) { return item->getId(); });
}
} // namespace Microstructure