#include "SpinBuilder.h"
#include <Common/RandGen.h>
#include <algorithm>
#include <cmath>
#include <execution>
#include <iostream>
using Grid::GridPoint;
namespace Microstructure {

void SpinBuilder::setTexture() {
  // Roulette r(_stats->GetGrainsNumber());
  // r.MakeStellTexture();
  // r.MakeCopperTexture();
  // std::vector<Orientation> t= r.GetTexture();
  // RandomOrientations(t);
  randomOrientations();

  // todo encapsulate grid loop with std::function
  std::for_each(std::execution::par_unseq, grid->begin(), grid->end(),
                [](auto &&item) { item.updateColor(); });
}

void SpinBuilder::fillSpins(GridPoint &p) {
  spins.clear();
  for (auto &n : p.getNeighbors().getFirstOrderX())
    spins[n->getId()] += x_ratio;
  for (auto &n : p.getNeighbors().getFirstOrderY())
    spins[n->getId()] += y_ratio;
  for (auto &n : p.getNeighbors().getFirstOrderZ())
    spins[n->getId()] += z_ratio;
}

void SpinBuilder::generateGrains() {

  randId();

  do {
    for (auto i : Common::RandGen::genUniqueUIntVector(grid->getSize())) {
      auto &p = grid->get(i);

      // GradientFunction(i,gf);
      // if(RandomNumber() <= gf)
      // if(RandomNumber() <= GradientFunction(i))
      coarsening(p);
    }
    grains->buildAllUsingId();
  } while (grains->getStatGrainsNumber() > gn);
  clean();
  grains->buildAllUsingId();
}

// TODO::move to Grid class
void SpinBuilder::randId() {
  std::for_each(
      std::execution::par_unseq, grid->begin(), grid->end(),
      [this](auto &&item) { item.setId(Common::RandGen::genUInt(max_spin)); });
}

bool SpinBuilder::neighborsHaveTheSameId(GridPoint &p) {
  for (auto &n : p.getNeighbors().getFirstOrder())
    if (p.getId() != n->getId())
      return false;
  return true;
}

void SpinBuilder::coarsening(GridPoint &p) {
  spins.clear();
  if (!neighborsHaveTheSameId(p)) {
    fillSpins(p);
    std::random_device rd;
    std::mt19937 g(rd());
    // std::shuffle(spins.begin(), spins.end(), g);
    auto max = std::max_element(
        spins.begin(), spins.end(),
        [](const auto &a, const auto &b) { return a.second < b.second; });
    p.setId(max->first);
  }
}

void SpinBuilder::clean() {
  std::for_each(std::execution::par_unseq, grid->begin(), grid->end(),
                [this](auto &&item) {
                  if (item.getGrain() == nullptr)
                    coarsening(item);
                });
}

//: RandomOrientations: picks random orienation and assigns it to the grains
// with the same spin ://
void SpinBuilder::randomOrientations() {
  for (auto s = 0; s < max_spin; ++s) {
    auto ba = Orientation::BungeAngles<double>::getRandomOrientation();

    auto result = grains->getAllGrains().begin();
    do {
      result = std::find_if(result, grains->getAllGrains().end(),
                            [&s](auto &&g) { return g.getId() == s; });
      for (auto &p : *result)
        p->getOrientation().set(
            Orientation::Converter::BungeAnglesToQuaternion(ba));
    } while (result != grains->getAllGrains().end());
  }
}

// roulette
#ifdef ROULLETTE
void SpinBuilder::RandomOrientations(std::vector<Orientation> &t) //
{
  //_stats=_box->GetGrainStatistics();
  //_stats->CalculateGrains();
  std::vector<Grain> grains = _stats->GetAllGrains();
  float fi, fi1, fi2;
  if (t.size() != grains.size()) {
    std::cout << "\n Houston, mamy problem !!!!\n";
    std::cout << " tex size = " << t.size() << "\n";
    std::cout << " grain size = " << grains.size() << "\n";
  }
  for (unsigned int i = 0; i < t.size(); ++i) {
    t[i].Get(fi1, fi, fi2);
    Grain &g = grains[i];
    for (Pos j = 0; j < g.GetSize(); ++j) {
      g[j]->Set(fi1, fi, fi2, true); //!!! careful with degrees
      g[j]->SetSE(t[i].GetSE());
    }
  }
}
#endif

} // namespace Microstructure