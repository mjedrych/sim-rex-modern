#pragma once
#include "BoundaryMobility.h"
#include <cmath>
namespace Physics {

class FlipProbability {
public:
  void setMobility(BoundaryMobility *m) { mobility = m; }
  void setTemperature(double t) { temperature = t; }
  virtual double getProbability(double miso_angle, double delta_E) = 0;

protected:
  BoundaryMobility *mobility{nullptr};
  double temperature{0.0};
};

class StdRexProbability : public FlipProbability {
public:
  double getProbability(double miso_angle, double delta_E) {
    if (delta_E <= 0.0001)
      return -mobility->getMobility(miso_angle) * delta_E / 3.0;
    else if (temperature = 0.0)
      return 0.0;
    else
      // PROBLEM in legacy code to be checked !!!!!!!!!!
      return mobility->getMobility(miso_angle) * std::exp(-delta_E);
  }
};

class AlwaysTrueProbability : public FlipProbability {
public:
  double getProbability(double miso_angle, double delta_E) { return 1.0; }
};

class BoundaryEnergyDrivenProbability : public FlipProbability {
public:
  double getProbability(double miso_angle, double delta_E) {
    if (delta_E <= 0.0001)
      return -delta_E / 6.5;
    else if (temperature = 0.0)
      return 0.0;
    else
      // PROBLEM in legacy code to be checked !!!!!!!!!!
      return mobility->getMobility(miso_angle) * std::exp(-delta_E);
  }
};

class MobilityDrivenProbability : public FlipProbability {
public:
  double getProbability(double miso_angle, double delta_E) {
    if (delta_E <= 0.0001)
      return -mobility->getMobility(miso_angle);
    else if (temperature = 0.0)
      return 0.0;
    else
      // PROBLEM in legacy code to be checked !!!!!!!!!!
      return mobility->getMobility(miso_angle) * std::exp(-delta_E);
  }
};

// TODO
#ifdef CURVATURE
if (mcp->GetCurvature() >= 5.0) {
}
// propability/=mcp->GetCurvature();
else if (mcp->GetCurvature() <= -1.0)
  propability *= 2.0;
#endif

} // namespace Physics