#pragma once
#include <cmath>

namespace Physics {

class BoundaryMobility {
public:
  BoundaryMobility() {}
  BoundaryMobility(double ptheta) { theta = ptheta; }
  void setTheta(double ptheta) { theta = ptheta; }
  virtual double getMobility(double miso_angle) = 0;

protected:
  double theta{15.0};
};

class ArheniusMobility : public BoundaryMobility {
public:
  ArheniusMobility(double ptheta) : BoundaryMobility(ptheta) {}
  double getMobility(double miso_angle) {
    if (miso_angle <= theta)
      return 1.58198 * (1.0 - std::exp(-std::pow(miso_angle / theta, 3.0)));
    else
      return 1.0;
  }
};
} // namespace Physics