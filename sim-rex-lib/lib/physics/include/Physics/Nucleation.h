#pragma once
#include <Grid/GridPoint.h>
#include <Grid/Structure.h>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
using namespace std::string_literals;
namespace Physics {

class Nucleation {
public:
  Nucleation(Grid::Structure *gs) : name("RAW Nucleation"s), grid(gs) {}
  Nucleation(Nucleation *n) : nucleation(n), grid(n->grid) {}

  virtual void apply() {}
  virtual ~Nucleation() {
    // log destructions
    std::cout << name + "::Destructor\n";
  }

  virtual void operator()() {} // functor for decoration

  auto &getNucleiVector() { return nuclei_vec; }
  auto &getGrid() { return grid; }

  void setNucleiNumber(int n) { nuclei_number = n; }
  int getNucleiNumber() { return nuclei_number; }
  void setNucleiNumber(float fraction) {
    //  _box->GetGrainStatistics()->CalculateGrains();
    //  nuclei_number = static_cast<int>(
    //      fraction * _box->GetGrainStatistics()->GetGrainsNumber());
  }

  void transferData() {
    grid = nucleation->grid;
    nuclei_vec = nucleation->getNucleiVector();
  }
  void enlargeNucleis(const int &iter = 1, const bool &circle = true);
  void saveBungeAngles();

protected:
  Nucleation *nucleation{nullptr};
  Grid::Structure *grid{nullptr};
  std::string name{""s};
  std::vector<Grid::GridPoint *> nuclei_vec;
  int nuclei_number{0};
}; // namespace Physics
} // namespace Physics