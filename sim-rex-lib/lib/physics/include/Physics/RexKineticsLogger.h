#pragma once
#include <cmath>
#include <cstdio>
#include <fstream>
#include <memory>
namespace Physics {
class RexKineticsLogger {
public:
  RexKineticsLogger() {
    initializeGnuplot();
    jmak_file.open("Output/jmak.txt");
  }
  ~RexKineticsLogger() {
    jmak_file.close();
    if (!gnuplotFitPipe) {
      fprintf(gnuplotFitPipe, "load 'plot.sh' \n");
      fflush(gnuplotFitPipe);
      pclose(gnuplotFitPipe);
    }
    if (!gnuplotPipe)
      pclose(gnuplotPipe);
  }

  void log(double rec_frac, size_t mc_step) {
    jmak_file << mc_step << "\t" << rec_frac << "\t" << std::log(1.0 * mc_step)
              << "\t" << std::log(-std::log(1.0 - rec_frac)) << '\n';
    fprintf(gnuplotPipe, "plot 'Output/jmak.txt' \n");
    fflush(gnuplotPipe);
  }

private:
  void initializeGnuplot(std::string path = ".\\binary\\gnuplot.exe") {
    // http://www.stahlke.org/dan/gnuplot-iostream/
    // GNUPLOT INITIALIZATION //

    gnuplotPipe = popen(path.c_str(), "w");
    gnuplotFitPipe = popen(path.c_str(), "w");
    if (!gnuplotPipe && !gnuplotFitPipe) {
      fprintf(gnuplotPipe, "set grid xtics ytics \n");
      fprintf(gnuplotFitPipe, "set grid xtics ytics \n");
      fflush(gnuplotPipe);
    }
  }
  // std::unique_ptr<FILE, decltype(&pclose)> thePipe(
  //    popen(theCommand.c_str(), "r"), pclose);
  FILE *gnuplotPipe{nullptr};
  FILE *gnuplotFitPipe{nullptr};
  std::ofstream jmak_file;
};
} // namespace Physics