#pragma once
#include "../Nucleation.h"
namespace Physics {
class RandomPosition : public Nucleation {
public:
  RandomPosition(Nucleation *n) : Nucleation(n) {
    name = "NucleiRandomPositionDecorator";
  }
  void apply() {
    nucleation->apply();
    transferData();
    auto n = 0;
    while (n < nuclei_number) {
      auto &point = grid->getRandom();
      auto &monte_carlo = point.getExtensions().MonteCarlo;
      if (monte_carlo.getStoredEnergy() < 0.0)
        std::cerr << "ERROR:: energy smaller than 0.0 !\n";
      if (!monte_carlo.isNucleus()) {
        monte_carlo.isNucleus(true);
        monte_carlo.setStoredEnergy(0.0);
        // point.getExtensions().Ebsd.setIQ(0.0);
        nuclei_vec.push_back(&point);
        n++;
      }
    }
  }
};
} // namespace Physics
