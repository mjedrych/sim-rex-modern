#pragma once
#include "../Nucleation.h"
#include <Common/RandGen.h>
#include <Orientation/Orientation.h>
namespace Physics {

class AddSteelOrientation : public Nucleation {
public:
  AddSteelOrientation(Nucleation *n) : Nucleation(n) {
    name = "AddSteelOrientation";
  }

  void apply() {
    for (auto &nucleus : nuclei_vec) {
      auto ba = Orientation::BungeAngles<double>::getRandomSteelOrientation();
      nucleus->setOrientation(
          Orientation::Converter::BungeAnglesToQuaternion(ba));
      // nuclei->updateColor();
    }
  }
};
} // namespace Physics
