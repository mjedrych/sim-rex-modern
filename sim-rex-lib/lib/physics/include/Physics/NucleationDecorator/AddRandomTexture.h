#pragma once
#include "../Nucleation.h"
#include <Orientation/Orientation.h>
namespace Physics {

class AddRandomTexture : public Nucleation {
public:
  AddRandomTexture(Nucleation *n) : Nucleation(n) {
    name = "AddRandomTextureDecorator";
  }
  void apply() {
    nucleation->apply();
    transferData();
    for (auto &nucleus : nuclei_vec) {
      auto ba = Orientation::BungeAngles<double>::getRandomOrientation();
      // TODO: Check randomOrientation method//
      nucleus->setOrientation(
          Orientation::Converter::BungeAnglesToQuaternion(ba));
      // nuclei->updateColor();
    }
  }
};
} // namespace Physics
