#pragma once
#include "../Nucleation.h"
namespace Physics {
class AddSteelTexture : public Nucleation {
public:
  AddSteelTexture(Nucleation *n) : Nucleation(n) { name = "AddSteelTexture"; }
  void apply() {
    nucleation->apply();
    transferData();

    // Roulette r(nuclei_vec.size());
    // r.MakeStellTexture();
    // auto texture = r.getSteelTexture().begin();
    // TODO roulette texture generator
    for (auto &nucleus : nuclei_vec) {
      // auto ba = texture++; // return Orientations
      nucleus->setOrientation(Orientation::Converter::BungeAnglesToQuaternion(
          Orientation::BungeAngles<double>()));
      // nuclei->updateColor();
    }
  }
};
} // namespace Physics
