#pragma once
#include "../Nucleation.h"
#include <Orientation/Orientation.h>
namespace Physics {

class SingleNucleus : public Nucleation {
public:
  SingleNucleus(Nucleation *n) : Nucleation(n) {
    name = "SingleNucleus";
    auto central_position = Grid::GridPosition(grid->getDimensions() / 2);
    nucleus = &grid->get(central_position);
  }

  SingleNucleus(Nucleation *n, Orientation::BungeAngles<double> &ba)
      : SingleNucleus(n) {
    nucleus->setOrientation(
        Orientation::Converter::BungeAnglesToQuaternion(ba));
    // nucleus->UpdateColor();
  }

  SingleNucleus(Nucleation *n, Grid::GridPosition &pos) : Nucleation(n) {
    name = "SingleNucleus";
    nucleus = &grid->get(pos);
  }

  SingleNucleus(Nucleation *n, Grid::GridPosition &pos,
                Orientation::BungeAngles<double> &ba)
      : Nucleation(n) {
    name = "SingleNucleus";
    nucleus = &grid->get(pos);
    nucleus->setOrientation(
        Orientation::Converter::BungeAnglesToQuaternion(ba));
    // nucleus->UpdateColor();
  }

  void apply() {
    nucleation->apply();
    transferData();
    auto &monte_carlo = nucleus->getExtensions().MonteCarlo;
    monte_carlo.isNucleus(true);
    monte_carlo.setStoredEnergy(0.0);
    nuclei_vec.push_back(nucleus);
  }

private:
  Grid::GridPoint *nucleus;
};
} // namespace Physics