#include "BoundaryEnergy/BoundaryEnergy.h"
#include "BoundaryMobility.h"
#include "FlipProbability.h"
#include "Nucleation.h"

#include "StoredEnergy/StoredEnergy.h"
#include <Grid/GridPoint.h>
#include <Grid/Structure.h>
#include <functional>
#include <sstream>
namespace Physics {
class ModelMonteCarlo {
public:
  ModelMonteCarlo(FlipProbability *fp, BoundaryMobility *bm, BoundaryEnergy *be)
      : flip_probability(fp), mobility(bm), b_energy(be) {
    flip_probability->setMobility(mobility);
  }
  void setTheta(double th) { theta = th; }
  void setGammaMax(double gm) { gamma_max = gm; }
  virtual void Recrystallization() = 0;
  virtual void GG(const unsigned int &m) = 0; // default m = 1000

  void GrainShrinking() {}
  void DoNucleation() {
    /*
    int site_number=_box->Get_xsize()*_box->Get_ysize()*_box->Get_zsize();
    _n=new RRNucleation();
    _n->SetBox(_box);
    _n->SetNucleiNumber(site_number/200);
    (*_n)();
    */
  }

protected:
  auto recNumber() {
    unsigned int sum = 0;
    for_each(grid->begin(), grid->end(), [&sum](auto &&item) {
      if (item.getExtensions().MonteCarlo.getStoredEnergy() == 0.0)
        sum++;
    });
    return sum;
  }

  auto recFraction() { return recNumber() / grid->getSize(); }
  auto recFraction(double rec_num) { return rec_num / grid->getSize(); }
  double deltaE(Grid::GridPoint &root, Grid::GridPoint &n) {
    double benergy_prev = b_energy->getEnergy(root);
    root.getOrientation().set(n.getOrientation().getCopy());
    double benergy_next = b_energy->getEnergy(root);
    return benergy_next - benergy_prev -
           root.getExtensions().MonteCarlo.getStoredEnergy();
  }

  void recrystalizePoint(Grid::GridPoint &flip_point, Grid::GridPoint &n) {
    auto &mc = flip_point.getExtensions().MonteCarlo;
    mc.setStoredEnergy(0.0);
    // flip_point.updateColor();
    flip_point.setId(n.getId()); // or flip_point.setGrain(n.getGrain());
  }

  // TODO improve perc
  void logProgress(double perc, double rec_frac) {
    if ((perc - rec_frac) <= 0.0000009f) {
      std::cout << "Zrekrystalizowane " << rec_frac << "\n";
      std::ostringstream stm;
      // stm.setf(ios::fixed, ios::floatfield);
      stm.precision(2);
      stm << perc;
      Grid::GridIO::ExportBinaryVtk(*grid, "Output/rec_" + stm.str() + ".vtk");
      if (perc >= 0.09999f)
        perc += 0.05f;
      else
        perc += 0.01f;
    }
  }
  void InitializeGG() {}
  void GGReport(const unsigned int &step) {}
  bool flipTrial(Grid::GridPoint &flip_point);
  void loopAlgorithm(std::function<void(unsigned int &i)> f);

  //-------------------------
  Nucleation *nucleation{nullptr};
  FlipProbability *flip_probability{nullptr};
  BoundaryEnergy *b_energy{nullptr};
  StoredEnergy *se{nullptr};
  BoundaryMobility *mobility{nullptr};
  // TotalEnergy* te{nullptr};
  Grid::Structure *grid{nullptr};

  double theta{15.0};
  double gamma_max{1.0};
  int mcstep{0};
  long num_pf_points{0};
  // ReadControler _rc;

  // std::ofstream ggFile;
  // std::ostringstream stm;
  // GrainStatistics grainStats;
};
} // namespace Physics

#ifdef LEGACY
public:
PhysicsModel(MCBox *b, const ReadControler &rc) : _box(b), _rc(rc) {
  ggFile.open("Output/gg.txt");
  _box->InitializeRandGen();
}

InitializeGnuplot();
jmakFile.open("Output/jmak.txt");

virtual ~PhysicsModel() {
  jmakFile.close();
  ggFile.close();
}

void SetBox(MCBox *b) {
  _box = b;
  _box->InitializeRandGen();
}

class NeighboringZonePM : public PhysicsModel {
public:
  NeighboringZonePM(MCBox *b) : PhysicsModel(b) {}
  ~NeighboringZonePM() {}
  void Recrystallization();
};

class EqualDistancePM : public PhysicsModel {
public:
  EqualDistancePM(MCBox *b) : PhysicsModel(b) {}
  ~EqualDistancePM() {}
  void Recrystallization();
};
#endif