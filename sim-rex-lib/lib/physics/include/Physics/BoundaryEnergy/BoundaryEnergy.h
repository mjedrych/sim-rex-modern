#pragma once
#include <Grid/GridPoint.h>
#include <Orientation/Calculator.h>
#include <cmath>

namespace Physics {

class BoundaryEnergy {
public:
  BoundaryEnergy() {}
  BoundaryEnergy(double pgamma, double ptheta) {
    gamma_max = pgamma, theta = ptheta;
  }
  void setGammaMax(double pgamma) { gamma_max = pgamma; }
  void setTheta(double ptheta) { theta = ptheta; }
  virtual double getEnergy(Grid::GridPoint &p1) = 0;

protected:
  double theta{15.0};
  double gamma_max{1.0};
};

class FirstOrderBE : public BoundaryEnergy {
public:
  FirstOrderBE() {}
  FirstOrderBE(double pgamma, double ptheta) : BoundaryEnergy(pgamma, ptheta) {}

  double getEnergy(Grid::GridPoint &p1) {
    double energy = 0.0;
    auto &&nn = p1.getNeighbors().getFirstOrder();
    // TODO change to parallel version
    for (auto &&n : nn) {
      double miso_angle = Orientation::Calculator::minMisoAngle(
                              p1.getOrientation(), n->getOrientation(), CS) *
                          180.0 / M_PI;
      if (miso_angle <= 1.0)
        continue;
      else if (miso_angle < theta)
        energy += gamma_max * (miso_angle / theta) *
                  (1.0 - std::log(miso_angle / theta));
      else if (miso_angle >= theta)
        energy += gamma_max;
    }
    return energy;
  }
};

// TODO:: to be improved significantly
class SecondOrderBE : public BoundaryEnergy {
public:
  SecondOrderBE() {}
  SecondOrderBE(double pgamma, double ptheta)
      : BoundaryEnergy(pgamma, ptheta) {}
  double getEnergy(Grid::GridPoint &p1) {
    double energy = 0.0;

    auto &&n1 = p1.getNeighbors().getFirstOrder();
    // TODO change to parallel version
    for (auto &&n : n1) {
      double miso_angle = Orientation::Calculator::minMisoAngle(
                              p1.getOrientation(), n->getOrientation(), CS) *
                          180.0 / M_PI;
      if (miso_angle <= 1.0)
        continue;
      else if (miso_angle < theta)
        energy += gamma_max * (miso_angle / theta) *
                  (1.0 - std::log(miso_angle / theta));
      else if (miso_angle >= theta)
        energy += gamma_max;
    }

    auto &&n2 = p1.getNeighbors().getSecondOrder();
    // TODO change to parallel version
    for (auto &&n : n2) {
      double miso_angle = Orientation::Calculator::minMisoAngle(
                              p1.getOrientation(), n->getOrientation(), CS) *
                          180.0 / M_PI;
      if (miso_angle <= 1.0)
        continue;
      else if (miso_angle < theta)
        energy += gamma_max * (miso_angle / theta) *
                  (1.0 - std::log(miso_angle / theta));
      else if (miso_angle >= theta)
        energy += gamma_max;
    }
    return energy;
  }
};
} // namespace Physics
