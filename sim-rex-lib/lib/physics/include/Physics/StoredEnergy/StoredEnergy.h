#pragma once
#include <Common/Color.h>
#include <Common/RandGen.h>
#include <Grains/Grains.h>
#include <Grid/MCPoint.h>
#include <Grid/Structure.h>
namespace Physics {
/* potential application of STRATEGY design patter */
class StoredEnergy {

public:
  StoredEnergy() {}
  StoredEnergy(Grid::Structure *pgrid) : grid(pgrid) {}
  void setMaxSE(const double &m) { max_se = m; }
  void setMinSE(const double &m) { min_se = m; }

  virtual void apply() = 0;

protected:
  Grid::Structure *grid{nullptr};
  double max_se{1.0};
  double min_se{0.0};
};

class ConstantSE : public StoredEnergy {
protected:
public:
  ConstantSE(Grid::Structure *pgrid, double pmax_se) : StoredEnergy(pgrid) {
    max_se = pmax_se;
  }
  void apply() {
    for (auto &&p : *grid) {
      p.getExtensions().MonteCarlo.setStoredEnergy(max_se);
    }
  }
};

class RandomSE : public StoredEnergy {
protected:
public:
  RandomSE(Grid::Structure *pgrid, double pmax_se, double pmin_se)
      : StoredEnergy(pgrid) {
    max_se = pmax_se;
    min_se = pmin_se;
  }

  void apply() {
    for (auto &&p : *grid) {
      double se =
          Common::RandGen::genProbability() * (max_se - min_se) + min_se;
      p.getExtensions().MonteCarlo.setStoredEnergy(se);
    }
  }
};

class RandomGradientSE : public StoredEnergy {
public:
  RandomGradientSE(Grid::Structure *pgrid, double pmax_se, double pmin_se)
      : StoredEnergy(pgrid) {
    max_se = pmax_se;
    min_se = pmin_se;
  }

  void apply() {
    Grains::GrainCollection grains(grid);
    grains.CenterOfGravity();
    for (auto &grain : grains.getAllGrains()) {
      double max =
          0.5 * Common::RandGen::genProbability() * max_se + 0.5 * max_se;

      if (grain.getSize() < grains.getMinSize()) {
        for (auto &point : grain) {
          point->getExtensions().MonteCarlo.setStoredEnergy(max);
        }
      }

      /*
      else {
        float max_distance = 0.0;
        float x, y, z;
        grains[g].GetCogCoord(x, y, z);
        for (Pos p = 0; p < grains[g].GetSize(); ++p) {
          MCPoint *grainpoint = grains[g].GetGrainPoint(p);
          float d = grainpoint->GetDistance(x, y, z);
          if (d > max_distance)
            max_distance = d;
        }

        for (Pos p = 0; p < grains[g].GetSize(); ++p) {
          MCPoint *grainpoint = grains[g].GetGrainPoint(p);
          float d = grainpoint->GetDistance(x, y, z);

          float se =
              (grainpoint->GetDistance(x, y, z) / max_distance) * (max - _min) +
              _min;
          grainpoint->SetSE(se);
        }
      }

      */
    }
    for (auto &p : *grid) {
      auto energy = p.getExtensions().MonteCarlo.getStoredEnergy();
      p.setColor(Common::Color(energy * 25, energy * 25, energy * 25));
    }
    //  gstats->CalculateBoundaries();
    //  _box->SetBoundaryColor(255, 255, 255);
  }
};

class GradientSE : public StoredEnergy {
public:
  GradientSE(Grid::Structure *pgrid, double pmax_se, double pmin_se)
      : StoredEnergy(pgrid) {
    max_se = pmax_se;
    min_se = pmin_se;
  }
  void apply() {
    Grains::GrainCollection grains(grid);
    grains.CenterOfGravity();
    /*
      std::vector<Grain> grains = gstats->GetAllGrains();
      for (Pos g = 0; g < grains.size(); ++g) {
        float max = _max;

        if (grains[g].GetSize() < gstats->GetMinSize()) {
          for (Pos p = 0; p < grains[g].GetSize(); ++p)
            grains[g].GetGrainPoint(p)->SetSE(max);
        } else {
          float max_distance = 0.0;
          float x, y, z;
          grains[g].GetCogCoord(x, y, z);
          for (Pos p = 0; p < grains[g].GetSize(); ++p) {
            MCPoint *grainpoint = grains[g].GetGrainPoint(p);
            float d = grainpoint->GetDistance(x, y, z);
            if (d > max_distance)
              max_distance = d;
          }

          for (Pos p = 0; p < grains[g].GetSize(); ++p) {
            MCPoint *grainpoint = grains[g].GetGrainPoint(p);
            float d = grainpoint->GetDistance(x, y, z);

            float se =
                (grainpoint->GetDistance(x, y, z) / max_distance) * (max - _min)
      + _min; grainpoint->SetSE(se);
          }
        }

    for (auto &p : *grid) {
      Grid::MCPoint *mcp = static_cast<Grid::MCPoint *>(&p);
      p.setColor(Common::Color(mcp->getStoredEnergy() * 25,
                               mcp->getStoredEnergy() * 25,
                               mcp->getStoredEnergy() * 25));
    }
    //  gstats->CalculateBoundaries();
    //  _box->SetBoundaryColor(255, 255, 255);
  }
   */
  }
};
} // namespace Physics