#include "Physics.h"
#include "RexKineticsLogger.h"
#include <Grid/GridIO.h>
#include <Orientation/Calculator.h>
#include <algorithm>
#include <cmath>
#include <fstream>
namespace Physics {
void ModelMonteCarlo::Recrystallization() {
  InitializeGG();

  Grid::GridIO::ExportBinaryVtk(*grid, "Output/rec_0.00.vtk");
  std::cout << "Recrystallization starts\n";
  std::cout << "JMAX = " << gamma_max << "\n";

  auto rec_number = recNumber();
  bool rec = true;
  size_t mc_step = 0;
  auto rex_log = RexKineticsLogger();
  while (rec) {
#ifdef CURVATURE
    grainStats.FastCalculateGrainsById();
    grainStats.CalculateBoundaries();
    grainStats.CenterOfGravity();
    // grainStats.SortBoundary2D();
    grainStats.BoundaryCurvature();
#endif
    // randVec is analog to mod1
    loopAlgorithm([this, &rec_number, &rex_log, mc_step](auto &&item) {
      auto &sel_point = grid->get(item);
      if (flipTrial(sel_point)) {
        rec_number++;
        // LOG DATA
        logProgress(1.0, recFraction(rec_number));
        rex_log.log(recFraction(rec_number), mc_step);
      }
    });

    mc_step++;
    if (recFraction(rec_number) > 0.95)
      rec = false;

    std::cout << "number of steps = " << mc_step << "\n";
    std::cout << "Rekrystalization finished\n";
  } // while
}
void ModelMonteCarlo::loopAlgorithm(std::function<void(unsigned int &i)> f) {
  // randVec is analog to mod1
  auto randVec = Common::RandGen::genUniqueUIntVector(grid->getSize());
  std::for_each(std::execution::par_unseq, randVec.begin(), randVec.end(),
                [&f](auto &&item) { f(item); });
}
bool ModelMonteCarlo::flipTrial(Grid::GridPoint &flip_point) {
  auto &mc = flip_point.getExtensions().MonteCarlo;

  if (!mc.isRecrystallized()) {
    auto neighbor = flip_point.getNeighbors().getRandomNearestNeighbor();
    auto &mc_neighbor = neighbor->getExtensions().MonteCarlo;

    if (!mc_neighbor.isRecrystallized()) {
      // find another neighbor or return false
      return false;
    } else {
      auto orientation_bacup = flip_point.getOrientation().getCopy();
      auto delta_energy = deltaE(flip_point, *neighbor);
      auto flip_omega = Orientation::Calculator::minMisoAngle(
          flip_point.getOrientation(), neighbor->getOrientation(), CS);

      auto probability =
          flip_probability->getProbability(flip_omega, delta_energy);

      if (Common::RandGen::genProbability() <= probability) {
        recrystalizePoint(flip_point, *neighbor);
        return true;
      } else {
        flip_point.setOrientation(orientation_bacup);
        return false;
      }
    }
  } // if !rec
  return false;
}
} // namespace Physics

/*
void PhysicsModel::GG(const unsigned int &maxIter) {
  InitializeGG();

  long iter;
  while (mcstep < maxIter) {
    for_ijks(i, j, k, _box) (*_box)(i, j, k)->IsRanded(false);

#ifdef PARALLEL_GG
#pragma omp parallel for
#endif
    for (iter = 0; iter < numOfPoints; ++iter) {
      Neighbours n, mcn, mcn2;
      MCPoint *mcp;
      MCPoint *rn;
      float benergy;
      float newbenergy;
      float flipOmega;
      float deltaEnergy;
      float propability;
      float fi1_bacup;
      float Fi_bacup;
      float fi2_bacup;
      Pos i, j, k;

// MOD 1//
#ifdef GG_MOD1
      do {
        i = rand() % _box->Get_xsize();
        j = rand() % _box->Get_ysize();
        k = rand() % _box->Get_zsize();
        mcp = (*_box)(i, j, k);
        if (mcp->IsRanded() == true) {
          mcp->GetMCNeighbours(mcn);
          for (Pos n = 0; n < mcn.size(); ++n)
            if (mcn[n]->IsRanded() == false) {
              mcp = mcn[n];
              break;
            }
        }
      } while (mcp->IsRanded() == true);
#else
      i = r.Rand(_box->Get_xsize());
      j = r.Rand(_box->Get_ysize());
      k = r.Rand(_box->Get_zsize());
      mcp = (*_box)(i, j, k);
      if ((*_box)(i, j, k)->IsRanded() == true)
        continue;
#endif

      mcp->IsRanded(true);
      mcp->GetMCNeighbours(mcn);

#ifdef GG_MOD2
      mcn2.clear();
      for (Pos n = 0; n < mcn.size(); ++n) {
        // UWAGA: ziarna o roznym ID moga miec te sama orientacje!!! //todo:
        // poprawic
        if (mcp->GetId() !=
            mcn[n]->GetId()) // Random Neighbour IS NOT IN THE SAME GRAIN
          // if(mcp->GetOrientationId()!=mcn[n]->GetOrientationId()) //!
          // MOD2
          mcn2.push_back(mcn[n]);
      }

      if (mcn2.size() == 0)
        continue;
      else
        rn = mcn2[r.Rand(mcn2.size())];

#else
      rn = mcn[r.Rand(mcn.size())];
#endif

      flipOmega = mcp->FindMinOmega(rn) * RAD2DEG;
      if (flipOmega == 0.0)
        continue;

      benergy = BoundaryEnergy(mcp, mcn);
      fi1_bacup = mcp->Get_fi1();
      Fi_bacup = mcp->Get_fi();
      fi2_bacup = mcp->Get_fi2();
      mcp->Set(rn->Get_fi1(), rn->Get_fi(), rn->Get_fi2(), IN_RAD);
      newbenergy = BoundaryEnergy(mcp, mcn);
      deltaEnergy = newbenergy - benergy;

      if (deltaEnergy <= 0.0) // UWAGA na <=0.0
      {
        propability = Mobility(flipOmega);
        // propability = -Mobility(flipOmega)*deltaEnergy/(1.0);
        // propability = -deltaEnergy/(6.5f);
        // propability = 1.0;
      } else {
#ifdef TZERO
        propability = 0.0;
#else
        propability =
            Mobility(flipOmega) * exp(-deltaEnergy); // NICE PROBLEM
!!!!!!!!!! #endif
      }

      if (r.RandFraction() < propability) {
        mcp->SetSE(0.0);
        mcp->UpdateColor();
        mcp->SetId(rn->GetId());
      } else {
        mcp->Set(fi1_bacup, Fi_bacup, fi2_bacup, IN_RAD);
      }

    } // EOF for
    mcstep++;
    GGReport(maxIter / 50);
  } // EOF while

  std::cout << "GG STOP\n";
}

float PhysicsModel::RecFraction() {
  int defNumber = 0;
  for_ijks(i, j, k, _box) if ((*_box)(i, j, k)->GetSE() != 0.0) defNumber++;
  return 1.0f - 1.0f * defNumber /
                    (_box->Get_xsize() * _box->Get_ysize() *
_box->Get_zsize());
}

void PhysicsModel::InitializeGG() {
  _box->SetMCNeighboursRange(1); // liczba stref MC
  _box->ExportBinaryVtk("Output/gg_00.vtk");

  //----- REKRYSTALIZACJA ------//
  std::cout << "Rozpoczynam GG\n";
  std::cout << "JMAX = " << _gamma_max << "\n";
  // system("pause");

  mcstep = 0;
  numOfPoints = _box->Get_xsize() * _box->Get_ysize() * _box->Get_zsize();

  stm.setf(ios::fixed, ios::floatfield);
  stm.precision(2);

  mt.SetBox(_box);
  mt.Initialize();
  cout << "Orientations Number: " << mt.GetOrientationsNumber() << endl;

  grainStats.SetBox(_box);

  grainStats.FastCalculateGrains();
#ifdef GG_SAVEKINETICS
  ggFile << mcstep << "\t" << 1.0 * numOfPoints / grainStats.GetGrainsNumber()
         << "\t" << sqrt(1.0 * numOfPoints / grainStats.GetGrainsNumber())
         << "\t";
#ifdef GG_SAVEGRAINS
  for (Pos g = 0; g < grainStats.GetGrainsNumber(); ++g)
    ggFile << grainStats.GetGrains()[g].GetId() << "\t"
           << grainStats.GetGrains()[g].GetSize() << "\t";
#endif
  ggFile << endl;
#endif
}

void PhysicsModel::GGReport(const unsigned int &step) {
  if (mcstep % step == 0) {
    cout << mcstep << endl;
    grainStats.FastCalculateGrainsById();
#ifdef GG_SAVEKINETICS
    ggFile << mcstep << "\t" << grainStats.GetGrainsNumber() << "\t"
           << 1.0 * numOfPoints / grainStats.GetGrainsNumber() << "\t"
           << sqrt(1.0 * numOfPoints / grainStats.GetGrainsNumber()) << "\t";
#ifdef GG_SAVEGRAINS
    for (Pos g = 0; g < grainStats.GetGrainsNumber(); ++g)
      ggFile << grainStats.GetGrains()[g].GetId() << "\t"
             << grainStats.GetGrains()[g].GetSize() << "\t";
#endif
    ggFile << endl;
#endif
    stm.str(std::string());
    stm.clear();
    stm << mcstep;
    _box->ExportBinaryVtk("Output/gg_" + stm.str() + ".vtk");
  }
}

//-------- Fast Physics Model --------//

void FPhysicsModel::GG(const unsigned int &maxIter) {
  int counter = 0;
  InitializeGG();
  long iter;
  grainStats.SetMinMiso(5.0);
  while (grainStats.GetGrainsNumber() > 30) {
#ifdef CURVATURE
    grainStats.FastCalculateGrainsById();
    grainStats.CalculateBoundaries();
    grainStats.CenterOfGravity();
    // grainStats.SortBoundary2D();
    grainStats.BoundaryCurvature();
#endif
    for_ijks(i, j, k, _box) (*_box)(i, j, k)->IsRanded(false);

#ifdef PARALLEL_GG
#pragma omp parallel for
#endif
    for (iter = 0; iter < numOfPoints; ++iter) {
      Neighbours n, mcn, mcn2;
      MCPoint *mcp;
      MCPoint *rn;
      float benergy;
      float newbenergy;
      float flipOmega;
      float deltaEnergy;
      float propability;
      float fi1_bacup;
      float Fi_bacup;
      float fi2_bacup;
      int orientationIdBacup;
      Pos i, j, k;

// MOD 1//
#ifdef GG_MOD1
      do {
        // i = rand()%_box->Get_xsize();
        // j = rand()%_box->Get_ysize();
        // k = rand()%_box->Get_zsize();
        // mcp=(*_box)(i,j,k);
        mcp = _box->GetRandomPoint();
        counter++;
        if (mcp->IsRanded() == true) {
          mcp->GetMCNeighbours(mcn);
          for (Pos n = 0; n < mcn.size(); ++n)
            if (mcn[n]->IsRanded() == false) {
              mcp = mcn[n];
              break;
            }
        }
      } while (mcp->IsRanded() == true);
#else
      // i = r.Rand(_box->Get_xsize());
      // j = r.Rand(_box->Get_ysize());
      // k = r.Rand(_box->Get_zsize());
      // mcp=(*_box)(i,j,k);
      mcp = _box->GetRandomPoint();
      if ((*_box)(i, j, k)->IsRanded() == true)
        continue;
#endif

      mcp->IsRanded(true);
      mcp->GetMCNeighbours(mcn);

#ifdef GG_MOD2
      mcn2.clear();
      for (Pos n = 0; n < mcn.size(); ++n) {
        // UWAGA: ziarna o roznym ID moga miec te sama orientacje!!! //todo:
        // poprawic
        // if(mcp->GetId()!=mcn[n]->GetId()) // Random Neighbour IS NOT IN
        // THE SAME GRAIN
        if (mcp->GetOrientationId() != mcn[n]->GetOrientationId()) //! MOD2
          mcn2.push_back(mcn[n]);
      }

      if (mcn2.size() == 0)
        continue;
      else
        // rn = mcn2[r.Rand(mcn2.size())];
        rn = mcn2[Rand(mcn2.size())];

#else
      rn = mcn[r.Rand(mcn.size())];
#endif

      flipOmega = mt.GetMiso(mcp, rn);
      if (flipOmega == 0.0)
        continue;
#ifdef EXTENDED_BE
      benergy = ExtendedBoundaryEnergy(mcp, mcn);
#else
      benergy = BoundaryEnergy(mcp, mcn);
#endif
      fi1_bacup = mcp->Get_fi1();
      Fi_bacup = mcp->Get_fi();
      fi2_bacup = mcp->Get_fi2();
      orientationIdBacup = mcp->GetOrientationId();
      mcp->Set(rn->Get_fi1(), rn->Get_fi(), rn->Get_fi2(), IN_RAD);
      mcp->SetOrientationId(rn->GetOrientationId());
#ifdef EXTENDED_BE
      newbenergy = ExtendedBoundaryEnergy(mcp, mcn);
#else
      newbenergy = BoundaryEnergy(mcp, mcn);
#endif
      deltaEnergy = newbenergy - benergy;

      if (deltaEnergy <= 0.0) // UWAGA na <=0.0
      {
        propability = Mobility(flipOmega);
#ifdef CURVATURE
        if (mcp->GetCurvature() >= 5.0) {
        }
        // propability/=mcp->GetCurvature();
        else if (mcp->GetCurvature() <= -1.0)
          propability *= 2.0;
#endif

        // propability = -Mobility(flipOmega)*deltaEnergy/(1.0);
        // propability = -deltaEnergy/(6.5f);
        // propability = 1.0;
      } else {
#ifdef TZERO
        propability = 0.0;
#else
        propability =
            Mobility(flipOmega) * exp(-deltaEnergy); // NICE PROBLEM
!!!!!!!!!! #endif
      }

      // if (r.RandFraction()<propability)
      if (RandFraction() < propability) {
        mcp->SetSE(0.0);
        mcp->UpdateColor();
        mcp->SetId(rn->GetId());
      } else {
        mcp->Set(fi1_bacup, Fi_bacup, fi2_bacup, IN_RAD);
        mcp->SetOrientationId(orientationIdBacup);
      }

    } // EOF for
    mcstep++;
    GGReport(50);
    // GGReport(maxIter/50);
  } // EOF while

  std::cout << "GG STOP\n";
  std::cout << "Ilosc losowan z boxa: " << counter << std::endl;
}


void FPhysicsModel::GrainShrinking() {
  InitializeGG();

  long iter;
  while (grainStats.GetGrains()[1]->GetSize() > 0) {
    for_ijks(i, j, k, _box) (*_box)(i, j, k)->IsRanded(false);

#ifdef PARALLEL_GG
#pragma omp parallel for
#endif
    for (iter = 0; iter < numOfPoints; ++iter) {
      Neighbours n, mcn, mcn2;
      MCPoint *mcp;
      MCPoint *rn;
      float benergy;
      float newbenergy;
      float flipOmega;
      float deltaEnergy;
      float propability;
      float fi1_bacup;
      float Fi_bacup;
      float fi2_bacup;
      int orientationIdBacup;
      Pos i, j, k;

// MOD 1//
#ifdef GG_MOD1
      do {
        i = rand() % _box->Get_xsize();
        j = rand() % _box->Get_ysize();
        k = rand() % _box->Get_zsize();
        mcp = (*_box)(i, j, k);
        if (mcp->IsRanded() == true) {
          mcp->GetMCNeighbours(mcn);
          for (Pos n = 0; n < mcn.size(); ++n)
            if (mcn[n]->IsRanded() == false) {
              mcp = mcn[n];
              break;
            }
        }
      } while (mcp->IsRanded() == true);
#else
      i = r.Rand(_box->Get_xsize());
      j = r.Rand(_box->Get_ysize());
      k = r.Rand(_box->Get_zsize());
      mcp = (*_box)(i, j, k);
      if ((*_box)(i, j, k)->IsRanded() == true)
        continue;
#endif

      mcp->IsRanded(true);
      mcp->GetMCNeighbours(mcn);

#ifdef GG_MOD2
      mcn2.clear();
      for (Pos n = 0; n < mcn.size(); ++n) {
        // UWAGA: ziarna o roznym ID moga miec te sama orientacje!!! //todo:
        // poprawic
        // if(mcp->GetId()!=mcn[n]->GetId()) // Random Neighbour IS NOT IN
        // THE SAME GRAIN
        if (mcp->GetOrientationId() != mcn[n]->GetOrientationId()) //! MOD2
          mcn2.push_back(mcn[n]);
      }

      if (mcn2.size() == 0)
        continue;
      else
        rn = mcn2[r.Rand(mcn2.size())];

#else
      rn = mcn[r.Rand(mcn.size())];
#endif

      flipOmega = mt.GetMiso(mcp, rn);
      if (flipOmega == 0.0)
        continue;
#ifdef EXTENDED_BE
      benergy = ExtendedBoundaryEnergy(mcp, mcn);
#else
      benergy = BoundaryEnergy(mcp, mcn);
#endif
      fi1_bacup = mcp->Get_fi1();
      Fi_bacup = mcp->Get_fi();
      fi2_bacup = mcp->Get_fi2();
      orientationIdBacup = mcp->GetOrientationId();
      mcp->Set(rn->Get_fi1(), rn->Get_fi(), rn->Get_fi2(), IN_RAD);
      mcp->SetOrientationId(rn->GetOrientationId());
#ifdef EXTENDED_BE
      newbenergy = ExtendedBoundaryEnergy(mcp, mcn);
#else
      newbenergy = BoundaryEnergy(mcp, mcn);
#endif
      deltaEnergy = newbenergy - benergy;

      if (deltaEnergy <= 0.0) // UWAGA na <=0.0
      {
        propability = Mobility(flipOmega);
        // propability = -Mobility(flipOmega)*deltaEnergy/(1.0);
        // propability = -deltaEnergy/(6.5f);
        // propability = 1.0;
      } else {
#ifdef TZERO
        propability = 0.0;
#else
        propability =
            Mobility(flipOmega) * exp(-deltaEnergy); // NICE PROBLEM
!!!!!!!!!! #endif
      }

      if (r.RandFraction() < propability) {
        mcp->SetSE(0.0);
        mcp->UpdateColor();
        mcp->SetId(rn->GetId());
      } else {
        mcp->Set(fi1_bacup, Fi_bacup, fi2_bacup, IN_RAD);
        mcp->SetOrientationId(orientationIdBacup);
      }

    } // EOF for
    mcstep++;
    GGReport(50);
  } // EOF while
  std::cout << "GG STOP\n";
}

#ifdef OTHER_MODELS

void NeighboringZonePM::Recrystallization() {
  MCBox *h2d = _box;
  //----- REKRYSTALIZACJA ------//
  std::cout << "Rozpoczynam Test\n";
  system("pause");

  rec = true;
  Neighbours n;
  MCPoint *mcp;
  MCPoint *rn;
  int mcstep = 0;
  float rec_frac = 0.0f;
  float perc = 0.01f;
  bool plot = true;
  unsigned int numOfAttemps = 0;
  unsigned int numOfPoints =
      h2d->Get_xsize() * h2d->Get_ysize() * h2d->Get_zsize();
  int maxRange = h2d->Get_xsize();
  int range = 1;

  MCPoint *initialNucleus;
  for_ijks(i, j, k, h2d) if ((*h2d)(i, j, k)->IsNucleus() == true)
      initialNucleus = (*h2d)(i, j, k);

  for_ijks(i, j, k, h2d) (*h2d)(i, j, k)->IsActive(false);

  initialNucleus->IsActive(true);
  std::vector<MCPoint *> rexVector;
  rexVector.push_back(initialNucleus);

  Neighbours nv;
  rexVector[0]->GetNeighbours(nv);
  for (unsigned int n = 0; n < nv.size(); ++n) {
    if (nv[n]->IsActive() == false) {
      nv[n]->IsActive(true);
      rexVector.push_back(nv[n]);
    }
  }
  unsigned int startPosition = 1;

  while (rec) {
    mcstep++;
    std::vector<MCPoint *> gridPoints;
    for_ijks(i, j, k, _box) gridPoints.push_back((*_box)(i, j, k));
    for (unsigned int iter = 0; iter < numOfPoints; ++iter) {

      int p = r.Rand(gridPoints.size());
      mcp = gridPoints[p];
      gridPoints.erase(gridPoints.begin() + p);
      if (mcp->GetSE() != 0.0) {
        numOfAttemps = 0;
        Neighbours n;
        mcp->GetNeighbours(n);
        rn = n[r.Rand(n.size())];

        if (rn->GetSE() == 0.0) // Random Neighbour IS Recrystallized !
        {
          float propability;
          if (mcp->IsActive() == true) {
            propability = 1.0;
          } else
            propability = 0.0;

          if (r.RandFraction() < propability) {
            mcp->Set(rn->Get_fi1(), rn->Get_fi(), rn->Get_fi2());
            mcp->SetSE(0.0);
            mcp->UpdateColor();
            rec_frac = RecFraction();

            bool rangeIncrease = true;
            for (Pos i = startPosition; i < rexVector.size(); ++i)
              if (rexVector[i]->GetSE() != 0.0) {
                rangeIncrease = false;
                break;
              }

            if ((rangeIncrease == true) && (range > 0)) {
              cout << "RANGE: " << range << endl;
              cout << "Number of Rex Points " << rexVector.size() << " "
                   << rec_frac * numOfPoints << endl;
              jmakFile << mcstep << "\t" << rec_frac << "\t"
                       << log(1.0 * mcstep) << "\t" << log(-log(1.0 -
rec_frac))
                       << endl;
              fprintf(gnuplotPipe, "plot 'Output/jmak.txt' \n");
              fflush(gnuplotPipe);

              std::cout << "Zrekrystalizowane " << rec_frac << "\n";
              std::ostringstream stm;
              stm.setf(ios::fixed, ios::floatfield);
              stm.precision(2);
              stm << range;
              h2d->ExportBinaryVtk("Output/rec_0" + stm.str() + ".vtk");

              unsigned int stopPosition = rexVector.size();
              for (Pos v = startPosition; v < stopPosition; ++v) {
                Neighbours nv;
                rexVector[v]->GetNeighbours(nv);
                for (unsigned int n = 0; n < nv.size(); ++n) {
                  if (nv[n]->IsActive() == false) {
                    nv[n]->IsActive(true);
                    rexVector.push_back(nv[n]);
                  }
                }
              }
              startPosition = stopPosition;
              range++;
            }
          }

        } // EOF IF
      } else
        numOfAttemps++;

      if ((numOfAttemps > numOfPoints) || (range >= maxRange)) {
        plot = false;
        rec = false;
        break;
      }
    } // EOF for
  }   // EOF while

  std::cout << "Liczba krokow = " << mcstep << "\n";
  jmakFile.close();
  fprintf(gnuplotFitPipe, "load 'plot.sh' \n");
  fflush(gnuplotFitPipe);

  std::cout << "Rekrystalizacja zakonczona\n";
}

void EqualDistancePM::Recrystallization() {
  MCBox *h2d = _box;
  //----- REKRYSTALIZACJA ------//
  std::cout << "Rozpoczynam Test\n";
  system("pause");

  rec = true;
  Neighbours n;
  MCPoint *mcp;
  MCPoint *rn;
  int mcstep = 0;
  float rec_frac = 0.0f;
  float perc = 0.01f;
  bool plot = true;
  unsigned int numOfAttemps = 0;
  unsigned int numOfPoints =
      h2d->Get_xsize() * h2d->Get_ysize() * h2d->Get_zsize();
  int maxRange = h2d->Get_xsize();
  int range = 1;

  MCPoint *initialNucleus;
  for_ijks(i, j, k, h2d) if ((*h2d)(i, j, k)->IsNucleus() == true)
      initialNucleus = (*h2d)(i, j, k);

  for_ijks(i, j, k, h2d) (*h2d)(i, j, k)->IsActive(false);

  initialNucleus->IsActive(true);
  std::vector<MCPoint *> rexVector;
  rexVector.push_back(initialNucleus);

  for_ijks(i, j, k, h2d) {
    if ((*h2d)(i, j, k)->IsActive() == false)
      if (rexVector[0]->GetDistance((*h2d)(i, j, k)) <= 1.0 * range + 0.0001f)
{ rexVector.push_back((*h2d)(i, j, k));
        (*h2d)(i, j, k)->IsActive(true);
      }
  }
  unsigned int startPosition = 1;

  while (rec) {
    mcstep++;
    std::vector<MCPoint *> gridPoints;
    for_ijks(i, j, k, _box) gridPoints.push_back((*_box)(i, j, k));
    for (unsigned int iter = 0; iter < numOfPoints; ++iter) {
      int p = r.Rand(gridPoints.size());
      mcp = gridPoints[p];
      gridPoints.erase(gridPoints.begin() + p);

      if (mcp->GetSE() != 0.0) {
        numOfAttemps = 0;
        Neighbours n;
        mcp->GetNeighbours(n);

        rn = n[r.Rand(n.size())];

        if (rn->GetSE() == 0.0) // Random Neighbour IS Recrystallized !
        {
          float propability;
          if (mcp->IsActive() == true) {
            propability = 1.0;
          } else
            propability = 0.0;

          if (r.RandFraction() < propability) {
            mcp->Set(rn->Get_fi1(), rn->Get_fi(), rn->Get_fi2());
            mcp->SetSE(0.0);
            mcp->UpdateColor();
            rec_frac = RecFraction();

            bool rangeIncrease = true;
            for (Pos i = startPosition; i < rexVector.size(); ++i)
              if (rexVector[i]->GetSE() != 0.0) {
                rangeIncrease = false;
                break;
              }

            if ((rangeIncrease == true) && (range > 0)) {
              cout << "RANGE: " << range << endl;
              cout << "Number of Rex Points " << rexVector.size() << " "
                   << rec_frac * numOfPoints << endl;
              jmakFile << mcstep << "\t" << rec_frac << "\t"
                       << log(1.0 * mcstep) << "\t" << log(-log(1.0 -
rec_frac))
                       << endl;
              fprintf(gnuplotPipe, "plot 'Output/jmak.txt' \n");
              fflush(gnuplotPipe);

              std::cout << "Zrekrystalizowane " << rec_frac << "\n";
              std::ostringstream stm;
              stm.setf(ios::fixed, ios::floatfield);
              stm.precision(2);
              stm << range;
              h2d->ExportBinaryVtk("Output/rec_0" + stm.str() + ".vtk");
              range++;
              unsigned int stopPosition = rexVector.size();
              for_ijks(i, j, k, h2d) {
                if ((*h2d)(i, j, k)->IsActive() == false)
                  if (rexVector[0]->GetDistance((*h2d)(i, j, k)) <=
                      1.0 * range + 0.0001f) {
                    rexVector.push_back((*h2d)(i, j, k));
                    (*h2d)(i, j, k)->IsActive(true);
                  }
              }
              startPosition = stopPosition;
            }
          }

        } // EOF IF
      } else
        numOfAttemps++;

      if ((numOfAttemps > numOfPoints) || (range >= maxRange)) {
        plot = false;
        rec = false;
        break;
      }
    } // EOF for
  }   // EOF while

  std::cout << "Liczba krokow = " << mcstep << "\n";
  jmakFile.close();
  fprintf(gnuplotFitPipe, "load 'plot.sh' \n");
  fflush(gnuplotFitPipe);

  std::cout << "Rekrystalizacja zakonczona\n";
}
#endif

*/