#include "Nucleation.h"
#include <fstream>
namespace Physics {
void Nucleation::saveBungeAngles() {
  std::ofstream file("nucleation.eul");
  for (auto &nuclei : nuclei_vec) {
    auto bunge_angles = Orientation::Converter::QuaternionToBungeAngles(
        nuclei->getOrientation());
    file << bunge_angles.getPhi1() << "\t" << bunge_angles.getPhi() << "\t"
         << bunge_angles.getPhi2() << "\t 1.0 \n";
  }
  file.close();
}

void enlargeNucleis(const int &iter = 1, const bool &circle = true) {
#ifdef ENLARGE
  int iterations = 0;
  unsigned int startPosition = 0;
  unsigned int nuclei_number;
  Neighbours nv;
  MCPoint *p;
  if (circle == true) {
    nuclei_number = nuclei_vec.size();
    for (Pos v = 0; v < nuclei_number; ++v) {
      for_ijks(i, j, k, _box) {
        p = (*_box)(i, j, k);
        if ((nuclei_vec[v]->GetDistance(p) <= 1.0 * iter + 0.0001f) &&
            (p->IsNucleus() == false)) {
          p->IsNucleus(true);
          p->SetSE(0.0);
          p->SetId(nuclei_vec[v]->GetId());
          p->Set(nuclei_vec[v]->Get_fi1(), nuclei_vec[v]->Get_fi(),
                 nuclei_vec[v]->Get_fi2(), IN_RAD);
          p->UpdateColor();
          nuclei_vec.push_back(p);
        }
      }
    }
  } else {
    while (iterations < iter) {
      unsigned int nuclei_number = nuclei_vec.size();
      for (Pos v = startPosition; v < nuclei_number; ++v) {
        Neighbours nv;
        nuclei_vec[v]->GetNeighbours(nv);
        for (unsigned int n = 0; n < nv.size(); ++n) {
          if (nv[n]->IsNucleus() == false) {
            nv[n]->IsNucleus(true);
            nv[n]->SetSE(0.0);
            nv[n]->SetId(nuclei_vec[v]->GetId());
            nv[n]->Set(nuclei_vec[v]->Get_fi1(), nuclei_vec[v]->Get_fi(),
                       nuclei_vec[v]->Get_fi2(), IN_RAD);
            nv[n]->UpdateColor();
            nuclei_vec.push_back(nv[n]);
          }
        }
      }
      startPosition = nuclei_number;
      iterations++;
    }
  }
#endif
}
} // namespace Physics