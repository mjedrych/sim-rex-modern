
#include "gtest/gtest.h"
#include <Grid/Grid.h>
#include <Orientation/Orientation.h>
#include <Physics/NucleationDecorator/NucleationDecorator.h>
#include <memory>
using namespace Physics;
TEST(NucleationDecorators, SingleNuclei) {
  Grid::SquareNeighborsFinder square;
  Grid::Structure grid(&square, 10, 10);
  Nucleation n(&grid);
  SingleNucleus sn(&n);
  sn.apply();
  EXPECT_EQ(
      grid.get(Grid::Position(5, 5, 0)).getExtensions().MonteCarlo.isNucleus(),
      true);
  EXPECT_EQ(grid.getRandom().getExtensions().MonteCarlo.isNucleus(), false);
}