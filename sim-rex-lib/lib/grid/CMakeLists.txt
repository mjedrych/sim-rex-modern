cmake_minimum_required(VERSION 3.11-3.18)

project(Grid)
set(LIBRARY_NAME
    Grid
)

add_library(Grid 
    src/Grid.cpp 
    src/Structure.cpp 
    src/SquareNeighborsFinder.cpp 
    src/HexNeighborsFinder.cpp 
    src/CubeNeighborsFinder.cpp 
    src/FccNeighborsFinder.cpp)

target_include_directories(${LIBRARY_NAME} PRIVATE
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include/${LIBRARY_NAME}>
    $<INSTALL_INTERFACE:include/${LIBRARY_NAME}>
)

target_include_directories(${LIBRARY_NAME} PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
    $<INSTALL_INTERFACE:include>
)

target_link_libraries(${LIBRARY_NAME}
    PRIVATE 
    tbb
    Common
    Orientation 
    )

target_compile_features(Grid PUBLIC cxx_std_20)