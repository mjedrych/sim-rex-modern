#include "gtest/gtest.h"
#include <Grid/Grid.h>
#include <iostream>
using namespace Grid;
TEST(Grid, Generate) {
  HexNeighborsFinder cnf;
  Structure grid(&cnf, 10, 10);

  for (auto &g : grid)
    std::cout << g.getRealPosition() << "\n";
}
