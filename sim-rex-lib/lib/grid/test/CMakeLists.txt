﻿cmake_minimum_required(VERSION 3.11-3.18)

project(Grid_Tests)

set(LIBRARY_NAME Grid)

set(GridTests ${LIBRARY_NAME}_GridTests)
    add_executable(${GridTests} GridTests.cpp)
    add_test(NAME ${GridTests} COMMAND ${GridTests})
    target_link_libraries(${GridTests} PRIVATE Grid Common Orientation gtest_main)



