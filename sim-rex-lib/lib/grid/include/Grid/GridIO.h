#pragma once
#include "GridPoint.h"
#include "Structure.h"
namespace Grid {
class GridIO {
public:
  static Structure ImportOsc(const std::string &, int = 0) {
    return Structure(nullptr, 0, 0);
  }
  static Structure ImportAng(const std::string &, int = 0) {
    return Structure(nullptr, 0, 0);
  }

  static void ExportAng(const Structure &, const std::string &, int = 0) {}
  static void ExportOsc(const Structure &, const std::string &, int = 0) {}
  static void ExportBinaryVtk(const Structure &, const std::string &) {}
  static void ExportVtk(const Structure &, const std::string &) {}
  static void ExportSPVtk(const Structure &, const std::string &) {
  } // DEPRECATED
  static void ExportEulers(const Structure &, const std::string &) {}
};
} // namespace Grid