#pragma once
#include "NeighborsFinder.h"
namespace Grid {

class CubeNeighborsFinder : public NeighborsFinder {
public:
  // CubeNeighborsFinder(Structure *g) : NeighborsFinder(g) {}
  void firstOrderGridPositions(GridPoint &gp);
  void secondOrderGridPositions(GridPoint &gp);
  void calculateRealPositions(GridPoint &gp);
};
} // namespace Grid