#pragma once
#include "NeighborsFinder.h"
namespace Grid {

class FccNeighborsFinder : public NeighborsFinder {
public:
  // FccNeighborsFinder(Structure *g) : NeighborsFinder(g) {}
  void firstOrderGridPositions(GridPoint &gp);
  void secondOrderGridPositions(GridPoint &gp);
  void calculateRealPositions(GridPoint &gp);
};
} // namespace Grid