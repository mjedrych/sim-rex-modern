#pragma once
#include "NeighborsFinder.h"
namespace Grid {

class HexNeighborsFinder : public NeighborsFinder {
public:
  // HexNeighborsFinder(Structure *g) : NeighborsFinder(g) {}
  void firstOrderGridPositions(GridPoint &gp);
  void secondOrderGridPositions(GridPoint &gp);
  void calculateRealPositions(GridPoint &gp);

private:
  void shiftPositionsX(std::vector<GridPosition> &gpv, int shift);
};
} // namespace Grid