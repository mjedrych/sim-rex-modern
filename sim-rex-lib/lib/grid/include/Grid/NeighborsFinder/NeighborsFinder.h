#pragma once
#include "../GridPoint.h"
#include "../Structure.h"
#include <algorithm>
#include <execution>
#include <functional>
#include <vector>
namespace Grid {
class NeighborsFinder {
public:
  NeighborsFinder() {}
  NeighborsFinder(Structure *g) : grid(g) {}
  virtual void firstOrderGridPositions(GridPoint &) = 0;
  virtual void secondOrderGridPositions(GridPoint &) = 0;
  virtual void calculateRealPositions(GridPoint &gp) = 0;

  void setGrid(Structure *g) { grid = g; }
  void firstOrderNeighbors(GridPoint &gp) {
    firstOrderGridPositions(gp);
    loopPositions(px,
                  [&gp](GridPoint &p) { gp.getNeighbors().addFirstOrderX(p); });
    loopPositions(py,
                  [&gp](GridPoint &p) { gp.getNeighbors().addFirstOrderY(p); });
    loopPositions(pz,
                  [&gp](GridPoint &p) { gp.getNeighbors().addFirstOrderZ(p); });
    gp.getNeighbors().mergeFirstNeighbors();
  }

  void firstOrderNeighbors() {
    std::for_each(std::execution::par_unseq, grid->begin(), grid->end(),
                  [&](auto &&item) { firstOrderNeighbors(item); });
  }

  void secondOrderNeighbors(GridPoint &gp) {
    secondOrderGridPositions(gp);
    loopPositions(
        px, [&gp](GridPoint &p) { gp.getNeighbors().addSecondOrderX(p); });
    loopPositions(
        py, [&gp](GridPoint &p) { gp.getNeighbors().addSecondOrderY(p); });
    loopPositions(
        pz, [&gp](GridPoint &p) { gp.getNeighbors().addSecondOrderZ(p); });
    gp.getNeighbors().mergeSecondNeighbors();
  }

  void secondOrderNeighbors() {
    std::for_each(std::execution::par_unseq, grid->begin(), grid->end(),
                  [&](auto &&item) { secondOrderNeighbors(item); });
  }
  void calculateRealPositions() {
    std::for_each(std::execution::par_unseq, grid->begin(), grid->end(),
                  [&](auto &&item) { calculateRealPositions(item); });
  }

protected:
  std::vector<GridPosition> px;
  std::vector<GridPosition> py;
  std::vector<GridPosition> pz;

  std::vector<GridPosition> p2x;
  std::vector<GridPosition> p2y;
  std::vector<GridPosition> p2z;

private:
  void loopPositions(const std::vector<GridPosition> &positions,
                     const std::function<void(GridPoint &p)> &f) {
    GridPosition max_pos = grid->getLastGridPosition();
    for (auto pos : positions)
      if (pos.hasMinus() || pos.isGreaterThan(max_pos)) {

      } else {
        f(grid->get(pos));
      }
  }
  Structure *grid = nullptr;
  bool periodic_conditions = true;
};
} // namespace Grid