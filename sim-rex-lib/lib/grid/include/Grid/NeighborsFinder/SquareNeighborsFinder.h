#pragma once
#include "NeighborsFinder.h"
namespace Grid {

class SquareNeighborsFinder : public NeighborsFinder {
public:
  // SquareNeighborsFinder(Structure *g) : NeighborsFinder(g) {}
  void firstOrderGridPositions(GridPoint &gp);
  void secondOrderGridPositions(GridPoint &gp);
  void calculateRealPositions(GridPoint &gp);
};
} // namespace Grid