#pragma once
#include "GridPoint.h"
#include <Common/RandGen.h>
#include <vector>
namespace Grid {

class NeighborsFinder;
class Structure {
public:
  Structure(NeighborsFinder *nf, unsigned int x, unsigned int y,
            unsigned int z = 1);

  auto begin() noexcept { return data.begin(); }
  auto begin() const noexcept { return data.cbegin(); };
  auto end() noexcept { return data.end(); }
  auto end() const noexcept { return data.cend(); };
  auto getDimensions() {
    return GridPosition((int)max_x, (int)max_y, (int)max_z);
  }
  size_t getSize() { return max_x * max_y * max_z; }
  auto getLastGridPosition() {
    return GridPosition((int)max_x - 1, (int)max_y - 1, (int)max_z - 1);
  }

  auto &get(const GridPosition &p) { return data[index(p)]; }
  auto &get(const size_t &i) { return data[i]; }
  auto &getRandom() { return get(Common::RandGen::genUInt(getSize() - 1)); }

private:
  size_t index(const GridPosition &p) {
    return p.getZ() * max_y * max_x + p.getY() * max_x + p.getX();
  }

  std::vector<GridPoint> data;
  // NeighborsFinder *nf;
  unsigned int max_x;
  unsigned int max_y;
  unsigned int max_z;
};
} // namespace Grid