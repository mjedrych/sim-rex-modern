#pragma once
#include "GridPoint.h"
#include <Common/RandGen.h>
#include <vector>
namespace Grid {
class GridPoint;
class Neighbors {
public:
  auto begin() noexcept { return n.begin(); }
  auto begin() const noexcept { return n.cbegin(); };
  auto end() noexcept { return n.end(); }
  auto end() const noexcept { return n.cend(); };

  void addFirstOrderX(GridPoint &gp) { n1x.emplace_back(&gp); }
  void addFirstOrderY(GridPoint &gp) { n1y.emplace_back(&gp); }
  void addFirstOrderZ(GridPoint &gp) { n1z.emplace_back(&gp); }
  void addSecondOrderX(GridPoint &gp) { n2x.emplace_back(&gp); }
  void addSecondOrderY(GridPoint &gp) { n2y.emplace_back(&gp); }
  void addSecondOrderZ(GridPoint &gp) { n2z.emplace_back(&gp); }

  void mergeFirstNeighbors() {
    n1.clear();
    n1.insert(n1.end(), n1x.begin(), n1x.end());
    n1.insert(n1.end(), n1y.begin(), n1y.end());
    n1.insert(n1.end(), n1z.begin(), n1z.end());
  }
  void mergeSecondNeighbors() {
    n2.clear();
    n2.insert(n2.end(), n2x.begin(), n2x.end());
    n2.insert(n2.end(), n2y.begin(), n2y.end());
    n2.insert(n2.end(), n2z.begin(), n2z.end());
  }
  void mergeAll() {
    n.clear();
    n.insert(n.end(), n1.begin(), n1.end());
    n.insert(n.end(), n2.begin(), n2.end());
  }

  const auto &getFirstOrder() { return n1; }
  const auto &getSecondOrder() { return n2; }
  const auto &getFirstOrderX() { return n1x; }
  const auto &getFirstOrderY() { return n1y; }
  const auto &getFirstOrderZ() { return n1z; }
  auto &getRandomNearestNeighbor() {
    return n1[Common::RandGen::genUInt(n1.size() - 1)];
  }

private:
  std::vector<GridPoint *> n;
  std::vector<GridPoint *> n1, n2; // all 1st and 2nd neighbors
  std::vector<GridPoint *> n1x;    // first order neighbors - x direction
  std::vector<GridPoint *> n1y;
  std::vector<GridPoint *> n1z;
  std::vector<GridPoint *> n2x; // first order neighbors - x direction
  std::vector<GridPoint *> n2y;
  std::vector<GridPoint *> n2z;
};
} // namespace Grid