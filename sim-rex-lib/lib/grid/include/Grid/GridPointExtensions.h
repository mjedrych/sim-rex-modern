#pragma once
#include "EBSDPoint.h"
#include "MCPoint.h"
namespace Grid {
class GridPointExtensions {
public:
  MCPoint MonteCarlo;
  EBSDPoint Ebsd;
};
} // namespace Grid