#pragma once
#include <Orientation/BungeAngles.h>
namespace Grid {
class EBSDPoint {
private:
  Orientation::BungeAngles<double> bangles;
  float iq{0.0f};
  float ci{0.0f};
  bool is_active{false};
};
} // namespace Grid