#pragma once
#include <cmath>
#include <iostream>
namespace Grid {

template <typename T> class Position {
public:
  Position() : x(0), y(0), z(0) {}
  Position(T xp, T yp, T zp = 0) : x(xp), y(yp), z(zp) {}
  void set(T x, T y, T z = 0) {
    this->x = x;
    this->y = y;
    this->z = z;
  }
  Position operator+(const Position &);
  Position operator*(T);
  Position operator/(T);
  Position get() { return *this; }

  T getX() const { return x; }
  T getY() const { return y; }
  T getZ() const { return z; }

  void shiftX(T shift) { x = x + shift; }

  double squaredDistanceTo(const Position &sp) {
    return (x - sp.getX()) * (x - sp.getX()) +
           (y - sp.getY()) * (y - sp.getY()) +
           (z - sp.getZ()) * (z - sp.getZ());
  }

  bool hasMinus() {
    return std::signbit(x) || std::signbit(y) || std::signbit(z);
  }

  bool isGreaterThan(const Position &p) {
    if (x > p.getX() || y > p.getY() || z > p.getZ())
      return true;
    return false;
  }

  friend std::ostream &operator<<(std::ostream &out, Position p) {
    out << "X: " << p.getX() << ", Y: " << p.getY() << ", Z: " << p.getZ()
        << "\n";
    return out;
  }

private:
  T x;
  T y;
  T z;
};

template <typename T> Position<T> Position<T>::operator+(const Position<T> &p) {
  Position result;
  result.x = x + p.x;
  result.y = y + p.y;
  result.z = z + p.z;
  return result;
}

template <typename T> Position<T> Position<T>::operator*(T a) {
  Position result;
  result.x = x * a;
  result.y = y * a;
  result.z = z * a;
  return result;
}

template <typename T> Position<T> Position<T>::operator/(T a) {
  Position result;
  result.x = x / a;
  result.y = y / a;
  result.z = z / a;
  return result;
}
typedef Position<double> RealPosition;
typedef Position<int> GridPosition;
} // namespace Grid