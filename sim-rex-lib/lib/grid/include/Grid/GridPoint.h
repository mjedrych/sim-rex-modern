#pragma once

#include "GridPointExtensions.h"
#include "Neighbors.h"
#include "Position.h"
#include <Common/Color.h>
#include <Orientation/Orientation.h>

using Common::Color;
namespace Grains {
class Grain;
class Boundary;
} // namespace Grains

typedef Grains::Grain *GrainPtr;
namespace Grid {
class GridPoint {
public:
  GridPoint() {}
  GridPoint(const GridPosition &pgp, int pid) : gp(pgp), id(pid) {}
  const auto &getGrain() { return g; }
  void setGrain(GrainPtr g) { this->g = g; }

  auto getId() { return id; }
  void setId(size_t pid) { id = pid; }
  void setGridPosition(const GridPosition &pgp) { gp = pgp; }
  void setRealPosition(double x, double y, double z = 0.0) { r.set(x, y); }
  void setColor(const Color &rgb) { color = rgb; }
  void updateColor(){};
  void setOrientation(const Orientation::Quaternion<double> &o) {
    orientation = o;
  }
  auto &&getOrientation() { return orientation; }
  const auto &getRealPosition() const { return r; }
  const auto &getGridPosition() const { return gp; }
  const auto &getNeighbors() const { return n; }
  auto &getNeighbors() { return n; }
  auto &getExtensions() { return extensions; }
  friend std::ostream &operator<<(std::ostream &out, GridPoint p) {
    out << "ID: " << p.getId() << ", GridPosition: " << p.getGridPosition()
        << "RealPosition: " << p.getRealPosition() << "\n";
    return out;
  }

protected:
  Orientation::Quaternion<double> orientation;
  GridPointExtensions extensions;
  size_t id;
  GridPosition gp;
  RealPosition r;
  Neighbors n;
  Grains::Boundary *gb = nullptr;
  GrainPtr g = nullptr;
  Color color;
};

} // namespace Grid