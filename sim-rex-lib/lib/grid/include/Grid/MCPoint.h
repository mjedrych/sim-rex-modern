#pragma once
namespace Grid {
class MCPoint {
public:
  void setStoredEnergy(double pse) { se = pse; }
  auto getStoredEnergy() { return he; }
  void isNucleus(bool f) { nucleus = f; }
  auto isNucleus() { return nucleus; }
  auto isRecrystallized() {
    if (se == 0.0)
      return true;
    return false;
  }

private:
  double se{0.0};
  double he{0.0};
  double bend{0.0};
  int spin{0};
  bool randed{false};
  bool nucleus{false};
};
} // namespace Grid