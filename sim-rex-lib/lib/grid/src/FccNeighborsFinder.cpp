
#include "NeighborsFinder/FccNeighborsFinder.h"
namespace Grid {
void FccNeighborsFinder::firstOrderGridPositions(GridPoint &gp) {

  const int i = gp.getGridPosition().getX();
  const int j = gp.getGridPosition().getY();
  const int k = gp.getGridPosition().getZ();
  //------- z = k -----------------//
  if (k % 3 == 0) // struktura A
    if (j % 2) {
      py[0] = GridPosition(i, j + 1, k);
      py[1] = GridPosition(i + 1, j + 1, k);
      px[0] = GridPosition(i - 1, j, k);
      px[1] = GridPosition(i + 1, j, k);
      py[2] = GridPosition(i, j - 1, k);
      py[3] = GridPosition(i + 1, j - 1, k);

      //------- z = k+1 -----------------//
      pz[0] = GridPosition(i, j, k + 1);
      pz[1] = GridPosition(i + 1, j, k + 1);
      pz[2] = GridPosition(i, j - 1, k + 1);

      //------- z = k-1 -----------------//
      pz[3] = GridPosition(i, j, k - 1);
      pz[4] = GridPosition(i, j - 1, k - 1);
      pz[5] = GridPosition(i + 1, j - 1, k - 1);
    } else {
      py[0] = GridPosition(i - 1, j + 1, k);
      py[1] = GridPosition(i, j + 1, k);
      px[0] = GridPosition(i - 1, j, k);
      px[1] = GridPosition(i + 1, j, k);
      py[2] = GridPosition(i - 1, j - 1, k);
      py[3] = GridPosition(i, j - 1, k);

      //------- z = k+1 -----------------//
      pz[0] = GridPosition(i - 1, j, k + 1);
      pz[1] = GridPosition(i, j, k + 1);
      pz[2] = GridPosition(i, j - 1, k + 1);

      //------- z = k-1 -----------------//
      pz[3] = GridPosition(i, j, k - 1);
      pz[4] = GridPosition(i - 1, j - 1, k - 1);
      pz[5] = GridPosition(i, j - 1, k - 1);
    }

  else if (k % 3 == 1) //---- struktura B
    if (j % 2) {
      py[0] = GridPosition(i - 1, j + 1, k);
      py[1] = GridPosition(i, j + 1, k);
      px[0] = GridPosition(i - 1, j, k);
      px[1] = GridPosition(i + 1, j, k);
      py[2] = GridPosition(i - 1, j - 1, k);
      py[3] = GridPosition(i, j - 1, k);

      //------- z = k+1 -----------------//
      pz[0] = GridPosition(i - 1, j, k + 1);
      pz[1] = GridPosition(i, j, k + 1);
      pz[2] = GridPosition(i, j - 1, k + 1);

      //------- z = k-1 -----------------//
      pz[3] = GridPosition(i, j + 1, k - 1);
      pz[4] = GridPosition(i - 1, j, k - 1);
      pz[5] = GridPosition(i, j, k - 1);
    } else {
      py[0] = GridPosition(i, j + 1, k);
      py[1] = GridPosition(i + 1, j + 1, k);
      px[0] = GridPosition(i - 1, j, k);
      px[1] = GridPosition(i + 1, j, k);
      py[2] = GridPosition(i, j - 1, k);
      py[3] = GridPosition(i + 1, j - 1, k);

      //------- z = k+1 -----------------//
      pz[0] = GridPosition(i, j, k + 1);
      pz[1] = GridPosition(i + 1, j, k + 1);
      pz[2] = GridPosition(i, j - 1, k + 1);

      //------- z = k-1 -----------------//
      pz[3] = GridPosition(i, j + 1, k - 1);
      pz[4] = GridPosition(i, j, k - 1);
      pz[5] = GridPosition(i + 1, j, k - 1);
    }

  else if (k % 3 == 2) //----struktura C -- !!! wzory na plaszczyznie k
                       // odpowiadaja wzorom ze strukruty A
    if (j % 2) {
      py[0] = GridPosition(i, j + 1, k);
      py[1] = GridPosition(i + 1, j + 1, k);
      px[0] = GridPosition(i - 1, j, k);
      px[1] = GridPosition(i + 1, j, k);
      py[2] = GridPosition(i, j - 1, k);
      py[3] = GridPosition(i + 1, j - 1, k);

      //------- z = k+1 -----------------//
      pz[0] = GridPosition(i, j + 1, k + 1);
      pz[1] = GridPosition(i + 1, j + 1, k + 1);
      pz[2] = GridPosition(i, j, k + 1);

      //------- z = k-1 -----------------//
      pz[3] = GridPosition(i, j + 1, k - 1);
      pz[4] = GridPosition(i, j, k - 1);
      pz[5] = GridPosition(i + 1, j, k - 1);
    } else {
      py[0] = GridPosition(i - 1, j + 1, k);
      py[1] = GridPosition(i, j + 1, k);
      px[0] = GridPosition(i - 1, j, k);
      px[1] = GridPosition(i + 1, j, k);
      py[2] = GridPosition(i - 1, j - 1, k);
      py[3] = GridPosition(i, j - 1, k);

      //------- z = k+1 -----------------//
      pz[0] = GridPosition(i - 1, j + 1, k + 1);
      pz[1] = GridPosition(i, j + 1, k + 1);
      pz[2] = GridPosition(i, j, k + 1);

      //------- z = k-1 -----------------//
      pz[3] = GridPosition(i, j + 1, k - 1);
      pz[4] = GridPosition(i - 1, j, k - 1);
      pz[5] = GridPosition(i, j, k - 1);
    }
}
void FccNeighborsFinder::secondOrderGridPositions(GridPoint &gp) {
  const int i = gp.getGridPosition().getX();
  const int j = gp.getGridPosition().getY();
  const int k = gp.getGridPosition().getZ();

  px = {GridPosition(i - 1, j, k), //
        GridPosition(i + 1, j, k)};

  //------- z = k -----------------//
  if (k % 3 == 0) // A structure
    if (j % 2) {
      py[0] = GridPosition(i, j + 1, k);
      py[1] = GridPosition(i + 1, j + 1, k);
      py[2] = GridPosition(i, j - 1, k);
      py[3] = GridPosition(i + 1, j - 1, k);

      pz[0] = GridPosition(i, j, k + 1);
      pz[1] = GridPosition(i + 1, j, k + 1);
      pz[2] = GridPosition(i, j - 1, k + 1);

      pz[3] = GridPosition(i, j, k - 1);
      pz[4] = GridPosition(i, j - 1, k - 1);
      pz[5] = GridPosition(i + 1, j - 1, k - 1);
    } else {
      py[0] = GridPosition(i - 1, j + 1, k);
      py[1] = GridPosition(i, j + 1, k);
      py[2] = GridPosition(i - 1, j - 1, k);
      py[3] = GridPosition(i, j - 1, k);

      pz[0] = GridPosition(i - 1, j, k + 1);
      pz[1] = GridPosition(i, j, k + 1);
      pz[2] = GridPosition(i, j - 1, k + 1);

      pz[3] = GridPosition(i, j, k - 1);
      pz[4] = GridPosition(i - 1, j - 1, k - 1);
      pz[5] = GridPosition(i, j - 1, k - 1);
    }

  else if (k % 3 == 1) //---- B structure
    if (j % 2) {
      py[0] = GridPosition(i - 1, j + 1, k);
      py[1] = GridPosition(i, j + 1, k);
      py[2] = GridPosition(i - 1, j - 1, k);
      py[3] = GridPosition(i, j - 1, k);

      pz[0] = GridPosition(i - 1, j, k + 1);
      pz[1] = GridPosition(i, j, k + 1);
      pz[2] = GridPosition(i, j - 1, k + 1);

      pz[3] = GridPosition(i, j + 1, k - 1);
      pz[4] = GridPosition(i - 1, j, k - 1);
      pz[5] = GridPosition(i, j, k - 1);
    } else {
      py[0] = GridPosition(i, j + 1, k);
      py[1] = GridPosition(i + 1, j + 1, k);
      py[2] = GridPosition(i, j - 1, k);
      py[3] = GridPosition(i + 1, j - 1, k);

      pz[0] = GridPosition(i, j, k + 1);
      pz[1] = GridPosition(i + 1, j, k + 1);
      pz[2] = GridPosition(i, j - 1, k + 1);

      pz[3] = GridPosition(i, j + 1, k - 1);
      pz[4] = GridPosition(i, j, k - 1);
      pz[5] = GridPosition(i + 1, j, k - 1);
    }

  else if (k % 3 == 2) //----C structure -- !!!
    // wzory na plaszczyznie k odpowiadaja wzorom ze struktury A
    if (j % 2) {
      py[0] = GridPosition(i, j + 1, k);
      py[1] = GridPosition(i + 1, j + 1, k);
      py[2] = GridPosition(i, j - 1, k);
      py[3] = GridPosition(i + 1, j - 1, k);

      pz[0] = GridPosition(i, j + 1, k + 1);
      pz[1] = GridPosition(i + 1, j + 1, k + 1);
      pz[2] = GridPosition(i, j, k + 1);

      pz[3] = GridPosition(i, j + 1, k - 1);
      pz[4] = GridPosition(i, j, k - 1);
      pz[5] = GridPosition(i + 1, j, k - 1);
    } else {
      py[0] = GridPosition(i - 1, j + 1, k);
      py[1] = GridPosition(i, j + 1, k);
      py[2] = GridPosition(i - 1, j - 1, k);
      py[3] = GridPosition(i, j - 1, k);

      pz[0] = GridPosition(i - 1, j + 1, k + 1);
      pz[1] = GridPosition(i, j + 1, k + 1);
      pz[2] = GridPosition(i, j, k + 1);

      pz[3] = GridPosition(i, j + 1, k - 1);
      pz[4] = GridPosition(i - 1, j, k - 1);
      pz[5] = GridPosition(i, j, k - 1);
    }
} // namespace Grid
void FccNeighborsFinder::calculateRealPositions(GridPoint &gp) {
  float x = 1.0f;
  float y, z;
  const int i = gp.getGridPosition().getX();
  const int j = gp.getGridPosition().getY();
  const int k = gp.getGridPosition().getZ();
  x = i * x;
  y = j * sqrt(3.0f) / 2;
  z = k * sqrt(6.0f) / 3;

  if (k % 3 == 0) {
    if (j % 2 != 0)
      x += 0.5f;
  }

  else if (k % 3 == 1) {
    y += sqrt(3.0f) / 6.0f;
    if (j % 2 == 0)
      x += 0.5f;
  }

  else if (k % 3 == 2) {
    y += 2.0f * sqrt(3.0f) / 6.0;
    if (j % 2 != 0)
      x += 0.5f;
  }
  gp.setRealPosition(x, y, z);
}
} // namespace Grid