#include "NeighborsFinder/HexNeighborsFinder.h"
#include <cmath>
namespace Grid {
void HexNeighborsFinder::shiftPositionsX(std::vector<GridPosition> &gpv,
                                         int shift) {
  for (auto &gp : gpv)
    gp.shiftX(shift);
}
void HexNeighborsFinder::firstOrderGridPositions(GridPoint &gp) {

  const int i = gp.getGridPosition().getX();
  const int j = gp.getGridPosition().getY();
  px = {GridPosition(i - 1, j), //
        GridPosition(i + 1, j)};

  py = {GridPosition(i, j + 1),     //
        GridPosition(i + 1, j + 1), //
        GridPosition(i, j - 1),     //
        GridPosition(i + 1, j - 1)};
  if (j % 2 == 0) {
    shiftPositionsX(py, -1);
  }
}
void HexNeighborsFinder::secondOrderGridPositions(GridPoint &gp) {
  const int i = gp.getGridPosition().getX();
  const int j = gp.getGridPosition().getY();
  p2x = {GridPosition(i - 2, j), //
         GridPosition(i + 2, j)};

  p2y = {GridPosition(i - 1, j + 1), //
         GridPosition(i - 1, j + 2), //
         GridPosition(i, j + 2),     //
         GridPosition(i + 1, j + 2), //
         GridPosition(i + 2, j + 1), //
         GridPosition(i + 2, j - 1), //
         GridPosition(i + 1, j - 2), //
         GridPosition(i, j - 2),     //
         GridPosition(i - 1, j - 2), //
         GridPosition(i - 1, j - 1)};
  if (j % 2 == 0) {
    shiftPositionsX(p2y, -1);
  }
}

/* corresponds well to Hex representation in OIM Software */
void HexNeighborsFinder::calculateRealPositions(GridPoint &gp) {
  float x_step = 1.0;
  float y_step = x_step * 0.5f * std::sqrt(3.0f);
  const int i = gp.getGridPosition().getX();
  const int j = gp.getGridPosition().getY();

  if (j % 2 != 0)
    gp.setRealPosition(i * (x_step) + 0.5f * x_step, j * y_step);
  else
    gp.setRealPosition(i * (x_step) + 0.5f * x_step, j * y_step);
}

} // namespace Grid