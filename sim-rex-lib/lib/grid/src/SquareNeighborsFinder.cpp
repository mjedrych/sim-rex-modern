#include "NeighborsFinder/SquareNeighborsFinder.h"
namespace Grid {
void SquareNeighborsFinder::firstOrderGridPositions(GridPoint &gp) {
  const int i = gp.getGridPosition().getX();
  const int j = gp.getGridPosition().getY();
  px = {GridPosition(i - 1, j), //
        GridPosition(i + 1, j)};

  py = {GridPosition(i - 1, j + 1), //
        GridPosition(i, j + 1),     //
        GridPosition(i + 1, j + 1), //
        GridPosition(i - 1, j - 1), //
        GridPosition(i, j - 1),     //
        GridPosition(i + 1, j - 1)};
}
void SquareNeighborsFinder::secondOrderGridPositions(GridPoint &gp) {
  const int i = gp.getGridPosition().getX();
  const int j = gp.getGridPosition().getY();
  p2x = {GridPosition(i - 2, j), //
         GridPosition(i + 2, j)};

  p2y = {
      GridPosition(i - 2, j + 1), //
      GridPosition(i, j + 2),     //
      GridPosition(i + 2, j + 1), //
      GridPosition(i - 2, j - 1), //
      GridPosition(i, j - 2),     //
      GridPosition(i + 2, j - 1), //
      // 4 points in diagonal directions
      GridPosition(i - 1, j + 2), //
      GridPosition(i + 1, j + 2), //
      GridPosition(i - 1, j - 2), //
      GridPosition(i + 1, j - 2)  //
  };
} // namespace Grid

void SquareNeighborsFinder::calculateRealPositions(GridPoint &gp) {
  float x_step = 1.0f;
  float y_step = 1.0f;
  const int i = gp.getGridPosition().getX();
  const int j = gp.getGridPosition().getY();
  gp.setRealPosition(i * x_step, j * y_step);
}
} // namespace Grid