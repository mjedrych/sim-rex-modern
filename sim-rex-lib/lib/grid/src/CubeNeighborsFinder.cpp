#include "NeighborsFinder/CubeNeighborsFinder.h"
namespace Grid {
void CubeNeighborsFinder::firstOrderGridPositions(GridPoint &gp) {
  const int i = gp.getGridPosition().getX();
  const int j = gp.getGridPosition().getY();
  const int k = gp.getGridPosition().getZ();
  px = {GridPosition(i - 1, j, k), //
        GridPosition(i + 1, j, k)};

  py = {GridPosition(i - 1, j + 1, k), //
        GridPosition(i, j + 1, k),     //
        GridPosition(i + 1, j + 1, k), //
        GridPosition(i - 1, j - 1, k), //
        GridPosition(i, j - 1, k),     //
        GridPosition(i + 1, j - 1, k)};

  //------- z = k-1 -----------------//
  pz = {GridPosition(i - 1, j + 1, k - 1), //
        GridPosition(i, j + 1, k - 1),     //
        GridPosition(i + 1, j + 1, k - 1), //
        GridPosition(i - 1, j, k - 1),     //
        GridPosition(i + 1, j, k - 1),     //
        GridPosition(i - 1, j - 1, k - 1), //
        GridPosition(i, j - 1, k - 1),     //
        GridPosition(i + 1, j - 1, k - 1), //

        //------- z = k+1 -----------------//
        GridPosition(i - 1, j + 1, k + 1), //
        GridPosition(i, j + 1, k + 1),     //
        GridPosition(i + 1, j + 1, k + 1), //
        GridPosition(i - 1, j, k + 1),     //
        GridPosition(i + 1, j, k + 1),     //
        GridPosition(i - 1, j - 1, k + 1), //
        GridPosition(i, j - 1, k + 1),     //
        GridPosition(i + 1, j - 1, k + 1)};
}
void CubeNeighborsFinder::secondOrderGridPositions(GridPoint &gp) {
  const int i = gp.getGridPosition().getX();
  const int j = gp.getGridPosition().getY();
  const int k = gp.getGridPosition().getZ();
  p2x = {GridPosition(i - 2, j, k), //
         GridPosition(i + 2, j, k)};
  p2y = {
      GridPosition(i - 2, j + 1, k), //
      GridPosition(i, j + 2, k),     //
      GridPosition(i + 2, j + 1, k), //
      GridPosition(i - 2, j - 1, k), //
      GridPosition(i, j - 2, k),     //
      GridPosition(i + 2, j - 1, k), //
      // 4 points in diagonal directions
      GridPosition(i - 1, j + 2, k), //
      GridPosition(i + 1, j + 2, k), //
      GridPosition(i - 1, j - 2, k), //
      GridPosition(i + 1, j - 2, k)  //
  };

  //------- z = k-1 -----------------//
  p2z = {GridPosition(i - 2, j + 1, k - 1), //
         GridPosition(i - 1, j + 2, k - 1), //
         GridPosition(i, j + 2, k - 1),     //
         GridPosition(i + 1, j + 2, k - 1), //

         GridPosition(i + 2, j + 1, k - 1), //
         GridPosition(i + 2, j, k - 1),     //
         GridPosition(i + 2, j - 1, k - 1), //

         GridPosition(i + 1, j - 2, k - 1), //
         GridPosition(i, j - 2, k - 1),     //
         GridPosition(i - 1, j - 2, k - 1), //
         GridPosition(i - 2, j - 1, k - 1), //

         //------- z = k+1 -----------------//
         GridPosition(i - 2, j + 1, k + 1), //
         GridPosition(i - 1, j + 2, k + 1), //
         GridPosition(i, j + 2, k + 1),     //
         GridPosition(i + 1, j + 2, k + 1), //

         GridPosition(i + 2, j + 1, k + 1), //
         GridPosition(i + 2, j, k + 1),     //
         GridPosition(i + 2, j - 1, k + 1), //

         GridPosition(i + 1, j - 2, k + 1), //
         GridPosition(i, j - 2, k + 1),     //
         GridPosition(i - 1, j - 2, k + 1), //
         GridPosition(i - 2, j - 1, k + 1)};
}

void CubeNeighborsFinder::calculateRealPositions(GridPoint &gp) {
  float x_step = 1.0f;
  float y_step = 1.0f;
  float z_step = 1.0f;
  const int i = gp.getGridPosition().getX();
  const int j = gp.getGridPosition().getY();
  const int k = gp.getGridPosition().getZ();
  gp.setRealPosition(i * x_step, j * y_step, k * z_step);
}
} // namespace Grid