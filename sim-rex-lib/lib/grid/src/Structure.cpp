#include "Structure.h"
#include "NeighborsFinder/NeighborsFinder.h"

namespace Grid {
Structure::Structure(NeighborsFinder *nf, unsigned int x, unsigned int y,
                     unsigned int z)
    : max_x(x), max_y(y), max_z(z) {

  for (int id = 0, z = 0; z < max_z; ++z)
    for (int y = 0; y < max_y; ++y)
      for (int x = 0; x < max_x; ++x)
        data.emplace_back(GridPosition(x, y, z), id++);

  nf->setGrid(this);
  nf->firstOrderNeighbors();
  for (auto &p : data)
    p.getNeighbors().mergeFirstNeighbors();
  nf->calculateRealPositions();
}
} // namespace Grid