#pragma once
// random-shuffle
#include <algorithm>
#include <numeric>
#include <random>
#include <vector>
namespace Common {
class RandGen {
public:
  static unsigned int genUInt(unsigned int max) {
    std::random_device rd;
    std::uniform_int_distribution<> distrib(0, max);
    std::mt19937 gen(rd());
    return distrib(gen);
  }

  static std::vector<unsigned int> genUniqueUIntVector(unsigned int max) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::vector<unsigned int> unique_ints(max);
    std::iota(unique_ints.begin(), unique_ints.end(), 0);
    std::shuffle(unique_ints.begin(), unique_ints.end(), gen);
    return unique_ints;
  }

  static double genProbability() {
    std::random_device rd;
    std::uniform_real_distribution<> distrib(0.0, 1.0);
    std::mt19937 gen(rd());
    return distrib(gen);
  }
};
} // namespace Common