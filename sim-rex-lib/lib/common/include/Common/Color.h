#pragma once
namespace Common {
class Color {
public:
  Color() {}
  Color(unsigned char rr, unsigned char gg, unsigned char bb)
      : r(rr), g(gg), b(bb) {}
  void set(unsigned char r, unsigned char g, unsigned char b) {
    this->r = r;
    this->g = g;
    this->b = b;
  }

private:
  unsigned char r, g, b;
};
} // namespace Common