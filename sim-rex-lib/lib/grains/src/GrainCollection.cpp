#include "GrainCollection.h"
#include <algorithm>
#include <cmath>
#include <execution>
#include <fstream>
#include <iostream>
#include <iterator>
namespace Grains {

void GrainCollection::buildAlgorithm(const std::function<void(Grain &)> &f) {
  all_grains.clear();

  std::for_each(std::execution::par_unseq, grid->begin(), grid->end(),
                [](auto &&item) { item.setGrain(nullptr); });

  std::for_each(grid->begin(), grid->end(), [&f, this](auto &&item) {
    if (item.getGrain() == nullptr) {
      Grain g(&item);
      f(g);
      all_grains.emplace_back(g);
    }
  });
  // TODO error check!
  stat_grains_end =
      std::partition(all_grains.begin(), all_grains.end(),
                     [this](auto &&g) { return g.getSize() > min_gs; });

  // std::copy_if(all_grains.begin(), all_grains.end(),
  //              std::back_inserter(stat_grains),
  //              [this](auto &&item) { return item.getSize() > min_gs; });
}

void GrainCollection::buildAllUsingId() {
  buildAlgorithm([](Grain &g) { g.build(); });
}

void GrainCollection::buildAll() {
  buildAlgorithm([this](Grain &g) { g.build(min_miso_angle); });
}

void GrainCollection::calcAvGs() {
  if (getStatGrainsNumber())
    av_gs = 0.0;
  else {
    double sum = 0.0;
    std::for_each(all_grains.begin(), stat_grains_end,
                  [&sum](auto &&i) { sum += i.getSize(); });
    av_gs = sum / getStatGrainsNumber();
  }
}

void GrainCollection::calcAvMisoAngle() {
  if (getStatGrainsNumber())
    av_gs = 0.0;
  else {
    std::for_each(std::execution::par_unseq, all_grains.begin(),
                  all_grains.end(), [](auto &&i) { i.calcAvMisoAngle(); });
    double sum = 0.0;
    std::for_each(all_grains.begin(), stat_grains_end,
                  [&sum](auto &&i) { sum += i.getAvMisoAngle(); });
    av_gs = sum / getStatGrainsNumber();
  }
}

void GrainCollection::printStatGrains() {
  std::cout << "Grains Number: = " << getStatGrainsNumber() << "\n";
  for (auto &g : std::vector(all_grains.begin(), stat_grains_end))
    std::cout << " Grain size of grain nr: " << g.getId() << " = "
              << g.getSize() << "\n";
}

void GrainCollection::saveGrains() {
  std::ofstream file("grains_log.txt");
  file << "Grains Number: = " << getStatGrainsNumber() << "\n";
  file << "ID \t size [points] \t sqrt(size)\n";
  for (auto &g : std::vector(all_grains.begin(), stat_grains_end))
    file << g.getId() << g.getSize() << std::sqrt(1.0f * g.getSize()) << "\n";
}
#ifdef BOUNDARY
void Grains::BoundaryCurvature() {
  for (Pos i = 0; i < _grains.size(); ++i)
    _grains[i]->BoundaryCurvatureAdv();
}

void Grains::ColorBoundaryCurvature() {
  Color c;
  float curvature;
  for_ijks(i, j, k, _box) if (_box->Get(i, j, k)->InSortedBoundary()) {
    curvature = (*_box)(i, j, k)->GetCurvature();
    if ((curvature < 10.0f) && (curvature >= 0.0f)) {
      c = 255 * curvature / 10.0f;
      (*_box)(i, j, k)->SetColor(c, 0, 0);
    }

    else if ((curvature > -10.0f) && (curvature <= -0.0f)) {
      c = -255 * curvature / 10.0f;
      (*_box)(i, j, k)->SetColor(0, 0, c);
    }

    else if ((curvature >= 10.0f) && (curvature < 20.0f)) {
      c = 255 * curvature / 20.0f;
      (*_box)(i, j, k)->SetColor(255, c, 0);
    }

    else if ((curvature <= -10.0f) && (curvature > -20.0f)) {
      c = -255 * curvature / 10.0f;
      (*_box)(i, j, k)->SetColor(0, c, 255);
    } else if (curvature >= 20.0f) {
      c = 255 * curvature / 50.0f;
      (*_box)(i, j, k)->SetColor(255, 255, c);
    }

    else if (curvature <= -20.0f) {
      c = -122 * curvature / 50.0f;
      (*_box)(i, j, k)->SetColor(c, 255, c);
    } else {
      (*_box)(i, j, k)->SetColor(0, 255, 0);
    }
  }
}
* /

    void Grains::ColorBoundaryCurvature() {
  float max_distance = 0.0f;
  Pos grain = 0;
  for (Pos i = 0; i < _grains.size(); ++i)
    if (_grains[i]->MaxBoundaryCurvature() > max_distance) {
      max_distance = _grains[i]->MaxBoundaryCurvature();
      grain = i;
    }

  cout << max_distance << endl;
  _grains[grain]->SetGrainColor(255, 0, 0);
  float curvature;
  Color c;
  for_ijks(i, j, k, _box) if (_box->Get(i, j, k)->InSortedBoundary()) {
    curvature = (*_box)(i, j, k)->GetCurvature();
    if (curvature >= 0.0000f) {
      c = 255 * curvature / max_distance;
      (*_box)(i, j, k)->SetColor(c, 0, 0);
    } else {
      c = -255 * curvature / max_distance;
      (*_box)(i, j, k)->SetColor(0, 0, c);
    }
  }
}
void Grains::ColorBoundaryCurvatureByGradient() {
  float max_distance = 0.0f;
  Pos grain = 0;
  for (Pos i = 0; i < _grains.size(); ++i)
    if (_grains[i]->MaxBoundaryCurvature() > max_distance) {
      max_distance = _grains[i]->MaxBoundaryCurvature();
      grain = i;
    }

  cout << max_distance << endl;
  //_grains[grain]->SetGrainColor(255,0,0);
  float curvature;
  Color c;
  for_ijks(i, j, k, _box) if (_box->Get(i, j, k)->InSortedBoundary()) {
    curvature = (*_box)(i, j, k)->GetCurvature();
    if (curvature >= 0.0000f) {
      if (curvature < 0.25f * max_distance) {
        c = 255 * curvature / (0.25f * max_distance);
        (*_box)(i, j, k)->SetColor(c, c,
                                   c / 2.0f); // 255,255,128 - yellow
      } else if ((curvature >= 0.25 * max_distance) &&
                 (curvature < 0.5f * max_distance)) {
        c = 255 * (curvature - 0.25 * max_distance) / (0.25f * max_distance);
        (*_box)(i, j, k)->SetColor(255, 255 - 90 * c / 255.0f,
                                   128 - c / 2.0f); // 255,165,0 - orange
      } else if ((curvature >= 0.5 * max_distance) &&
                 (curvature < 0.75f * max_distance)) {
        c = 255 * (curvature - 0.5 * max_distance) / (0.25f * max_distance);
        (*_box)(i, j, k)->SetColor(255, 165 - 100 * c / 255.0f,
                                   0); // 255,65,0 - orange red
      } else if (curvature >= 0.75 * max_distance) {
        c = 100 * (curvature - 0.75 * max_distance) / (0.25f * max_distance);
        (*_box)(i, j, k)->SetColor(255 - 80 * c / 255.0f, 40 * c / 255.0f,
                                   40 * c / 255.0f); // 175-40-40 firebrick
      }
    } else if (curvature < -0.00000f) {
      if (curvature > -0.25f * max_distance) {
        c = -255 * curvature / (0.25f * max_distance);
        (*_box)(i, j, k)->SetColor(135 * c / 255.0f, 206 * c / 255.0f,
                                   c); // 135,206,255 - sky blue
      } else if ((curvature > -0.5 * max_distance) &&
                 (curvature <= -0.25f * max_distance)) {
        c = -255 * (curvature + 0.25 * max_distance) / (0.25f * max_distance);
        (*_box)(i, j, k)->SetColor(
            135 - 70 * c / 255.0f, 206 - 100 * c / 255.0f,
            255 - 30 * c / 255.0f); // Royal Blue 65-106-225
      } else if ((curvature > -0.75 * max_distance) &&
                 (curvature <= -0.5f * max_distance)) {
        c = -255 * (curvature + 0.5 * max_distance) / (0.25f * max_distance);
        (*_box)(i, j, k)->SetColor(65 - 65 * c / 255.0f, 106 - 106 * c / 255.0f,
                                   225 + 25 * c / 255.0f); // 0 0 255
      } else if (curvature <= -0.75 * max_distance) {
        c = -255 * (curvature - 0.8 * max_distance) / (0.25f * max_distance);
        (*_box)(i, j, k)->SetColor(160 * c / 255.0f, 32 * c / 255.0f,
                                   255 - 15 * c / 255.0f); // purple 160-32-240
      }
    }
  }
}

void Grains::CalculateBoundaries() {
  for (Pos i = 0; i < _grains.size(); ++i)
    _grains[i]->FindBoundaryByGrainId();
}

void Grains::SortBoundary2D() {
  for (Pos i = 0; i < _grains.size(); ++i)
    _grains[i]->SortBoundary2D();
}
#endif
} // namespace Grains