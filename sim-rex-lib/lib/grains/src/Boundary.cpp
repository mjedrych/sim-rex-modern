
#include "Boundary.h"
namespace Grains {}
#ifdef COMMENT
void Grain::SetGrainBoundaryColor(Color r, Color g, Color b) {
  for (Pos i = 0; i < _boundary.size(); ++i)
    _boundary[i]->SetColor(r, g, b);
}

void Grain::FindBoundaryByGrainId(Grain &) {
  _boundary.clear();
  Neighbours n;
  for (Pos p = 0; p < _g.size(); ++p) {
    _g[p]->GetNeighbours(n);
    for (Pos q = 0; q < n.size(); ++q)
      if (_g[p]->GetId() != n[q]->GetId()) {
        _boundary.push_back(_g[p]);
        _g[p]->InBoundary(true);
        break;
      }
  }
}

void Grain::SortBoundary2D() {
  static bool rerun = false;
  if (_boundary.size() == 0) {
    cout << "SortBoundary2D::Boundary can't be sorted before FindBoundary "
            "algorithm\n";
    return;
  }
  MCPoint *b0 = 0;
  Neighbours n;
  Neighbours b;
  std::vector<int> ids;

  if (!_periodicConditions)
    for (Pos i = 0; i < _boundary.size(); ++i) {
      if ((_boundary[i]->GetI() == 0) ||
          (_boundary[i]->GetI() == _box->Get_xsize() - 1) ||
          (_boundary[i]->GetJ() == 0) ||
          (_boundary[i]->GetJ() == _box->Get_ysize() - 1)) {
        b0 = _boundary[i];
        _sortedboundary.push_back(b0);
        b0->InSortedBoundary(true);
        break;
      }
    }

  if (_sortedboundary.size() == 0) {
    for (Pos i = 0; i < _boundary.size(); ++i) {
      if (_boundary[i]->GetNumberOfNearestIds() > 3) {
        b0 = _boundary[i];
        b0->SetColor(255, 0, 0);
        break;
      }
    }
    if (b0 == 0)
      for (Pos i = 0; i < _boundary.size(); ++i) {
        if (_boundary[i]->GetNumberOfNearestIds() > 2) {
          b0 = _boundary[i];
          b0->SetColor(0, 0, 255);
          break;
        }
      }
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    // IMPORTANT BUG:: there might be a boundary without triple points -> grain
    // inside a grain
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    if (b0 != 0) {
      _sortedboundary.push_back(b0);
      b0->InSortedBoundary(true);
      // b0->SetColor(0,0,255);
      Pos stop;

      for (Pos iter = 0; iter < 4; ++iter) {
        stop = _sortedboundary.size();
        for (Pos i = 0; i < stop; ++i) {

          _sortedboundary[i]->GetNeighbours(n);
          for (Pos q = 0; q < n.size(); ++q) {
            if (n[q]->InBoundary() && !n[q]->InSortedBoundary() &&
                n[q]->GetId() == b0->GetId()) {
              if (n[q]->GetX() > b0->GetX()) {
                n[q]->InSortedBoundary(true);
                int color = 255 - 255.0 * (_sortedboundary.size() + 1) /
                                      _boundary.size();
                n[q]->SetColor(color, color, color);
                _sortedboundary.push_back(n[q]);
              }
            }
          }
        }
      }

      if (stop == _sortedboundary.size()) // no new points!!!
      {
        for (Pos c = 1; c < _sortedboundary.size(); ++c) {
          _sortedboundary[c]->InSortedBoundary(false);
          _sortedboundary[c]->UpdateColor();
        }
        _sortedboundary.clear();
        _sortedboundary.push_back(b0);

        for (Pos iter = 0; iter < 4; ++iter) {
          stop = _sortedboundary.size();
          for (Pos i = 0; i < stop; ++i) {
            _sortedboundary[i]->GetNeighbours(n);
            for (Pos q = 0; q < n.size(); ++q) {
              if (n[q]->InBoundary() && !n[q]->InSortedBoundary() &&
                  n[q]->GetId() == b0->GetId()) {
                if (n[q]->GetX() < b0->GetX()) {
                  n[q]->InSortedBoundary(true);
                  int color = 255 - 255.0 * (_sortedboundary.size() + 1) /
                                        _boundary.size();
                  n[q]->SetColor(color, color, color);
                  _sortedboundary.push_back(n[q]);
                }
              }
            }
          }
        }
      }
    } else {
      cout << "SORT_BOUNDARY2D::GRAIN Boundary doesnt have triple points! "
              "Sorting is stopped\n";
      return;
    }
  }

  int increment = 2;
  _sortedboundary[_sortedboundary.size() - 1]->GetNeighbours(n);
  for (Pos q = 0; q < n.size(); ++q) {
    if (!n[q]->InSortedBoundary()) {
      increment = 1;
      break;
    }
  }
  for (Pos i = _sortedboundary.size() - increment; i < _sortedboundary.size();
       ++i) // 2 bo czasem ostatni punkt posortowanej granicy moze byc
            // zablokowany
  {
    _sortedboundary[i]->GetNeighbours(n);

    for (Pos q = 0; q < n.size(); ++q) {

      if (n[q]->InBoundary() && !n[q]->InSortedBoundary() &&
          n[q]->GetId() == b0->GetId()) {
        n[q]->InSortedBoundary(true);
        int color =
            255 - 255.0 * (_sortedboundary.size() + 1) / _boundary.size();
        n[q]->SetColor(color, color, color);
        _sortedboundary.push_back(n[q]);
      }
    }
  }
  // check of rebranching!!
  if (rerun == false) {
    float distance;
    if (_sortedboundary.size() % 2 == 0)
      distance = _sortedboundary[_sortedboundary.size() / 2]->GetDistance(
          _sortedboundary[(_sortedboundary.size() + 2) / 2]);
    else
      distance = _sortedboundary[(_sortedboundary.size() - 1) / 2]->GetDistance(
          _sortedboundary[(_sortedboundary.size() + 1) / 2]);

    if (distance > 10.0f) {
      cout << "Split Boundary\n";
      std::random_shuffle(_boundary.begin(), _boundary.end());
      for (Pos i = 0; i < _sortedboundary.size(); ++i) {
        _sortedboundary[i]->InSortedBoundary(false);
        _sortedboundary[i]->UpdateColor();
      }
      _sortedboundary.clear();
      // rerun=true;
      SortBoundary2D();
    }
  }
}
/*
void Grain::BoundaryCurvature()
{
        if(_boundary.size()==0)
                FindBoundaryByGrainId();

        CenterOfGravity();
        SortBoundary2D();
        MCPoint* left;
        MCPoint* right;
        Pos index;
        Pos range=10;
        if(_sortedboundary.size()>2*range)
                for(Pos i=0;i<_sortedboundary.size();++i)
                {
                        if(i<range)
                        {
                                index=_sortedboundary.size()-range-1+i;
                                left=_sortedboundary[index];
                                right=_sortedboundary[i+range];
                        }
                        else if(i>_sortedboundary.size()-range-1)
                        {
                                index=range+i-_sortedboundary.size();
                                right=_sortedboundary[i-10];
                                left=_sortedboundary[index];
                        }
                        else
                        {
                                left=_sortedboundary[i-range];
                                right=_sortedboundary[i+range];
                        }

                        float x,y;

                        x=(right->GetX()+left->GetX())/2.0f;
                        y=(right->GetY()+left->GetY())/2.0f;

                        float distance1, distance2;


                        distance1=_cog->GetDistance(x,y,0);
                        distance2=_cog->GetDistance(_sortedboundary[i]);

                        //float distance2=_cog->GetDistance(_sortedboundary[i]);

                        //cout<<distance1<<" "<<distance2<<endl;
                        //system("pause");
                        if(distance2-distance1>-0.1f)
                                _sortedboundary[i]->SetColor(255,255,255);
                        else
                                _sortedboundary[i]->SetColor(0,0,0);


                }
        //for(Pos i=0;i<_boundary.size();++i)
        //	_boundary[i]->SetRadius(_boundary[i]->GetDistance(_avX, _avY,
_avZ));
}
*/

void Grain::BoundaryCurvatureAdv() {
  if (_boundary.size() == 0) {
    FindBoundaryByGrainId();
    CenterOfGravity();
    SortBoundary2D();
  }

  if (_sortedboundary.size() == 0)
    SortBoundary2D();

  if ((_avX == 0.0f) && (_avY == 0.0f))
    CenterOfGravity();

  if (_sortedboundary.size() == 0) {
    // cout<<"ERROR: no boundary points in curvature calculation\n";
    return;
  }

  Pos counter = 0;
  for (Pos i = 1; i < _sortedboundary.size(); ++i) {
    if (_sortedboundary[i]->GetDistance(_sortedboundary[i - 1]) > 10.0f) {
      counter++;
    }
  }
  if (1.0f * counter / _sortedboundary.size() > 0.3f) {
    // cout<<"ERROR::Boundary wrongly sorted; End of calculations in this
    // case\n";
    return;
  }
  MCPoint *prev;
  MCPoint *next;
  Pos index;

  prev = _sortedboundary[0];
  // prev->SetColor(0,0,255);

  for (index = 1; index < _sortedboundary.size(); ++index)
    if ((_sortedboundary[index]->GetNumberOfNearestIds() > 2) ||
        (index == _sortedboundary.size() - 1)) {
      next = _sortedboundary[index];
      next->SetColor(255, 0, 0);
      break;
    }

  for (Pos i = 1; i < _sortedboundary.size(); ++i) {

    if (index == i) {

      prev = next;
      for (Pos p = index + 1; p < _sortedboundary.size(); ++p) {
        if (_sortedboundary[p]->GetNumberOfNearestIds() > 2) {
          index = p;
          next = _sortedboundary[p];
          next->SetColor(255, 0, 0);
          break;
        }

        if ((_sortedboundary[p]->GetNumberOfNearestIds() == 2) &&
            (_sortedboundary[p]->GetI() ==
             0)) // uwzgl�dniam TRPILE POINTS NA BRZEGU X=0
        {
          index = p;
          next = _sortedboundary[p];
          next->SetColor(255, 0, 0);
          break;
        }

        if (p == _sortedboundary.size() - 1) {
          index = p;
          prev = next;
          next = _sortedboundary[p];
          next->SetColor(255, 0, 0);
        }
      }
      // continue;
    }

    if (next != prev) {
      float xp = next->GetX() - prev->GetX();
      float yp = next->GetY() - prev->GetY();

      float vbx = next->GetX() - _sortedboundary[i]->GetX();
      float vby = next->GetY() - _sortedboundary[i]->GetY();

      float vcx = next->GetX() - _avX;
      float vcy = next->GetY() - _avY;

      float vnx = _sortedboundary[i]->GetX() - _avX;
      float vny = _sortedboundary[i]->GetY() - _avY;

      float S1 = vbx * yp - vby * xp;
      float S2 = vcx * yp - vcy * xp;
      float S0 = vnx * yp - vny * xp;

      float distance3;
      float A, B, C;
      if (prev->GetX() != next->GetX()) {
        A = (prev->GetY() - next->GetY()) / (prev->GetX() - next->GetX());
        C = next->GetY() - next->GetX() * (next->GetY() - prev->GetY()) /
                               (next->GetX() - prev->GetX());
        B = -1.0f;
      } else {
        return;
      }

      if ((A == 0.0f) && (B == 0.0f)) {
        distance3 = 0.0f;
        return;
      } else
        distance3 = fabs((A * _sortedboundary[i]->GetX() +
                          B * _sortedboundary[i]->GetY() + C)) /
                    sqrt(A * A + B * B);

      if (distance3 < 0.001f)
        distance3 = 0.0f;

      if ((S0 > 0.0f) && (S2 > 0.0f))
        if (S1 < 0.0f) {
          _sortedboundary[i]->SetCurvature(distance3);
        } else
          _sortedboundary[i]->SetCurvature(-distance3);

      else if ((S0 < 0.0f) && (S2 < 0.0f))
        if (S1 > 0.0f) {
          _sortedboundary[i]->SetCurvature(distance3);
        } else
          _sortedboundary[i]->SetCurvature(-distance3);

      else if (S0 * S2 < 0.0f)
        _sortedboundary[i]->SetCurvature(distance3);
    }
  }
}

void Grain::AverageBoundaryCurvature() {
  // it is assumed that each point has a correct curvature already calculated
  for (Pos i = 0; i < _sortedboundary.size(); ++i)
    _avBC += _sortedboundary[i]->GetCurvature();
  _avBC /= _sortedboundary.size();
}

void Grain::AverageBoundaryRadius() {
  // it is assumed that each point has a correct curvature already calculated
  for (Pos i = 0; i < _boundary.size(); ++i)
    _avBR += _boundary[i]->GetRadius();
  _avBR /= _boundary.size();
}

float Grain::MaxBoundaryCurvature() {
  // zakldam ze kazdy punkt ma juz policzona krzywizne!
  float max = 0.0f;

  for (Pos i = 0; i < _sortedboundary.size(); ++i)
    if (fabs(_sortedboundary[i]->GetCurvature()) > max)
      max = fabs(_sortedboundary[i]->GetCurvature());
  return max;
}

// -- DEPRECATED --//
void Grain::FindBoundary(const float &bl) {
  _boundary.clear();
  Neighbours n;
  for (Pos p = 0; p < _g.size(); ++p) {
    _g[p]->GetNeighbours(n);
    for (Pos q = 0; q < n.size(); ++q)
      if (_g[p]->FindMinOmega(n[q]) * RAD2DEG <= bl) {
        _boundary.push_back(_g[p]);
        break;
      }
  }
}
#endif
