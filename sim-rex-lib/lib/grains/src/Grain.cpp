#include "Grain.h"
#include <Orientation/BungeAngles.h>
#include <Orientation/Calculator.h>
#include <Orientation/Converter.h>
#include <Orientation/CrystalSymmetry.h>

#include <cmath>
#include <iostream>
namespace Grains {
void Grain::buildAlgorithm(
    const std::function<bool(GridPoint *p1, GridPoint *p2)> &condition) {
  for (auto i = 0; i < data.size(); ++i) {
    auto &p = data[i];
    auto nn = p->getNeighbors();
    for (const auto &n : nn) {
      if ((n != nullptr) && (condition(p, n)))
        add(n);
    }
  }
}

void Grain::build(float min_mis) {
  buildAlgorithm([&min_mis](GridPoint *p1, GridPoint *p2) {
    return Orientation::Calculator::minMisoAngle(p1->getOrientation(),
                                                 p2->getOrientation(), CS) *
               180.0 / M_PI <=
           min_mis;
  });
}

void Grain::build() {
  buildAlgorithm(
      [](GridPoint *p1, GridPoint *p2) { return p1->getId() == p2->getId(); });
}

void Grain::add(GridPoint *s) {
  if (s->getGrain() == nullptr) {
    s->setGrain(this);
    data.emplace_back(s);
  }
}

void Grain::getCentroid() {
  if (data.size() == 0)
    std::cerr << "ERROR: No points in a grain\n";
  else {
    for (auto &p : data) {
      centroid = centroid + p->getRealPosition();
    }

    centroid = centroid / data.size();
  }
}

void Grain::calcAvMisoAngle() {
  float mis_sum = 0.0;
  int counter = 0;
  for (auto first = 0; first < getSize() - 1; ++first)
    for (auto second = first + 1; second < getSize(); ++second) {
      mis_sum += Orientation::Calculator::minMisoAngle(
          data[first]->getOrientation(), data[second]->getOrientation(), CS);
      counter++;
    }
  if (counter > 0)
    av_mis = mis_sum / counter;
}

// TODO:: finish the algorithm
auto Grain::calcAvOrientation() {
  Orientation::Quaternion<double> av_orientation;
  for (auto &p : data) {
    av_orientation = av_orientation + p->getOrientation();
  }
  return av_orientation;
}

// TODO:: finish the algorithm
void Grain::calcGos() {
  // if (_avFi1 == -1.0)
  auto av_orientation = calcAvOrientation();
  float mis_sum = 0.0;

  for (auto &p : data)
    mis_sum += Orientation::Calculator::minMisoAngle(av_orientation,
                                                     p->getOrientation(), CS);
  gos = mis_sum / getSize();

  if (gos > 10.0) {
    // FindBoundaryByGrainId();
    // SetGrainBoundaryColor(0, 0, 0);
  }
}

void Grain::calcAvBungeAngles() {
  const double RAD_60 = M_PI_2 / 3.0;
  const double RAD_90 = M_PI_4;
  const double RAD_45 = RAD_90 / 2.0;
  const double RAD_180 = M_PI_2;
  const double RAD_360 = M_PI;
  double phi_1, Phi, phi_2;
  std::vector<double> phi_1_vec;
  std::vector<double> phi_2_vec;
  std::vector<double> Phi_vec;

  for (auto i = 0; i < getSize(); ++i) {
    Orientation::BungeAngles<double> ba =
        Orientation::Converter::QuaternionToBungeAngles(
            data[i]->getOrientation());
    phi_1 = ba.getPhi1();
    Phi = ba.getPhi();
    phi_2 = ba.getPhi2();

    if (phi_1 > RAD_90) {
      phi_1 = RAD_180 - Phi;
      phi_1 += RAD_180;

      if (phi_1 > RAD_360)
        phi_1 -= RAD_360;

      while (phi_1 > 0)
        phi_2 -= RAD_60;

      phi_2 = -phi_2;
    } else {
      while (phi_2 > RAD_60)
        phi_2 -= RAD_60;
    }

    phi_1_vec.push_back(phi_1);
    Phi_vec.push_back(Phi);
    if (phi_2_vec.size() == 0)
      phi_2_vec.push_back(phi_2);
    else {
      if ((phi_2_vec[i - 1] - phi_2) > RAD_45) {
        phi_2 += RAD_60;
      } else if ((phi_2 - phi_2_vec[i - 1]) > RAD_45) {
        for (auto f = 0; f < phi_2_vec.size(); ++f)
          phi_2_vec[f] += RAD_60;
      }
      phi_2_vec.push_back(phi_2);
    }
  }

  /*for(Pos f=1;f<fi1_vec.size();++f)
  {
          if(fi1_vec[f]-fi1_vec[f-1]>RAD_45)
                  fi1_vec[f]=RAD_360-fi1_vec[f];
          else if(fi1_vec[f-1]-fi1_vec[f]>RAD_45)
                  fi1_vec[f-1]=RAD_360-fi1_vec[f-1];
  }*/
  phi_1 = Phi = phi_2 = 0.0;
  for (auto a : phi_1_vec)
    phi_1 += a;
  for (auto a : phi_2_vec)
    phi_2 += a;
  for (auto a : Phi_vec)
    Phi += a;
  avg_angles.set(phi_1 / getSize(), Phi / getSize(), phi_2 / getSize(), false);
}

//:: SetGrainColor:: testing function to select grains which where found by
// grain recognition algorithm
void Grain::setColor(Color rgb) {
  for (auto &p : data)
    p->setColor(rgb);
}
void Grain::setAvOrientationToGrainPoints() {
  // if (_avFi1 == -1.0)
  //  CalculateAvOrientation();

  for (auto &p : data)
    p->setOrientation(calcAvOrientation());
}

void Grain::getCentroidPoint() {
  /*
  if (_cog == 0) {
    Pos nearestPointIndex = 0;
    float min_distance = _g[0]->GetDistance(_avX, _avY, _avZ);
    float new_distance;
    for (Pos i = 1; i < _g.size(); ++i) {
      new_distance = _g[i]->GetDistance(_avX, _avY, _avZ);
      if (new_distance < min_distance) {
        min_distance = new_distance;
        nearestPointIndex = i;
      }
    }

    _cog = _g[nearestPointIndex];
    _cog->IsCenterOfGravity(true);
  } else {
    cout << "Point of Gravity already exists\n";
  }
  */
}

} // namespace Grains
