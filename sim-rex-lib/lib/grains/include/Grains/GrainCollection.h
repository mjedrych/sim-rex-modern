#pragma once
#include "Grain.h"
#include <Grid/Grid.h>
#include <functional>
#include <vector>

/*
GrainStatistics(MCBox *b) : _box(b), _min_gs(20), _min_misorientation(15.0) {}
  GrainStatistics(MCBox *b, ReadControler &rc) : _box(b) {
    using namespace GuiParameters;
    int temp;
    rc.Get(pSMinGS, temp);
    _min_gs = temp;
    rc.Get(pSMinMis, _min_misorientation);
  }
  void SetBox(MCBox *b) { _box = b; }
*/
namespace Grains {
class GrainCollection {
public:
  GrainCollection(Grid::Structure *pgrid) : grid(pgrid) {}

  auto getStatGrains() {
    return std::vector(all_grains.begin(), stat_grains_end);
  }
  auto &getAllGrains() { return all_grains; }
  auto getMinSize() { return min_gs; }

  auto getStatGrainsNumber() {
    return std::distance(all_grains.begin(), stat_grains_end);
  }
  auto getAllGrainsNumber() { return all_grains.size(); }

  void setMinMiso(const float &mm) { min_miso_angle = mm; }
  void setMinGS(const unsigned int &gs) { min_gs = gs; }
  void buildAllUsingId();
  void buildAll();

  void CalculateBoundaries();
  void CenterOfGravity();
  void PointOfGravity();
  void BoundaryCurvature();
  void ColorBoundaryCurvature();
  void ColorBoundaryCurvatureByGradient();
  void SortBoundary2D();

  void calcAvGs();
  void calcAvMisoAngle();
  auto getAvGs() { return av_gs; }
  auto getAvMisorientation() { return av_miso_angle; }

  void printStatGrains();
  void saveGrains();

private:
  void buildAlgorithm(const std::function<void(Grain &)> &);
  void applyOnEachGrain(const std::function<void(Grain &)> &);
  std::vector<Grain>::iterator stat_grains_end{nullptr};
  std::vector<Grain> all_grains;
  Grid::Structure *grid;
  float av_gs{0.0f};
  float av_miso_angle{0.0f};
  unsigned int min_gs{20};
  float min_miso_angle{15.0 / 180.0 * M_PI};
};
} // namespace Grains