#pragma once
#include <Common/Color.h>
//#include <Grid/Grid.h>
using Common::Color;

namespace Grains {
class Boundary {
public:
  void FindBoundaryByGrainId() {}
  void BoundaryCurvature() {}
  void BoundaryCurvatureAdv() {}
  void SortBoundary2D() {}
  void AverageBoundaryCurvature() {}
  void AverageBoundaryRadius() {}
  float MaxBoundaryCurvature() { return 0.0; }
  void setGrainBoundaryColor(Color rgb) {}
  // float getAverageBoundaryCurvature() { return av_BC; }

private:
  //	std::list<MCPoint*> _sortedboundarylist;
  //	std::vector<MCPoint*> _sortedboundary;
  float av_BC;
  float av_BR;
};
} // namespace Grains