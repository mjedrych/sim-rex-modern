#pragma once
#include "Boundary.h"
#include <Common/Color.h>
#include <Grid/Grid.h>
#include <Orientation/Orientation.h>
#include <functional>
#include <iostream>
using Common::Color;
using Grid::GridPoint;
using Grid::RealPosition;

namespace Grains {

class Grain {
public:
  Grain(GridPoint *s) {
    data.reserve(500 * 500);
    add(s);
    id = s->getId();
  }

  auto begin() noexcept { return data.begin(); }
  auto begin() const noexcept { return data.cbegin(); };
  auto end() noexcept { return data.end(); }
  auto end() const noexcept { return data.cend(); };

  void build(float min_mis);
  void build();

  void getCentroid();
  void getCentroidPoint();

  auto calcAvOrientation();
  void calcAvBungeAngles();
  void calcAvMisoAngle();
  void calcGos();
  void calcGosUsingBungeAngles() {}

  // SET && GET //
  int getId() { return id; }
  void setColor(Color rgb);
  float getGos() { return gos; }
  float getAvMisoAngle() { return av_mis; }

  auto getSize() { return data.size(); }
  const auto &GetCentroid() { return centroid; }
  void setAvOrientationToGrainPoints();

  GridPoint *operator[](size_t i) { return data[i]; }
  GridPoint *getGrainPoint(size_t i) {
    GridPoint *p = nullptr;
    try {
      p = data[i];
    } catch (const std::out_of_range &oor) {
      std::cerr << "Out of Range error: " << oor.what() << '\n';
      exit(EXIT_FAILURE);
    }
    return p;
  }

  friend std::ostream &operator<<(std::ostream &out, Grain g) {
    out << "ID: " << g.getId() << ", Grain Size: " << g.getSize() << "\n";
    return out;
  }

private:
  void add(GridPoint *);
  void
  buildAlgorithm(const std::function<bool(GridPoint *p1, GridPoint *p2)> &); //

  std::vector<GridPoint *> data;
  Boundary boundary;
  RealPosition centroid{0.0, 0.0, 0.0};
  Orientation::BungeAngles<double> avg_angles{-1.0, -1.0, -1.0, false};
  GridPoint *centroid_point{nullptr};

  int id{0};
  float gos{-1.0f};
  float av_mis{-1.0f};
};
} // namespace Grains