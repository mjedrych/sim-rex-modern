#include "gtest/gtest.h"
#include <Grains/Grains.h>
#include <Grid/Grid.h>
#include <iostream>
using namespace Grains;
using namespace Grid;
TEST(Grain, GenerateById_CubeStructure) {
  CubeNeighborsFinder cnf;
  Structure grid(&cnf, 10, 10);

  for (auto &g : grid)
    g.setId(1);

  Grain grain(&grid.get(GridPosition(5, 5)));
  grain.build();
  EXPECT_EQ(grid.getSize(), grain.getSize());
}

TEST(Grain, GenerateById_HexStructure) {
  HexNeighborsFinder cnf;
  Structure grid(&cnf, 10, 10);

  for (auto &g : grid)
    g.setId(1);

  Grain grain(&grid.get(GridPosition(2, 2)));
  grain.build();
  EXPECT_EQ(grid.getSize(), grain.getSize());
}

TEST(Grain, StatsTest) {
  int id = 10;
  CubeNeighborsFinder cnf;
  Structure grid(&cnf, 10, 10);

  for (auto &g : grid)
    g.setId(id);

  Grain grain(&grid.get(GridPosition(2, 2)));
  grain.build();
  grain.calcAvMisoAngle();
  grain.calcGosUsingBungeAngles();
  EXPECT_EQ(0.0, grain.getAvMisoAngle());
  EXPECT_EQ(-1.0, grain.getGos());
  EXPECT_EQ(id, grain.getId());
}

TEST(Grain, BlockedBuild) {
  CubeNeighborsFinder cnf;
  Structure grid(&cnf, 10, 10);

  for (auto &p : grid)
    p.setId(1);

  // inner scope to fire destructor
  {
    Grain grain1(&grid.get(GridPosition(2, 2)));
    grain1.build();
    EXPECT_EQ(grid.getSize(), grain1.getSize());

    Grain grain2(&grid.get(GridPosition(2, 2)));
    grain2.build();
    EXPECT_EQ(0, grain2.getSize());
  }
}
TEST(GrainCollection, GenerateById_CubeStructure) {
  CubeNeighborsFinder cnf;
  Structure grid(&cnf, 2, 2);

  for (auto &g : grid)
    g.setId(1);

  GrainCollection grains(&grid);
  grains.buildAllUsingId();
  EXPECT_EQ(1, grains.getAllGrainsNumber());
}
