#include <Orientation/Orientation.h>

#include <cmath>
#include <iostream>
typedef Orientation::Quaternion<double> OrientationD;
typedef Orientation::BungeAngles<double> BungeAnglesD;
typedef Orientation::RotMatrix<double> RotMatrixD;
using std::cout;
using namespace Orientation;
int main() {

  BungeAnglesD ea;

  ea.set(0.0, 200.0, 0.0);
  cout << ea;
  RotMatrixD m = Converter::BungeAnglesToRotMatrix(ea);
  ea = Converter::RotMatrixToBungeAngles(m);
  cout << "TEST FI > 180:    " << ea << "\n";

  OrientationD myQuat;
  myQuat.set(std::cos(M_PI_2), 0.0, 0.0, std::sin(M_PI_2));

  // myQuat.Conjugate();
  cout << "Misorientation   " << myQuat.getMisorientation() * 180.0 / M_PI
       << "\n";

  ea = Converter::QuaternionToBungeAngles(myQuat);
  cout << "QuaternionToBungeAngles  " << ea << "\n";

  return 0;
}